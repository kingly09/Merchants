//
//  MBLoginViewModel.swift
//  Merchants
//
//  Created by kingly on 2020/7/16.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit


enum MBLoginApi {
    case sendSmsVaifyCode(phone: String)
    case loginWithPhone(phone: String, smsCode: String)
}

extension MBLoginApi : MBBaseViewModel{
    var baseURL: URL {
        switch self {
        default:
            return URL(string: MBUrl.serverUrl)!
        }
    }
    
    var path: String {
        switch self {
        case .sendSmsVaifyCode:
            return MBUrl.Login.sendSmsCode
        case .loginWithPhone:
            return MBUrl.Login.loginByCode
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .post
        }
    }
    
    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
    
    var task: Task {
        var params : [String : Any] = [:]
        switch self {
        case .sendSmsVaifyCode(let phone):
            params["phone"] = phone
        case .loginWithPhone(let phone, let smsCode):
            params["phone"] = phone
            params["smsCode"] = smsCode
            params["registerType"] = "IOS"
            params["identifica"] = UIDevice.deviceId()
        }
        
        return .requestParameters(parameters: params, encoding: URLEncoding.default)
    }
    
    var headers: [String : String]? {
        return BYNetwork.defaultHeaders()
    }
    
    
}

class MBLoginViewModel: NSObject {
    
    

}
