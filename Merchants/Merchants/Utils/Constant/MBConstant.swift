//
//  MBConstant.swift
//  Merchants
//
//  Created by kingly on 2020/7/16.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit


/// 常量管理
class MBConstant: NSObject {
  
  
  // 字体
  static let FONT_10 : UIFont = UIFont.systemFont(ofSize: 10)
  static let FONT_11 : UIFont = UIFont.systemFont(ofSize: 11)
  static let FONT_12 : UIFont = UIFont.systemFont(ofSize: 12)
  static let FONT_13 : UIFont = UIFont.systemFont(ofSize: 13)
  static let FONT_14 : UIFont = UIFont.systemFont(ofSize: 14)
  static let FONT_15 : UIFont = UIFont.systemFont(ofSize: 15)
  static let FONT_16 : UIFont = UIFont.systemFont(ofSize: 16)
  static let FONT_17 : UIFont = UIFont.systemFont(ofSize: 17)
  static let FONT_18 : UIFont = UIFont.systemFont(ofSize: 18)
  static let FONT_20 : UIFont = UIFont.systemFont(ofSize: 20)
  static let FONT_22 : UIFont = UIFont.systemFont(ofSize: 22)
  static let FONT_32 : UIFont = UIFont.boldSystemFont(ofSize: 32)
  // 间距
  static let MARGIN_5  : CGFloat = 5
  static let MARGIN_10 : CGFloat = 10
  static let MARGIN_12 : CGFloat = 12
  static let MARGIN_15 : CGFloat = 15
  static let MARGIN_20 : CGFloat = 20
  

}

// MARK: - 线程队列
/// 创建一个当前线程队列
let KCreateConcurrentQueue  = DispatchQueue(label: "com.bc.castle.concurrent", attributes: .concurrent)

/// 主线程队列
let kMainThread = DispatchQueue.main;
/// 全局线程队列
let kGlobalThread = DispatchQueue.global(qos: .userInteractive)

// MARK: - 常用宽高

// Tab栏高度
public let globalTabbarHeight : CGFloat = isIPhoneX ? 83 : 49
// 导航栏高度
public let globalNavigationBarHeight : CGFloat = 64
let navHight : CGFloat =  isIPhoneX ? (globalNavigationBarHeight + statusBar) : globalNavigationBarHeight

// 除去导航栏高度，Tab栏高度
public let globalSizeWithoutNavbarOrTabbar = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT - (globalTabbarHeight + globalNavigationBarHeight))
/// 状态栏高度
public let statusBar: CGFloat  = 20

/// banner的高度
public let BANNER_HEIGHT: CGFloat = ((270*SCREEN_WIDTH)/750)
/// 最小值
public let FLT_MIN  = CGFloat.leastNormalMagnitude

/// 当前是否iphone x
let isIPhoneX: Bool =  BCGlobalFunc.isIPhoneXSeries()

/// 多国语言
var MLString: (String) -> String = { localizedString in
  return NSLocalizedString(localizedString, comment:localizedString);
}


// 状态栏高度
let  STATUS_BAR_HEIGHT : CGFloat   =  (isIPhoneX ? CGFloat(44): CGFloat(20))
// 导航栏高度
let NAVIGATION_BAR_HEIGHT: CGFloat  =  (isIPhoneX ? 88 : 64)
// tabBar高度
let TAB_BAR_HEIGHT   : CGFloat      =  (isIPhoneX ? (49 + 34) : 49)
// home indicator
let  HOME_INDICATOR_HEIGHT : CGFloat =  (isIPhoneX ? 34 : 0)

//let  kIphone5S : Bool = (SCREEN_HEIGHT <= 568) ? true : false

/// 最大密码输入框长度
let  MAX_TEXTFIELD_PWD_LEN  = 32

/// 手机号码长度
let  MAX_TEXTFIELD_MOBILE_LEN  = 11
/// 验证码的长度
let  MAX_TEXTFIELD_VERFYCODE_LEN  = 6
/// 当前模版
let isNight = "isNight"

let APP_ID = "bam"

/// 最大搜索输入长度
let  MAX_TEXTFIELD_SEARCH_LEN  = 30

let KTASEKeyKey  = "2916054684417924"

// MARK: - 常用的闭包宏定义
/// 只带一个error参数的失败闭包类型，主要用于处理失败回调
typealias BCErrorBlock = (NSError) -> Void

/// 只带一个errorMessage参数的失败闭包类型，主要用于处理失败回调
typealias BCErrorMessageBlock = (String) -> Void

/// 通用的空闭包类型，无参数，无返回值
typealias BCVoidBlock = () -> Void

/// 常用的返回Bool类型的闭包类型
typealias BCBoolBlock = (Bool) -> Void

/// 常用的手势回调的闭包类型
typealias BCGestureBlock = (UIGestureRecognizer) -> Void

/// 通知的闭包类型
typealias BCNotificationBlock = (NSNotification) -> Void

/// 常用的返回Bool类型和相应提示语的闭包类型
typealias BCBoolMsgBlock = (Bool, String) -> Void

/// 常用的返回数组类型的闭包类型
typealias BCArrayBlock = ([Any]) -> Void

/// 常用的返回Int类型的闭包类型
typealias BCIntBlock = (Int) -> Void;

/// 常用的返回字典类型的闭包类型
typealias BCDictionaryBlock = ([String: Any]) -> Void;

/// 常用的返回字典类型和错误提示语的闭包类型
typealias BCDictionaryMsgBlock = ([String: Any], String) -> Void;

/// 常用的返回String类型的闭包类型
typealias BCStringBlock = (String) -> Void;

/// 常用的返回Int类型和错误提示语的闭包类型
typealias BCIntMsgBlock = (Int, String) -> Void;

/// 常用的返回String类型和错误提示语的闭包类型
typealias BCStringMsgBlock = (String, String) -> Void;

/// 常用的返回任何类型(包括函数类型）的闭包类型
typealias BCAnyBlock = (Any) -> Void;

/// 常用的返回任何类型(包括函数类型）和错误提示语的闭包类型
typealias BCAnyMsgBlock = (Any, String) -> Void;

/// 常用的返回任何类类型的闭包类型
typealias BCAnyObjectBlock = (AnyObject) -> Void;

/// 常用的返回任何类类型和错误提示语的闭包类型
typealias BCAnyObjectMsgBlock = (AnyObject, String) -> Void

/// 常用的返回任何类类型和错误提示语的闭包类型
typealias BCAnyObjectBoolBlock = (AnyObject, Bool) -> Void

/// 调用者身份账号
let operatorStr =  "appUsers:sdk"

let testMobile  =  "18801014010"

/// appstore 下载链接
let kItunesDownloadUrl = "https://itunes.apple.com/app/id1445123498"
/// appstore评分
let kItunesitmsUrl = "itms-apps://itunes.apple.com/app/id1445123498"
