//
//  MBUrl.swift
//  Merchants
//
//  Created by kingly on 2020/7/16.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

class MBUrl: NSObject {
  
   
  /// 正式环境
 static let  serverUrl         = "https://xcx.baoyuyoujia.com"
  //static let  serverUrl         = "http://121.37.252.232:8890"
  /// 开发环境（联调）
  static let  serverUrlDevTest  = "http://121.37.252.232:8890"
  /// 测试环境
  static let  serverUrlTest     = "http://124.193.136.14:99"
  
  /// 到全局的 URL Pattern
  static let  routerFillURL     = "valuablejaneapp://"
    
  //static let  h5Url             = "http://192.168.1.88:9876"  //洪凯城的本地
  static let  h5Url             = "http://h5merchant.baoyuyoujia.com"   //宝玉有价测试环境
    

}


extension MBUrl {
    
    /// 路由器URL
    struct Router {
        
        struct Work {
            /// work 主界面
            static var Index = routerFillURL + "MBWorkVC/"
            
            struct Order {
                /// 订单管理
                static var OrderManage = Index + "MBOrderManageVC/"
           
            }
            
            struct Commod {
                 /// 商品管理
                 static var CommodManage = Index + "MBOrderManageVC/"
            
             }
            
            struct MyBoutique {
                /// 我的精品库
                static var MyBoutiqueManage = Index + "MBMyBoutiqueManageVC/"
                       
            }
            
            struct PublicGoods {
                /// 我的公有库
                static var PublicGoodsManage = Index + "MBPublicGoodsManageVC/"
                       
            }
            
            struct ActivityDec {
                /// 平台活动
                static var PublicGoodsManage = Index + "MBActivityDecManageVC/"
                       
            }
            
            
            struct Live {
              
                struct LiveRecording {
                     /// 直播回放列表
                     static var List = Index + "Live/MBLiveRecordingVC/"
                                   
                }
                
                struct AnchorManage {
                    /// 主播管理
                    static var List = Index + "Live/MBAnchorManageVC/"
                                                  
                }
                
                struct LiveAnnounce {
                    /// 直播计划
                    static var List = Index + "Live/MBLiveAnnounceVC/"
                                                  
                }
                
                /// 直播间
                struct LiveRoom {
                    /// 直播间主界面
                    static var  Open  = Index  + "Live/MBLiveRoomVC/"
                    /// 观看直播间
                    static var  Watch = Index  + "Live/MBWatchLiveRoomVC/"
                    /// 退出直播间
                    static var  Quit  = Index  + "Live/MBQuitLiveRoomVC/"
                                                  
                }
                                  
            }
                       
            
            
        }
        
        struct MsgCenter {
            
            /// 消息中心主界面
            static var Index = routerFillURL + "MBMsgCenter/"
        }
           
        struct MerCenter {
            
            /// 商户中心主界面
            static var Index = routerFillURL + "MerCenter/"
        }
        
    }
}





// MARK: -  用户系统
 extension MBUrl {
  
     struct Login {
        /// 登录发送验证码
        static var  sendSmsCode      =   "/merchant-api/merchant/main/sendSmsCode/"
        /// 登录校正验证码
        static var  loginByCode      =   "/merchant-api/merchant/main/loginByCode/"
    }
}


//
//http://h5admin.baoyuyoujia.com/
//http://h5app.baoyuyoujia.com/
//http://h5merchant.baoyuyoujia.com/
//https://xcx.baoyuyoujia.com/platform-admin
//https://xcx.baoyuyoujia.com/platform-api
//https://xcx.baoyuyoujia.com/merchant-api
//https://xcx.baoyuyoujia.com/static
//目前H5部分用二级域名访问

// MARK: -  商户端H5界面

//{path: '/settled', name: 'settled', component: Settled, meta: {title: '申请入驻'}}, //申请入驻选择
//{path: '/settled/fixedPriceApply', name: 'fixedPriceApply', component: FixedPriceApply, meta: {title: '申请入驻'}}, //一口价申请页面
//{path: '/settled/liveApply', name: 'liveApply', component: LiveApply, meta: {title: '申请入驻'}}, //直播申请页面
//{path: '/settled/form', name: 'settledForm', component: SettledForm, meta: {title: '资质录入'}}, //资质录入
//{path: '/settled/status', name: 'settledStatus', component: SettledStatus, meta: {title: '申请入驻'}}, //资质录入审核状态
//
//{path: '/order/list', name: 'orderList', component: OrderList, meta: {title: '订单列表'}}, //订单管理
//{path: '/order/detail/:id', name: 'orderDetail', component: OrderDetail, meta: {title: '订单详情'}}, //订单详情
//
//{path: '/activity', name: 'activity', component: Activity, meta: {title: '活动申报'}}, //活动申报选择页面
//{path: '/activity/list', name: 'activityList', component: ActivityList, meta: {title: '活动列表'}}, //活动列表页面
//{path: '/activity/apply', name: 'activityApply', component: ActivityApply, meta: {title: '活动申报'}}, //活动申报信息页面
//{path: '/activity/edit', name: 'activityEdit', component: ActivityEdit, meta: {title: '选款'}}, //活动选款页面
//{path: '/activity/detail', name: 'activityDetail', component: ActivityDetail, meta: {title: '报名记录'}}, //报名记录详情页面
//{path: '/activity/record', name: 'activityRecord', component: ActivityRecord, meta: {title: '报名记录'}}, //报名记录列表页面
//{path: '/activity/schedule', name: 'activitySchedule', component: ActivitySchedule, meta: {title: '活动排期'}}, //活动排期页面
//
//{path: '/product', name: 'product', component: Product, meta: {title: '商品管理'}},//商品管理
//{path: '/product/upload', name: 'upload', component: Upload, meta: {title: '上传商品'}},//上传商品
//{path: '/product/edit/:id', name: 'edit', component: Upload, meta: {title: '编辑商品'}},//编辑商品
//{path: '/product/style', name: 'style', component: Style, meta: {title: '请选择款式'}},//上传商品
//
//{path: 'my/wallet', name: 'wallet', component: MyWallet, meta: {title: '我的钱包'}}, //我的钱包
//{path: 'my/wallet/getMoney', name: 'getMoney', component: GetMoney, meta: {title: '提现'}}, //提现
//{path: 'my/wallet/addCard', name: 'addCard', component: AddCard, meta: {title: '添加法人账户信息'}}, //添加法人账户信息
//{path: 'my/wallet/rest', name: 'rest', component: Rest, meta: {title: '红包活动余额'}}, //红包活动余额
//{path: 'my/wallet/detail', name: 'detail', component: Detail, meta: {title: '钱包明细'}}, //钱包明细
//
//{path: '/live', name: 'live', component: Live, meta: {title: '直播'}}, //直播
//{path: '/live/plan', name: 'live', component: Plan, meta: {title: '直播计划'}}, //直播计划
//{path: '/live/create', name: 'create', component: Create, meta: {title: '新建直播'}}, //新建直播
//{path: '/live/aboutAccount', name: 'aboutAccount', component: AboutAccount, meta: {title: '关联人员'}}, //关联人员
//{path: '/live/record', name: 'record', component: Record, meta: {title: '直播记录'}}, //直播记录
//
//{path: '/qualityGoods/my', name: 'myQualityGoods', component: My, meta: {title: '我的精品库'}}, //我的精品库
//{path: '/qualityGoods/public', name: 'publicQualityGoods', component: Public, meta: {title: '公有精品库'}}, //公有精品库
//{path: '/qualityGoods/apply', name: 'applyQualityGoods', component: Apply, meta: {title: '申请精选产品'}}, //申请精选产品
//
//{path: '/marketing', name: 'marketing', component: Marketing, meta: {title: '营销管理'}}, //营销管理
//{path: '/redEnvelopes', name: 'redEnvelopes', component: RedEnvelopes, meta: {title: '红包管理'}}, //红包管理
//{path: '/redEnvelopes/add', name: 'redEnvelopesAdd', component: RedEnvelopesAdd, meta: {title: '新增红包'}}, //新增红包
//{path: '/redEnvelopes/detail', name: 'redEnvelopesDetail', component: RedEnvelopesDetail, meta: {title: '红包详情'}}, //红包详情
//{path: '/coupon', name: 'coupon', component: Coupon, meta: {title: '优惠券管理'}}, //优惠券管理
//{path: '/coupon/detail/:id', name: 'couponDetail', component: CouponDetail, meta: {title: '优惠券详情'}}, //优惠券详情
//{path: '/coupon/record/:id', name: 'couponRecord', component: CouponRecord, meta: {title: '优惠券发放记录'}}, //优惠券发放记录
//{path: '/coupon/goodsList/:id', name: 'couponGoodsList', component: CouponGoodsList, meta: {title: '优惠券关联商品'}}, //优惠券关联商品
//{path: '/coupon/Add', name: 'couponAdd', component: CouponAdd, meta: {title: '新增优惠券'}}, //新增优惠券
//{path: '/activityMGT', name: 'activityMGT', component: ActivityMGT, meta: {title: '活动管理'}}, //活动管理
//{path: '/activityMGT/detail/:id', name: 'activityMGTDetail', component: ActivityMGTDetail, meta: {title: '活动详情'}}, //活动详情
//{path: '/activityMGT/add', name: 'activityMGTAdd', component: ActivityMGTAdd, meta: {title: '新增活动'}}, //新增活动
//
//{path: '/business', name: 'business', component: Business, meta: {title: '商户中心'}}, //商户中心
//{path: '/business/detail', name: 'businessDetail', component: BusinessDetail, meta: {title: '店铺设置'}}, //店铺设置
//{path: '/business/edit', name: 'businessEdit', component: BusinessEdit, meta: {title: '编辑资料'}}, //编辑资料
//{path: '/business/changePhone', name: 'businessChangePhone', component: BusinessChangePhone, meta: {title: '解除绑定手机号'}}, //绑定手机号
//{path: '/business/pwd', name: 'businessPWD', component: BusinessPWD, meta: {title: '设置交易密码'}}, //设置交易密码
//{path: '/account', name: 'account', component: Account, meta: {title: '账户管理'}}, //账户管理
//{path: '/account/add', name: 'accountAdd', component: AccountAdd, meta: {title: '新增子账号'}}, //新增子账号
//{path: '/account/edit/:id', name: 'accountEdit', component: AccountEdit, meta: {title: '编辑子账号'}}, //编辑子账号
//
//{path: '/role', name: 'role', component: Role, meta: {title: '角色管理'}}, //角色管理
//{path: '/role/edit/:id', name: 'roleEdit', component: RoleEdit, meta: {title: '编辑角色'}}, //编辑/新增角色

extension MBUrl {
    
    struct h5 {
        static var token = "?token=" + MBUserInfo.shared.getLoginRspInfo().merToken!
        
        struct live {
            ///直播记录
            static var  record               =   MBUrl.h5Url  + "/live/record" + token
            ///直播计划
            static var  plan                 =   MBUrl.h5Url  + "/live/plan" + token
            ///主播管理
            static var anchorManage          =   MBUrl.h5Url  + "/live/anchorManage" + token
        }
        
        struct settled {
        
            /// 申请入驻选择
            static var  index                 =   MBUrl.h5Url  + "/settled" + token
            /// 一口价申请页面
            static var  fixedPriceApply       =   MBUrl.h5Url  + "/settled/fixedPriceApply" + token
            /// 直播申请页面
            static var  liveApply             =   MBUrl.h5Url  + "/settled/liveApply" + token
            /// 资质录入
            static var  settledForm           =   MBUrl.h5Url  + "/settled/form" + token
            ///资质录入审核状态
            static var  settledStatus         =   MBUrl.h5Url  + "/settled/status" + token
            
        }
        
        ///订单管理
        struct order {
            
            static var  orderList              =   MBUrl.h5Url  + "/order/list" + token
            static var  orderDetail            =   MBUrl.h5Url  + "/order/detail/:id" + token
        }
        
        ///商品管理
        struct product {
            
            static var  list              =   MBUrl.h5Url  + "/product" + token
        
        }
        
        struct wallet {
             
              static var  me              =   MBUrl.h5Url  + "/my/wallet" + token
              /// 红包活动余额
              static var  rest            =   MBUrl.h5Url  + "/my/wallet/rest" + token
             /// 钱包明细
              static var  detail          =   MBUrl.h5Url  + "/my/wallet/detail" + token
            
        }
        
        struct qualityGoods {
              
              /// 我的精品库
              static var  me              =   MBUrl.h5Url  + "/qualityGoods/my" + token
              /// 公有库
              static var  publics         =   MBUrl.h5Url  + "/qualityGoods/public" + token
            
            
            
        }
    
        struct marketing {
            
            /// 营销管理
            static var  index                 =   MBUrl.h5Url  + "/marketing" + token
            
             
        }
        
        struct activity {
            ///活动申报
            static var  index                 =   MBUrl.h5Url  + "/activity" + token
        }
        
        struct merCenter {
            ///店铺设置
            static var  businessDetail        =   MBUrl.h5Url  + "/business/detail" + token
            /// 编辑资料
            static var  businessedit         =   MBUrl.h5Url  + "/business/edit" + token
            /// 帐户设置
            static var account                =   MBUrl.h5Url  + "/account" + token
            /// 角色设置
            static var role                   =   MBUrl.h5Url  + "/role" + token
            
            
        }
        
        
    }
    
    
}


