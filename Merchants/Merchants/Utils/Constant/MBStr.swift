//
//  BCStr.swift
//  Merchants
//
//  Created by kingly on2020/7/16.
//  Copyright © 2020年 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//


import UIKit

/// 用来管理字符串文案
class MBStr: NSObject {
  
  // MARK: - 公共的或常见的
  struct Common {
    static var all       : String { return NSLocalizedString("全部", comment: "") }
    static var add       : String { return NSLocalizedString("添加", comment: "") }
    static var edit      : String { return NSLocalizedString("修改", comment: "") }
    static var delete    : String { return NSLocalizedString("删除", comment: "") }
    static var share     : String { return NSLocalizedString("分享", comment: "") }
    static var confirm   : String { return NSLocalizedString("确定", comment: "") }
    static var cancel    : String { return NSLocalizedString("取消", comment: "") }
    static var submit    : String { return NSLocalizedString("提交", comment: "") }
    static var search    : String { return NSLocalizedString("搜索", comment: "") }
    static var update    : String { return NSLocalizedString("编辑", comment: "") }
    static var save      : String { return NSLocalizedString("保存", comment: "") }
    static var next      : String { return NSLocalizedString("下一步", comment: "") }
    /// 占位
    static var blank    : String { return NSLocalizedString("  ", comment: "") }
  }
  
  // MARK: -  VCTitle
  struct VCTitle {
    
    // MARK: - tabBar  标题
    static var msgCenter : String { return NSLocalizedString("消息", comment: "") }
    static var work : String { return NSLocalizedString("工作台", comment: "") }
    static var merCenter  : String { return NSLocalizedString("商户中心", comment: "") }
    static var me : String { return NSLocalizedString("我的", comment: "") }

    
    // MARK: - 登录注册
    static var regAgreement : String { return NSLocalizedString("平台注册协议", comment: "") }
    

  }
  
  // MARK: -  登录
  struct LoginSystem {
    
     // MARK: - 商户端 - 登录界面
    static var verifyCodeValue : String { return NSLocalizedString("请重新输入正确的验证码", comment: "") }
    
    
    
    // MARK: - 登录界面
    static var hi           : String { return NSLocalizedString("Hi～", comment: "") }
    static var welcome      : String { return NSLocalizedString("新用户注册", comment: "") }
    static var switchingSms : String { return NSLocalizedString("短信验证码登录", comment: "") }
    static var switchingPwd : String { return NSLocalizedString("用密码登录", comment: "") }
    static var login        : String { return NSLocalizedString("登录", comment: "") }
    static var loginName    : String { return NSLocalizedString("用户名", comment: "") }
    static var password     : String { return NSLocalizedString("密码", comment: "") }
   
    static var mobile       : String { return NSLocalizedString("手机号", comment: "") }
    
    static var username     : String { return NSLocalizedString("姓名", comment: "") }
    static var mobileCode   : String { return NSLocalizedString("手机号码", comment: "") }
    static var sex          : String { return NSLocalizedString("性别", comment: "") }
    static var email        : String { return NSLocalizedString("邮箱", comment: "") }
    static var bengmen      : String { return NSLocalizedString("部门", comment: "") }
    static var userCode     : String { return NSLocalizedString("员工编码", comment: "") }
    static var job          : String { return NSLocalizedString("职务", comment: "") }
    
    static var verifyCode   : String { return NSLocalizedString("验证码", comment: "") }
    static var getVerifyCode   : String { return NSLocalizedString("获取验证码", comment: "") }
    
    // MARK: - 默认文案
    static var loginNamePlaceholder  : String { return NSLocalizedString("请输入用户名/邮箱/手机号", comment: "") }
    static var passwordPlaceholder   : String { return NSLocalizedString("请输入密码", comment: "") }
    static var mobilePlaceholder     : String { return NSLocalizedString("请输入手机号", comment: "") }
    static var resetPlaceholder      : String { return NSLocalizedString("请设置密码～", comment: "") }
    static var verifyCodePlaceholder : String { return NSLocalizedString("请输入验证码", comment: "") }
    static var namePlaceholder       : String { return NSLocalizedString("请输入姓名", comment: "") }
    static var realnamePlaceholder   : String { return NSLocalizedString("请输入真实姓名", comment: "") }
    static var lcsrfail              : String { return NSLocalizedString("两次输入的密码不一致", comment: "") }
     static var pwdfail              : String { return NSLocalizedString("", comment: "") }

    // MARK: - 注册界面
    static var reg            : String { return NSLocalizedString("用户注册", comment: "") }
    static var forgetPassword : String { return NSLocalizedString("忘记密码", comment: "") }
    static var updatePassword : String { return NSLocalizedString("修改密码", comment: "") }
    
    static var welcomeReg     : String { return NSLocalizedString("欢迎注册竹云城堡", comment: "") }
    static var verifyMobileNote: String { return NSLocalizedString("请验证手机号~", comment: "") }
    static var loginedUser    : String { return NSLocalizedString("已有账号？去登录", comment: "") }
    static var regAge         : String { return NSLocalizedString("注册即代表阅读并同意", comment: "") }
    static var regAgreement   : String { return NSLocalizedString("《平台注册协议》", comment: "") }
    static var regNext        : String { return NSLocalizedString("下一步", comment: "") }
    
    
    static var forgetPasswordao      : String { return NSLocalizedString("啊哦~", comment: "") }
    static var forgetPasswordNote    : String { return NSLocalizedString("忘记密码了？", comment: "") }
    
    static var codeTimeMsg     : String { return NSLocalizedString("秒后重发", comment: "") }
    static var verifyMobilegs  : String { return NSLocalizedString("请输入正确的手机号码", comment: "") }
    static var verifyYzmgs  : String { return NSLocalizedString("请输入正确的验证码", comment: "") }
    static var readRegAgreement: String { return NSLocalizedString("请先阅读《竹云城堡平台注册协议》", comment: "") }
    
     static var isSetted      : String { return NSLocalizedString("已设置", comment: "") }
    
     static var yzpwdNote    : String { return NSLocalizedString("必须是8-18位英文字母，数字或符号，不能是纯数字或纯字母", comment: "") }
  }
  
  // MARK: - 工作台
  struct Work {
    
    static var mbname            : String { return NSLocalizedString("四会翡翠批发", comment: "") }
    static var noticeLabel       : String { return NSLocalizedString("平台公告轮播标题", comment: "") }
    static var totalamountnote   : String { return NSLocalizedString("平台公告轮播标题", comment: "") }
    static var commonEntry       : String { return NSLocalizedString("常用入口", comment: "") }
    static var liveEntry         : String { return NSLocalizedString("直播入口", comment: "") }
    static var applyResidence    : String { return NSLocalizedString("申请入驻", comment: "") }
    
    static var orderManage       : String { return NSLocalizedString("订单管理", comment: "") }
    static var commodManage      : String { return NSLocalizedString("商品管理", comment: "") }
    static var marketingManage   : String { return NSLocalizedString("营销管理", comment: "") }
    static var myBoutique        : String { return NSLocalizedString("我的精品库", comment: "") }
    static var publicGoods       : String { return NSLocalizedString("公有商品库", comment: "") }
    static var activityDec       : String { return NSLocalizedString("平台活动申报", comment: "") }
    
    
    static var liveAnnounce      : String { return NSLocalizedString("直播计划", comment: "") }
    static var liveRoom          : String { return NSLocalizedString("直播间管理", comment: "") }
    static var anchorManage      : String { return NSLocalizedString("主播管理", comment: "") }
    static var liveRecording     : String { return NSLocalizedString("直播记录", comment: "") }
    
  
}

struct Live {
    static var closeLive         : String { return NSLocalizedString("结束直播", comment: "") }
    static var openLive          : String { return NSLocalizedString("开始直播", comment: "") }
}
      
      
  
struct MerCenter {
    static var shopsetting       : String { return NSLocalizedString("店铺设置", comment: "") }
    static var accsetting        : String { return NSLocalizedString("账户设置", comment: "") }
    static var rolesetting       : String { return NSLocalizedString("角色设置", comment: "") }
    static var systemsetting     : String { return NSLocalizedString("系统设置", comment: "") }
    static var authinfoMer       : String { return NSLocalizedString("宝玉有价认证商家", comment: "") }
}
    
struct MsgCenter {
  
    static var platformnotice    : String { return NSLocalizedString("平台公告", comment: "") }
    static var systeminformation : String { return NSLocalizedString("系统信息", comment: "") }
    static var auctionresult     : String { return NSLocalizedString("拍卖结果", comment: "") }
    static var orderinfo         : String { return NSLocalizedString("订单信息", comment: "") }
    static var appraise          : String { return NSLocalizedString("商品评价", comment: "") }
}
    
  
  //网络错误
  struct HttpMsgError {
  
  
    static var networkNoRepresentor  : String { return NSLocalizedString("网络系统异常，请重试", comment: "") }
    static var networkHttpException : String { return NSLocalizedString("http系统异常，请重试", comment: "") }
    static var networkException      : String { return NSLocalizedString("网络异常，请重试", comment: "") }
 
  }
  
  // MARK: -  应用中心
  struct App {
  
      static var appcenter  : String { return NSLocalizedString("应用中心", comment: "") }
      static var editapp    : String { return NSLocalizedString("编辑应用", comment: "") }
      static var settingPwd : String { return NSLocalizedString("设置应用密码", comment: "") }
      static var allApp     : String { return NSLocalizedString("全部应用", comment: "") }
      static var setNoteApp  : String { return NSLocalizedString("请输入你要访问此应用的用户名和密码，只需要输入一次，以后不再需要输入。", comment: "") }
      static var setSuccMsg: String { return NSLocalizedString("你已成功设置该应用的用户名和密码", comment: "") }
      static var setSuccTitle: String { return NSLocalizedString("设置成功", comment: "") }
      static var continueAdd: String { return NSLocalizedString("继续添加", comment: "") }
      static var continueEidt: String { return NSLocalizedString("继续编辑", comment: "") }
     static var gotoApp : String { return NSLocalizedString("前往应用", comment: "") }
    
    static var noneUpdate : String { return NSLocalizedString("暂不更新", comment: "") }
    static var update     : String { return NSLocalizedString("版本更新", comment: "") }
    static var goupdate   : String { return NSLocalizedString("立即更新", comment: "") }
    static var chaupdate  : String { return NSLocalizedString("检查更新", comment: "") }
    static var maxVersion : String { return NSLocalizedString("已是最新版", comment: "") }
    
  }
  
  // MARK: -  企业相关
  struct company {
    
    static var mycompany  : String { return NSLocalizedString("我的企业", comment: "") }
    static var search     : String { return NSLocalizedString("搜索企业", comment: "") }
    static var add        : String { return NSLocalizedString("加入企业", comment: "") }
    static var skip       : String { return NSLocalizedString("跳过", comment: "") }
    static var welcome    : String { return NSLocalizedString("欢迎使用竹云城堡", comment: "") }
    static var welcomeNote: String { return NSLocalizedString("你现在可以加入企业，待企业管理员审批通过后可以通过城堡找到企业应用高效安全的完成工作", comment: "") }
    static var noadd    : String { return NSLocalizedString("你还未加入任何企业", comment: "") }
    static var companySearchEmpty    : String { return NSLocalizedString("请输入你所属的企业", comment: "") }
    static var joinCompany           : String { return NSLocalizedString("加入", comment: "") }
    static var application           : String { return NSLocalizedString("申请说明", comment: "") }
    static var applicationNotes      : String { return NSLocalizedString("请输入申请说明，方便管理员审核", comment: "") }
    static var applicationjoin       : String { return NSLocalizedString("申请加入", comment: "") }
    static var applicationjoinSucc   : String { return NSLocalizedString("你已申请加入该企业，请耐心等待管理员审核，现在你将以个人用户身份登录", comment: "") }
    static var applicationing        : String { return NSLocalizedString("申请中...", comment: "") }
    
  }
  
  
  // MARK: -  企业相关
  struct dev {
    
    static var test       : String { return NSLocalizedString("测试环境", comment: "") }
    static var dev        : String { return NSLocalizedString("开发环境", comment: "") }
    static var build      : String { return NSLocalizedString("生产环境", comment: "") }
    static var qiehuan    : String { return NSLocalizedString("切换环境", comment: "") }
 
  }

  
  

}
