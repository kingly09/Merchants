//
//  BCGlobalFunc.swift
//  Merchants
//
//  Created by kingly on2020/7/16.
//  Copyright © 2020年 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

/// 开机动画key
private let bootAnimationKey = "bootAnimationKey"
private let defaults = UserDefaults.standard

/// 根据图片名称获取图片
let globalImageWithName: (String) -> UIImage? = {imageName in
  return UIImage(named: imageName)
}
/// 用来管理只有在本工程才用到的全局方法
class BCGlobalFunc: NSObject {
  
  //MARK: 获取VIEW的VIEWCONTROLLER
  static func viewControllerWithView(view:UIView) -> UIViewController? {
    var next = view.superview
    while next != nil {
      let nextResponder = next!.next
      if(nextResponder != nil && (nextResponder! is UIViewController)){
        return nextResponder as? UIViewController
      }
      next = next!.superview
    }
    return nil
  }
  
  /// 是否需要开机动画
  ///
  /// - Returns: YES 开机动画, NO 不需要开机动画
  static func isBootAnimation() -> Bool {
    if defaults.bool(forKey: bootAnimationKey) {
      return false;
    }
     return true;
  }
  
  /// 设置开机动画是否完成
  ///
  /// - Parameter isAnimation: true 完成，false 没有完成
  static func saveBootAnimation( isAnimation: Bool) {
    defaults.set(isAnimation, forKey: bootAnimationKey)
  }
  
  
  /// 是iphone x系列的产品
  ///
  /// - Returns: true 为 iphone x系列的产品 false 不是
  static  func  isIPhoneXSeries() -> Bool {
    
    if (UIDevice.current.userInterfaceIdiom != .phone) {
      return false
    }
    
    if #available(iOS 11.0, *) {
      
      let mainWindow: UIWindow = ((UIApplication.shared.delegate?.window)!)!
      
      if mainWindow.safeAreaInsets.bottom > 0.0 {
        return true
      }
      
    }
    return false
  }

  

}
