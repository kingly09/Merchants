//
//  MBUserInfo.swift
//  Merchants
//
//  Created by kingly on 2020/7/16.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

enum LIVE_TYPE {
  case  NONE
  case  LIVEING
}

let KeySmallUserInfo   = Key<JSON>("KeySmallUserInfo")

@objc class MBUserInfo : NSObject {
  
  @objc static let shared = MBUserInfo()
    
    
    var liveType : LIVE_TYPE = .NONE
    
    var isFirst : Bool  = false
    
  // Make sure the class has only one instance
  // Should not init or copy outside
  private override init() {}
  
  @objc override func copy() -> Any {
    return self // BCUserInfo.shared
  }
  
  @objc override func mutableCopy() -> Any {
    return self // BCUserInfo.shared
  }
  
  // Optional
  @objc func reset() {
    // Reset all properties to default value
  }
  
/// 是否登录系统
///
/// - Returns: YES 登录过 NO 没有登录
@objc func isLogin() -> Bool {
     let loginRsp: MBLoginRspInfo  = self.getLoginRspInfo()
    if (loginRsp.merToken?.count)! > 0 {
         return  true
    }
    return false
 }
    
@objc func getLoginRspInfo() -> MBLoginRspInfo {
    let defaults = Defaults()
    if  defaults.has(KeySmallUserInfo) {
    let JSONSmallUserInfo  = defaults.get(for: KeySmallUserInfo)
    if JSONSmallUserInfo == nil {
        return MBLoginRspInfo(jsonData: JSON.null)
     }
    let loginRspInfo = MBLoginRspInfo(jsonData : JSONSmallUserInfo!)
        return loginRspInfo
     }
    return MBLoginRspInfo(jsonData: JSON.null)
}
  /// 退出登录
  @objc func signOut() {
    
    let defaults = Defaults()
    defaults.clear(KeySmallUserInfo)

//
//    GYCircleConst.saveGesture(nil, key: gestureOneSaveKey)
//    GYCircleConst.saveGesture(nil, key: gestureFinalSaveKey)
//
//
//    kMainThread.async {
//      let nav = BCMainNavgationVC()
//      nav.addChild(SmsLoginViewController())
//      UIApplication.shared.keyWindow?.rootViewController = nav
//      UIApplication.shared.keyWindow?.makeKeyAndVisible()
//    }
    
  }
  

  
  
  /// 进入主界面
  @objc func showRootViewController() {
    
    kMainThread.async {
        UIApplication.shared.keyWindow?.rootViewController = BCTabBarController()
      UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
  }
  
  /// 设置服务器地址
  ///
  /// - Parameter url: 服务器地址
  @objc func setupServerUrl(_ url: String) {
    let defaults = Defaults()
    let someKey = Key<String>("setupServerUrl")
    defaults.set(url, for: someKey)
  }
  
  
  /// 设置网络服务器
  ///
  /// - Returns: 返回对应的网络服务器地址
  @objc func getServerUrl() -> String {

    if(Bundle().ky_appSeverStatus == true) {
       return MBUrl.serverUrl
    }

    let defaults = Defaults()
    let someKey = Key<String>("setupServerUrl")
    if  defaults.has(someKey) {
      let serUrl   = defaults.get(for: someKey)
      if serUrl ==  nil {
       return MBUrl.serverUrl
      }
      return serUrl!
    }
    return MBUrl.serverUrl

  }
  
  
  /// 获得网络对应的文案信息
  ///
  /// - Returns: 返回网络对应的文案信息
  @objc func getServerUrlText() -> String {

    let u = MBUserInfo.shared.getServerUrl()
    if u == MBUrl.serverUrl {
      return MBStr.dev.build
    }
    if u == MBUrl.serverUrlDevTest {
      return MBStr.dev.dev
    }
    if u == MBUrl.serverUrlTest {
      return MBStr.dev.test
    }
    return MBStr.dev.build
  }
  
  
  //跳转到应用的AppStore页页面
//  @objc func gotoAppStore() {
//    let urlString = kItunesDownloadUrl
//    let url = NSURL(string: urlString)
//    UIApplication.shared.openURL(url! as URL)
//  }
  
  
  /// 保存暂不更新的版本号
  ///
  /// - Parameter updateVersion: 版本号
  @objc func setupNoneUpdate(_ updateVersion: Int) {
    
    let defaults = Defaults()
    let someKey = Key<Int>("setupNoneUpdate")
    defaults.set(updateVersion, for: someKey)
  }
  
  
  /// 是否点击过 - 暂不更新，如果服务器的版本号与点击的过的版本号一致 就是点击过暂不更新，其他的都算是没有点击过
  ///
  /// - Parameter updateVersion: 当前服务器的版本号
  /// - Returns: Bool  YES 为点击过暂不更新 NO为没有点击过
  @objc func isNoneUpdate(_ updateVersion: Int) -> Bool {
    
    let defaults = Defaults()
    let someKey = Key<Int>("setupNoneUpdate")
    if  defaults.has(someKey) {
      let ver   = defaults.get(for: someKey)
      if ver ==  nil {
        return false
      }
  
      if( ver == updateVersion) {
         return true
      }
      
      return false
    }
   return false
    
  }
  
    
    /// 获得当前直播状态
    /// - Returns: .none 未直播  .liveing 正在直播
  func getLiveType() -> LIVE_TYPE {
      
    return self.liveType
     
   }
  
}
