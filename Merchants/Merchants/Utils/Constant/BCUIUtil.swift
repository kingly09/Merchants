//
//  BCUIUtil.swift
//  Merchants
//
//  Created by kingly on2020/7/16.
//  Copyright © 2020年 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

let BASE_HEIGTH: CGFloat = 1334.0
let BASE_WIDTH: CGFloat =  750.0

var SCREEN_WIDTH: CGFloat = {
  return UIScreen.main.bounds.size.width
}()
var SCREEN_HEIGHT = {
  return UIScreen.main.bounds.size.height
}()

var SCREEN_SCALE: CGFloat = {
  return UIScreen.main.scale
}()
var SCREEN_BOUNDS: CGRect = {
  return UIScreen.main.bounds
}()

/// 该方法暂时只用做缩小两倍  没做像素适配
func ConvertWidth(pixel: CGFloat) -> CGFloat {
  //    return pixel * SCREEN_WIDTH / BASE_WIDTH
  
  return pixel * 0.5
}
/// 适配屏幕比例
func ConvertScreen(pixel: CGFloat) -> CGFloat {
  
  return pixel * SCREEN_WIDTH / BASE_WIDTH
  
  
}
///要适配4s高度不能用
//func ConvertHeight(pixel: CGFloat) -> CGFloat {
//    return pixel * SCREEN_HEIGHT / BASE_HEIGTH
//}


/// 获取字符串的Size
///
/// - Parameters:
///   - title: 内容字符串
///   - font: 字体大小
///   - size: 最大size
/// - Returns: 返回对应字符串的Size
func sizeWithString(title: String,font:UIFont,size:CGSize) -> CGSize {
  
  let attributes = [kCTFontAttributeName: font]
  return title.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes as [NSAttributedString.Key : Any], context: nil).size
}


class BCUIUtil {
  
  //视频宽度调整到全屏后的显示高度
  public static func realHeightFitScreenMedia(width: CGFloat, height: CGFloat) -> CGFloat {
    return height * (UIScreen.main.bounds.width / width)
  }
  
  public static func widthForOCClass(width : CGFloat) -> CGFloat {
    return ConvertWidth(pixel: width)
  }
  //    public static func heightForOCClass(Height : CGFloat) -> CGFloat {
  //        return ConvertHeight(pixel: Height)
  //    }
  //
}


extension String {
  func withLength(font:UIFont) -> CGFloat {
    
    let attributes = [kCTFontAttributeName: font]
    return self.boundingRect(with:CGSize.init(width: Int.max, height: Int.max) , options: .usesLineFragmentOrigin, attributes: attributes as [NSAttributedString.Key : Any], context: nil).size.width
  }
  
}


extension UILabel {
  func textLength() -> CGFloat {
    
    if let text = self.text {
      return text.withLength(font: font)
    }
    return 0
  }
}

extension UIFont {
  func convertSize() -> UIFont {
    var toFontSize = self.pointSize * SCREEN_WIDTH * SCREEN_SCALE / BASE_WIDTH
    //在6P上不做放大，只在小屏幕上缩小
    toFontSize = min(self.pointSize, toFontSize)
    if toFontSize == self.pointSize {
      return self
    }
    return UIFont(name: self.fontName, size: toFontSize)!
  }
}
