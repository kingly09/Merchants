//
//  BYAppConst.swift
//  Merchants
//
//  Created by kingly09 on 2020/7/12.
//  Copyright © 2020 kingly09. All rights reserved.
//

import Foundation

let kScreenWidth = UIScreen.main.bounds.size.width
let kScreenHeight = UIScreen.main.bounds.size.height
let kStatusBarHeight = UIApplication.shared.statusBarFrame.size.height

let kButtonColor = UIColor(red: 242/255.0, green: 77/255.0, blue: 51/255.0, alpha: 1)
let kDownColor = UIColor.init(red: 240/255.0, green: 241/255.0, blue: 244/255.0, alpha: 1)

/// 系统参数
let isIphoneX = kStatusBarHeight > 20 ? true : false
let kNavBarHeight : CGFloat = kStatusBarHeight > 20 ? 88 : 64
let kTabBarHeight : CGFloat = kStatusBarHeight > 20 ? 83 : 49
let kSafeAreaTopHeight : CGFloat = kStatusBarHeight > 20 ? 24 : 0
let kNavBarTopMargin : CGFloat = kStatusBarHeight > 20 ? 44 : 20
let kSafeAreaBottomHeight : CGFloat = kStatusBarHeight > 20 ? 34 : 0

/* 系统版本 */
let kSystemVersion:String = UIDevice.current.systemVersion
/* App名称 */
let kAppName: String? = Bundle.main.infoDictionary!["CFBundleDisplayName"] as? String
/* App版本号 */
let kAppVersion: String? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
/* bundleId */
let kAppBundleId: String? = Bundle.main.infoDictionary!["CFBundleIdentifier"] as? String
/* keyWindow*/
let kKeyWindow = UIApplication.shared.keyWindow
/* rootViewController*/
//let kRootViewController = UIApplication.shared.keyWindow!.rootViewController as! ESTabBarController
/* 通知中心*/
let kNotiCenter = NotificationCenter.default
/* 属性列表*/
let kUserDefaults = UserDefaults.standard

/// 常用颜色
let kColor_white = UIColor.white
let kColor_clear = UIColor.clear
let kColor_black = UIColor.black
let kColor_red = UIColor(hexStr: "F12D1B")
let kColor_textBlack = UIColor(hexStr: "2A2A2A")
let kColor_lightBlack = UIColor(hexStr: "242424")
let kColor_textGray = UIColor(hexStr: "5E5E5E")
let kColor_textLightGray = UIColor(hexStr: "ADADAD")
let kColor_detailTextGray = UIColor(hexStr: "8C8C8C")
let kColor_lineGrayColor = UIColor(hexStr: "F1F1F1")
let kColor_main = UIColor(hexStr: "D59E32")
let kColor_headerOptionalNormal = UIColor(hexStr: "999999")
let kColor_tableViewDefaultBackground = UIColor(hexStr: "F0F0F0")
let kColor_tableViewSeparator = UIColor(hexStr: "F9F9F9")
let kColor_disableGray = UIColor(hexStr: "BFBFBF")

func KColor(hexStr :String) -> UIColor {
    return UIColor(hexStr: hexStr)
}

/// 常用字体
/// - Parameter __SIZE__: 字体大小
func PFSC_Regular(size __SIZE__:CGFloat) ->UIFont {
    return UIFont.init(name: "PingFangSC-Regular", size: __SIZE__)!
}

func PFSC_Medium(size __SIZE__:CGFloat) ->UIFont {
    return UIFont.init(name: "PingFangSC-Medium", size: __SIZE__)!
}

func PFSC_Bold(size __SIZE__:CGFloat) ->UIFont {
    return UIFont.init(name: "PingFangSC-Semibold", size: __SIZE__)!
}

func PFSC_Light(size __SIZE__:CGFloat) ->UIFont {
    return UIFont.init(name: "PingFangSC-Light", size: __SIZE__)!
}

func Helvetica(size __SIZE__:CGFloat) ->UIFont {
    return UIFont.init(name: "Helvetica", size: __SIZE__)!
}

func Helvetica_Bold(size __SIZE__:CGFloat) ->UIFont {
    return UIFont.init(name: "Helvetica-Bold", size: __SIZE__)!
}

/// 图片设置
func UIImageMake(name: String) -> UIImage? {
    return UIImage(named: name) ?? UIImage()
}

//打印
func BYPrint<T>(parameter : T, file : String = #file, lineNumber : Int = #line)
{
    #if DEBUG
    
    let fileName = (file as NSString).lastPathComponent
    print("[\(fileName):line:\(lineNumber)]\n -\(parameter)\n")
    
    #endif
}

func AppVersion() -> String {
    let infoDict:Dictionary = Bundle.main.infoDictionary!
    let app_Version:String = infoDict["CFBundleShortVersionString"] as! String
    return "V"+app_Version
}

let noti_receive = NSNotification.Name(rawValue: "noti_receive")
let noti_user_noti_update = NSNotification.Name(rawValue: "noti_user_noti_update")
