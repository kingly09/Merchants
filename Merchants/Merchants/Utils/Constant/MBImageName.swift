//
//  VJImageName.swift
//  Merchants
//
//  Created by kingly on2020/7/16.
//  Copyright © 2020年 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
/// 图片名字管理类
class MBImageName: NSObject {

  // MARK: - 默认图标 placehode
  struct defaultImageName {
    static let header = "ic_moren_19"
    static let goods  = "default_product_big"
    static let video  = "default_video"
    static let logo   = "ic_LOGO"
   
    //缺省
    static let live   = "bg_shiping_19"
    static let livePreview  = "bg_quede_19"
    static let defaltHeadlogo  = "mine_headicon_default"
    
    static let defalt  = "ic_defalt"
  
    
  }
  
  // MARK: -   tabBar图标
  struct tabBar {
    static let merCenter    = "merCenter"
    static let work         = "work"
    static let msgCenter    = "msgCenter"
    static let me           = "me"
 
  }
  
  // MARK: -   常用的icon图标
  struct icon {
    static let check          = "check"
    static let uncheck        = "uncheck"
    static let backblack      = "backblack"
    static let whiteback      = "whiteback"
    static let rightArrow     = "rightArrow"
    static let ic_defalt      = "ic_defalt"
    static let btn_add        = "btn_add"
    static let btn_edit       = "btn_edit"
    static let btn_delete     = "btn_delete"
    static let btn_eye        = "btn_eye"
    static let btn_grep_eye   = "btn_grep_eye"
    static let dropDown       = "drop-down"
    static let noticeImage    = "ic_noticeImage"
    
    static let gift           = "gift"
    static let praise         = "praise"
    static let backclose      = "backclose"
    
    
  }
  
  // MARK: -  认证图标
  struct auth {
    static let fr             = "ic_fingerprint"
    static let qr             = "ic_scanning"
    static let otp            = "ic_dynamic"
    static let gesture        = "ic_finger"
    static let face           = "ic_face"
    static let pic_shield     = "pic_shield"
    static let ic_voiceprint  = "ic_voiceprint"
    
  }
  
  // MARK: -  首页
  struct home {
    static let fr             = "ic_fingerprint"
    static let trumpet        = "喇叭icon"
    static let image_advertising = "image_advertising"
    
  }
  
  
  // MARK: -  我的界面
  struct My {
    
    static let authSetting        = "icon_Setting"
    static let aboutUs            = "icon_about"
    static let edit               = "btn_edit"
    static let feedback           = "ic_opinion"
    
    static let Merchants           = "logo_yunchengbao"
    static let bcQr               = "image_weweima"
    static let logo_chengbao      = "logo_chengbao"
    
    static let btn_Unselected     = "btn_Unselected"
    static let btn_Selected       = "btn_Selected"
    static let image_phone        = "image_phone"
    static let ic_update          = "ic_update"
    
  }
  
  // MARK: -   应用相关
  struct app {
    static let add          = "btn_more"
    static let ic_search    = "ic_search"
  }
  
  // MARK: -   应用相关
  struct company {
    static let mycompany    = "ic_enterprise"
    static let ic_sad       = "ic_sad"
    static let ic_certification       = "ic_certification"
    static let ic_current             = "ic_current"
  }
    
    
   // MARK: -   工作台
  struct work {
     static let myWalletImageBg    = "MyMaskBg"
     static let titleImageBg       = "Rectangle"
    
       static let orderManage      = "订单管理"
       static let commodManage     = "商品管理"
       static let marketingManage  = "营销管理"
       static let myBoutique       = "我的精品库"
       static let publicGoods      = "公有商品库"
       static let activityDec      = "平台活动申报"
       static let liveAnnounce     = "直播计划"
       static let liveRoom         = "直播间管理"
       static let anchorManage     = "主播管理"
       static let liveRecording    = "直播记录"
    
      struct live {
          static let liveBgImageView    = "liveBgImageView"
          static let liveProduct        = "live_product"
          static let livePraise         = "live_praise"
          static let livePBg            = "live_pBg"
          static let liveMore           = "live_more"
          static let liveHd             = "live_hd"
          static let liveDaiban         = "live_daiban"
          static let liveActive         = "live_active"
          static let liveCamera         = "live_camera"
          static let liveGift           = "live_gift"
        
       }
    
   }
    
  struct MerCenter {
    
        static let shopsetting         = "shopsetting"
        static let accsetting          = "accsetting"
        static let rolesetting         = "rolesetting"
        static let systemsetting       = "systemsetting"
        static let authinfoMer         = "authinfoMer"
        static let shopHeaderBg        = "shopHeaderBg"
  }
    
 struct MsgCenter {
        static let platformnotice     = "platformnotice"
        static let systeminformation  = "systeminformation"
        static let auctionresult      = "auctionresult"
        static let orderinfo          = "orderinfo"
        static let appraise           = "appraise"
 }
    
}
