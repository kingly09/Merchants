//
//  BYConfigTool.swift
//  Merchants
//
//  Created by kingly09 on 2020/7/14.
//  Copyright © 2020 kingly09. All rights reserved.
//

import UIKit

class BYConfigTool: NSObject {
    // MARK:手机号（国内）
    class func isChinaPhoneLegal(phoneNo: String, areaCode: String = "+86") -> Bool {
        if areaCode != "+86" {
            return false
        }
        let regex = "^[0-9]{11}$"
        if NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: phoneNo) {
            return true
        }
        return false
    }
    
    
    // MARK:邮箱
    class func isEmail(email: String) -> Bool {
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        if NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: email) {
            return true
        }
        return false
    }
    
    // MARK:URL
    class func isURL(url: String) -> Bool {
        print(url)
        return true
//        let regex = "^((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)"
//        if NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: url) {
//            return true
//        }
//        return false
    }
}

extension UIDevice{
    static func deviceId() ->String{
        let uuid = UUID().uuidString.count > 0 ? UUID().uuidString : NSUUID().uuidString
        return uuid
    }
    
    //MARK: - 设备的具体型号
    static func modelName() ->String{

        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
                 
        switch identifier {
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone4"
        case "iPhone4,1":                               return "iPhone4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone5s"
        case "iPhone7,2":                               return "iPhone6"
        case "iPhone7,1":                               return "iPhone6 Plus"
        case "iPhone8,1":                               return "iPhone6s"
        case "iPhone8,2":                               return "iPhone6s Plus"
        case "iPhone8,4":                               return "iPhoneSE"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone7 Plus"
        case "iPhone10,1", "iPhone10,4":                return "iPhone8"
        case "iPhone10,5", "iPhone10,2":                return "iPhone8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhoneX"
        case "iPhone11,2":                              return "iPhoneXS"
        case "iPhone11,6":                              return "iPhoneXS Max"
        case "iPhone11,8":                              return "iPhoneXR"
        case "iPhone12,1":                              return "iPhone11"
        case "iPhone12,3":                              return "iPhone11 Pro"
        case "iPhone12,5":                              return "iPhone11 Pro Max"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
    //MARK: - 获取手机IP地址
    static func getIpAddress() ->String{
        
        var addresses = [String]()
        var ifaddr : UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while (ptr != nil) {
                let flags = Int32(ptr!.pointee.ifa_flags)
                var addr = ptr!.pointee.ifa_addr.pointee
                if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                    if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        if (getnameinfo(&addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                            if let address = String(validatingUTF8:hostname) {
                                addresses.append(address)
                            }
                        }
                    }
                }
                ptr = ptr!.pointee.ifa_next
            }
            freeifaddrs(ifaddr)
        }
        
        return addresses.first ?? "0.0.0.0"
        
    }
    
    
}
