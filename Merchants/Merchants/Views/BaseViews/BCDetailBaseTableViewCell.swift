//
//  BCDetailBaseTableViewCell.swift
//  Merchants
//
//  Created by kingly on2020/11/20.
//  Copyright ©2020 Bambooclound Co., Ltd. All rights reserved.
//

import UIKit
import SnapKit

class BCDetailBaseTableViewCell: BaseTableViewCell {
  
  override func setupUI(){
    self.contentView.backgroundColor = UIColor.white
    
    self.contentView.addSubview(titleLabel)
    self.contentView.addSubview(titleDetailLabel)
    self.contentView.addSubview(rightIcon)
    self.contentView.addSubview(lineView)
    
  }
  
  // MARK: - layout
  override func setupLayout() {
    
    titleLabel.snp.makeConstraints { (make) in
      make.width.equalTo(200)
      make.height.equalToSuperview()
      make.top.equalToSuperview()
      make.left.equalTo(10)
    }
    
    rightIcon.snp.makeConstraints { (make) in
      make.width.equalTo(16)
      make.height.equalTo(16)
      make.centerY.equalToSuperview()
      make.right.equalTo(-10)
    }
    
    titleDetailLabel.snp.makeConstraints { (make) in
      make.width.equalTo(250)
      make.height.equalToSuperview()
      make.top.equalToSuperview()
      make.right.equalTo(rightIcon.snp.left).offset(-15)
    }
    
    lineView.snp.makeConstraints { (make) in
      
      make.height.equalTo(1/SCREEN_SCALE)
      make.bottom.equalToSuperview()
      make.right.equalTo(0)
      make.left.equalToSuperview()
    }
  }
  
  
  lazy var titleLabel : UILabel = {
    var label = UILabel()
    label.font = MBConstant.FONT_15
    label.textColor = UIColor.black
    label.textAlignment = .left
    return label
  }()
  
  lazy var titleDetailLabel : UILabel = {
    var label = UILabel()
    label.font = MBConstant.FONT_15
    label.textColor = UIColor(hex: 0x979797)
    label.textAlignment = .right
    label.numberOfLines = 0
    label.backgroundColor = .clear
    return label
  }()
  
  lazy var rightIcon : UIImageView = {
    var icon = UIImageView()
    icon.image = globalImageWithName(MBImageName.icon.rightArrow)
    return icon
  }()
  
  lazy var lineView : UIView = {
    var view = UIView()
    view.backgroundColor = UIColor.appCellLineColor()
    return view
  }()
  
  var titleDetail : String = "" {
    
    didSet {
      
      self.titleDetailLabel.text = titleDetail
    }
    
  }
  
}
