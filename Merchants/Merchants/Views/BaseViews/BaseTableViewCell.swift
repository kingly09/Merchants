//
//  BaseTableViewCell.swift
//  Merchants
//
//  Created by kingly on2020/9/6.
//  Copyright © 2020年 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

  static public var cellReuseIdentifier : String  {
    let Identifier  = "k\(self)_reuseIdentifier"
    return Identifier
  }
  
  /// cell的高度
  var tableViewCellHight : CGFloat =  0.0
  
  //MARK:  init
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?){
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    self.backgroundColor =  UIColor.white
    self.setupUI()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    self.backgroundColor =  UIColor.white
  }
  
  deinit {
    print("\(type(of: self))： deinit")
  }
  
  /// 子类重写
  public func setupUI(){
    
    
    
  }
  
  // MARK: - layout
  override func layoutSubviews() {
    super.layoutSubviews()
    setupLayout()
  }
  
  /// 子类重些
  public  func setupLayout() {
    
  }

}
