//
//  MBMsgCenterTableViewCell.swift
//  Merchants
//
//  Created by kingly on 2020/7/20.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

let  KMBMsgCenterTableViewCellHight = 66.0


class MBMsgCenterTableViewCell: BCDetailBaseTableViewCell {

    deinit {
        print("deinit:: MBMsgCenterTableViewCell")
      }
      
      override func setupUI(){
        super.setupUI()
        
        self.contentView.addSubview(logoImageView)
      //  logoImageView.addSubview(xiaoHongdianView)
        logoImageView.addSubview(numLabel)
        
        self.contentView.addSubview(timeLabel)
    
      }
        
      fileprivate lazy var logoImageView: UIImageView = {
           var logoImageView = UIImageView()
           return logoImageView
       }()
    
      fileprivate lazy var timeLabel : UILabel = {
       var label = UILabel()
       label.font = MBConstant.FONT_10
       label.textColor = UIColor.black
       label.textAlignment = .right
       return label
     }()

//     fileprivate lazy var xiaoHongdianView: UIView = {
//         var view = UIView()
//         view.backgroundColor = UIColor(hexString: "FD3A39")
//         return view
//     }()
//
    fileprivate lazy var numLabel : UILabel = {
       var label = UILabel()
       label.font = MBConstant.FONT_12
       label.textColor =  UIColor(hexString: "AAAAAA")
       label.textAlignment = .center
       return label
     }()
     
      // MARK: - layout
      override func setupLayout() {
        
        logoImageView.snp.makeConstraints { (make) in
             make.top.equalTo((CGFloat(KMBMsgCenterTableViewCellHight) - 50)/2)
             make.left.equalTo(20)
             make.width.equalTo(50)
             make.height.equalTo(50)
        }
        logoImageView.layer.masksToBounds = true
        logoImageView.layer.cornerRadius = 25.0
       
        numLabel.snp.makeConstraints { (make) in
             make.top.equalTo(-8)
             make.right.equalTo(8)
             make.height.equalTo(16)
             make.width.equalTo(80)
        }
        numLabel.layer.masksToBounds = true
        numLabel.layer.cornerRadius = 8.0
    
        titleLabel.snp.makeConstraints { (make) in
             make.top.equalTo(14)
             make.left.equalTo(logoImageView.snp.right).offset(12)
             make.right.equalTo(-100)
             make.height.equalTo(20)
        }
        titleLabel.textAlignment = .left
        
        titleDetailLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(2)
            make.left.equalTo(82)
            make.right.equalTo(-20)
            make.height.equalTo(18)
        }
        titleDetailLabel.textAlignment = .left
        
        numLabel.snp.makeConstraints { (make) in
            make.top.equalTo(15)
            make.right.equalTo(-20)
            make.width.equalTo(100)
            make.height.equalTo(18)
        }
        
        lineView.snp.makeConstraints { (make) in
             make.height.equalTo(1/SCREEN_SCALE)
             make.bottom.equalToSuperview()
             make.right.equalTo(0)
             make.left.equalToSuperview()
         }
      }
      
      var settingModel: MBSettingModel? {
        
        didSet {
          
          logoImageView.image =   globalImageWithName(settingModel!.imageName)
            
          titleLabel.text = settingModel?.title
          
          if  settingModel?.detail.ky_stringLength() == 0 {
            titleDetailLabel.text = ""
          }else{
            titleDetailLabel.text = settingModel?.detail
          }
          
          numLabel.text = "7月6号 16：09"
            
        }
      }

      
    }
