//
//  MBLiveAnnounceCell.swift
//  Merchants
//
//  Created by kingly on 2020/7/28.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

let kMBLiveAnnounceCellHight = 234.0

class MBLiveAnnounceCell: BaseTableViewCell {
    
    deinit {
        print("MBLiveAnnounceCell deinit")
    }
    
    override func setupUI(){
        
        self.contentView.addSubview(LiveView)
        self.contentView.backgroundColor = UIColor(hexStr: "F3F3F3")
        
        self.contentView.addSubview(LivebgImageView)
        LivebgImageView.addSubview(logoImageView)
        LivebgImageView.addSubview(nameLabel)
        LivebgImageView.addSubview(nameNoteLabel)
        
        self.contentView.addSubview(titileLabel)
        self.contentView.addSubview(opentimeLabel)
        
    }
    
    // MARK: - layout
    override func setupLayout() {
        
        LiveView.snp.makeConstraints { (make) in
          make.top.equalTo(0)
          make.left.equalTo(15)
          make.right.equalTo(-15)
          make.height.equalTo(kMBLiveAnnounceCellHight)
        }
        
        LiveView.layer.cornerRadius = 8.0
        
        logoImageView.snp.makeConstraints { (make) in
          make.top.equalTo(10)
          make.left.equalTo(20)
          make.width.equalTo(25)
          make.height.equalTo(25)
        }
        
        nameLabel.snp.makeConstraints { (make) in
          make.top.equalTo(0)
          make.left.equalTo(logoImageView.snp.right).offset(4)
          make.right.equalTo(-20)
          make.height.equalTo(18)
        }
        
        nameNoteLabel.snp.makeConstraints { (make) in
          make.bottom.equalTo(-10)
          make.left.equalTo(15)
          make.right.equalTo(-20)
          make.height.equalTo(20)
        }
        
        titileLabel.snp.makeConstraints { (make) in
          make.bottom.equalTo(-35)
          make.left.equalTo(20)
          make.right.equalTo(-20)
          make.height.equalTo(18)
        }
        
        opentimeLabel.snp.makeConstraints { (make) in
          make.bottom.equalTo(-15)
          make.left.equalTo(20)
          make.right.equalTo(-20)
          make.height.equalTo(16)
        }
        
        
        
    }
    
    /// 直播背景
    fileprivate lazy var LiveView: UIView = {
        var View = UIView()
        View.backgroundColor = .white
        return View
    }()
    
    
    /// 直播背景
    fileprivate lazy var LivebgImageView: UIImageView = {
        var ImageView = UIImageView()
        ImageView.image = globalImageWithName(MBImageName.defaultImageName.defaltHeadlogo)
        return ImageView
    }()
    
    /// 直播logo
    fileprivate lazy var logoImageView: UIImageView = {
        var logoImageView = UIImageView()
        logoImageView.image = globalImageWithName(MBImageName.defaultImageName.defaltHeadlogo)
        return logoImageView
    }()
    
    fileprivate lazy var nameLabel: UILabel = {
        var titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 12)
        titleLabel.textColor = UIColor(hexStr: "ffffff")
        titleLabel.textAlignment = .left
        titleLabel.text = "四会翡翠代购"
        return titleLabel
    }()
    
    fileprivate lazy var nameNoteLabel: UILabel = {
        var titleLabel = UILabel()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        titleLabel.textColor = UIColor(hexStr: "ffffff")
        titleLabel.textAlignment = .left
        titleLabel.text = "关注有礼 代购减半"
        return titleLabel
    }()
    
    fileprivate lazy var titileLabel: UILabel = {
        var titleLabel = UILabel()
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        titleLabel.textColor = UIColor(hexStr: "333333")
        titleLabel.textAlignment = .left
        titleLabel.text = "五周年庆 更多优惠等你来拿"
        return titleLabel
    }()
    
    fileprivate lazy var opentimeLabel: UILabel = {
        var titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 12)
        titleLabel.textColor = UIColor(hexStr: "999999")
        titleLabel.textAlignment = .left
        titleLabel.text = "开播时间:2020-07-06 16:3"
        return titleLabel
    }()
}
