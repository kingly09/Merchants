//
//  MBLiveProductCollectionCell.swift
//  Merchants
//
//  Created by kingly on 2020/7/27.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import YYText

let kMBLiveProductCollectionCellHight = 148.0

class MBLiveProductCollectionCell: UICollectionViewCell {
    
    static let cellWithReuseIdentifier = "kMBLiveProductCollectionCell"
    
    /// 初始化
    ///
    /// - Parameter frame: frame
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupCustomView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCustomView(){
        self.contentView.backgroundColor =  .white
        
        self.contentView.addSubview(logoImageView)
        self.contentView.addSubview(titleLabel)
        
        self.contentView.addSubview(priceLabel)
        self.contentView.addSubview(jxpriceLabel)
        
        self.contentView.addSubview(UserInfoSLineView)
        self.contentView.addSubview(liveNumberLabel)
        
        self.contentView.addSubview(editBtn)
        
        
        
    }
    
    // MARK: - 布局
    override func layoutSubviews() {
        super.layoutSubviews()
        
        logoImageView.snp.makeConstraints { (make) in
            make.top.equalTo(13)
            make.left.equalTo(13)
            make.right.equalTo(90)
            make.height.equalTo(90)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(20)
            make.left.equalTo(logoImageView.snp.right).offset(10)
            make.right.equalTo(-10)
            make.height.equalTo(34)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(12)
            make.left.equalTo(logoImageView.snp.right).offset(10)
            make.right.equalTo(-10)
            make.height.equalTo(16)
        }
        
        jxpriceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(priceLabel.snp.bottom).offset(12)
            make.left.equalTo(logoImageView.snp.right).offset(10)
            make.right.equalTo(-10)
            make.height.equalTo(16)
        }
        
        UserInfoSLineView.snp.makeConstraints { (make) in
                   make.bottom.equalTo(-35)
                   make.left.equalTo(0)
                   make.width.equalToSuperview()
                   make.height.equalTo(1)
        }
        
        liveNumberLabel.snp.makeConstraints { (make) in
                   make.bottom.equalTo(10)
                   make.left.equalTo(10)
                   make.right.equalTo(83)
                   make.height.equalTo(15)
        }
        
        editBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(7)
            make.right.equalTo(-10)
            make.width.equalTo(63)
            make.height.equalTo(22)
        }
        editBtn.layer.borderColor = UIColor(hexStr: "E69230").cgColor;
        editBtn.layer.borderWidth = 1;
        
    }
    // MARK: - 懒加载
    fileprivate lazy var logoImageView: UIImageView = {
        var imageView  = UIImageView()
        return imageView
    }()
    
    fileprivate lazy var  titleLabel : YYLabel = {
        var stateLab  =  YYLabel()
        let textStr = "商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题" as NSString
        let text = NSMutableAttributedString(string: textStr as String)
        text.yy_font = PFSC_Light(size: 12)
        text.yy_color = UIColor(hexStr: "333333")
        stateLab.attributedText = text
        stateLab.numberOfLines = 2
        return stateLab
    }()
    
    fileprivate lazy var  priceLabel : YYLabel = {
        var stateLab  =  YYLabel()
        let textStr = "￥2920.00" as NSString
        let text = NSMutableAttributedString(string: textStr as String)
        text.yy_font = PFSC_Light(size: 12)
        text.yy_color = UIColor(hexStr: "FF3B3B")
        
        stateLab.attributedText = text
        return stateLab
    }()
    
    fileprivate lazy var  jxpriceLabel : YYLabel = {
        var stateLab  =  YYLabel()
        let textStr = "（精选库价¥2000.00）" as NSString
        let text = NSMutableAttributedString(string: textStr as String)
        text.yy_font = PFSC_Light(size: 10)
        text.yy_color = UIColor(hexStr: "FF3B3B")
        
        stateLab.attributedText = text
        return stateLab
    }()
    
    fileprivate lazy var  UserInfoSLineView : UIView = {
        var view = UIView()
        view.backgroundColor = UIColor(hexStr: "EEEEEE")
        return view
    }()
    
    fileprivate lazy var  liveNumberLabel  : YYLabel = {
           var stateLab  =  YYLabel()
           let textStr = "直播货号：20492923  直播指令“1”" as NSString
           let text = NSMutableAttributedString(string: textStr as String)
           text.yy_font = PFSC_Light(size: 10)
           text.yy_color = UIColor(hexStr: "FF3B3B")
           stateLab.attributedText = text
           return stateLab
     }()
    
    fileprivate lazy var editBtn: UIButton = {
         var button = UIButton()
         button.backgroundColor = UIColor.white
       
         button.setTitleColor(UIColor(hexStr: "E69230"), for: .normal)
         button.setTitleColor(UIColor(hexStr: "666666"), for:.highlighted) //触摸高亮状态下文字的颜色
         
         button.adjustsImageWhenDisabled = true
         button.adjustsImageWhenHighlighted = true
         button.setTitle("编辑",for: .normal)
           
         button.titleLabel?.font  = MBConstant.FONT_10
         
         button.addTarget(self, action: #selector(editBtnDidClick(sender:)), for: .touchUpInside)
         return button
       }()
       

}


extension MBLiveProductCollectionCell {

 
    @objc func editBtnDidClick(sender: UIButton) {
     
   }

}
