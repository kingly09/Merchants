//
//  MBLiveOrderCollectionViewCell.swift
//  Merchants
//
//  Created by kingly on 2020/7/25.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import YYText

let kMBLiveOrderCollectionViewCellHight = 172.0


class MBLiveOrderCollectionViewCell: UICollectionViewCell {
    
    static let cellWithReuseIdentifier = "kMBLiveOrderCollectionViewCell"
    
    
    /// 初始化
    ///
    /// - Parameter frame: frame
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupCustomView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCustomView(){
        
        self.contentView.backgroundColor =  .white
        
        self.contentView.addSubview(logoImageView)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(priceLabel)
        self.contentView.addSubview(UserInfoSLineView)
        self.contentView.addSubview(totalpriceLabel)
        self.contentView.addSubview(orderstatusLabel)
        
        
    }
    
    // MARK: - 布局
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.contentView.layer.cornerRadius = 4.0
        
        logoImageView.snp.makeConstraints { (make) in
            make.top.equalTo(15)
            make.left.equalTo(10)
            make.width.equalTo(105)
            make.height.equalTo(105)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(30)
            make.left.equalTo(logoImageView.snp.right).offset(12)
            make.right.equalTo(-10)
            make.height.equalTo(34)
        }
        
        priceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(24)
            make.left.equalTo(logoImageView.snp.right).offset(12)
            make.right.equalTo(-10)
            make.height.equalTo(15)
        }
        UserInfoSLineView.snp.makeConstraints { (make) in
            make.bottom.equalTo(-35)
            make.left.equalTo(0)
            make.width.equalTo(kScreenWidth)
            make.height.equalTo(1)
        }
        
        totalpriceLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(-8)
            make.left.equalTo(26)
            make.width.equalTo(kScreenWidth/2)
            make.height.equalTo(20)
        }
        
        orderstatusLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(-8)
            make.right.equalTo(-10)
            make.width.equalTo(kScreenWidth/2)
            make.height.equalTo(20)
        }
        
        
    }
    
    // MARK: - 懒加载
    fileprivate lazy var logoImageView: UIImageView = {
        var imageView  = UIImageView()
        return imageView
    }()
    
    fileprivate lazy var  titleLabel : YYLabel = {
        var stateLab  =  YYLabel()
        let textStr = "商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题商品标题" as NSString
        let text = NSMutableAttributedString(string: textStr as String)
        text.yy_font = PFSC_Light(size: 12)
        text.yy_color = UIColor(hexStr: "333333")
        stateLab.attributedText = text
        stateLab.numberOfLines = 2
        return stateLab
    }()
    
    fileprivate lazy var  priceLabel : YYLabel = {
        var stateLab  =  YYLabel()
        let textStr = "￥6000+鉴定宝服务 ￥1000" as NSString
        let text = NSMutableAttributedString(string: textStr as String)
        text.yy_font = PFSC_Light(size: 12)
        text.yy_color = UIColor(hexStr: "333333")
        
        stateLab.numberOfLines = 2
        let highLightText1 = "￥6000"
        text.yy_setTextHighlight(textStr.range(of: highLightText1), color: UIColor(hexStr: "FF3B3B"), backgroundColor: nil) {(view, text, range, rect) in
            
        }
        stateLab.attributedText = text
        return stateLab
    }()
    
    fileprivate lazy var  UserInfoSLineView : UIView = {
        var view = UIView()
        view.backgroundColor = UIColor(hexStr: "EEEEEE")
        return view
    }()
    
    fileprivate lazy var  totalpriceLabel : YYLabel = {
        var stateLab  =  YYLabel()
        let textStr = "合计￥13000" as NSString
        let text = NSMutableAttributedString(string: textStr as String)
        text.yy_font = PFSC_Light(size: 12)
        text.yy_color = UIColor(hexStr: "333333")
        
        stateLab.numberOfLines = 2
        let highLightText1 = "￥13000"
        text.yy_setTextHighlight(textStr.range(of: highLightText1), color: UIColor(hexStr: "FF3B3B"), backgroundColor: nil) {(view, text, range, rect) in
            
        }
        stateLab.attributedText = text
        return stateLab
    }()
    
    fileprivate lazy var  orderstatusLabel : UILabel = {
        var Label = UILabel()
        Label.text = "待用户确认订单"
        Label.textAlignment = .right
        Label.textColor  = UIColor(hexStr: "E69230")
        Label.font = MBConstant.FONT_12
        
        return Label
    }()
    
    
    
}
