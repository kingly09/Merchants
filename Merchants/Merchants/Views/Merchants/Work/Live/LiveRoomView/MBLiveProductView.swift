//
//  MBLiveProductView.swift
//  Merchants
//
//  Created by kingly on 2020/7/27.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import YYText

class MBLiveProductView: UIView {

    var collectionView : UICollectionView?
    
     override init(frame: CGRect) {
           super.init(frame: frame)
           
           setupCustomView()
           setuplayout()
           
       }
       
       required init?(coder aDecoder: NSCoder) {
           fatalError("init(coder:) has not been implemented")
       }
       
       private func setupCustomView() {
           
           self.isUserInteractionEnabled = true
           self.backgroundColor = UIColor(hexStr: "000000", alpha: 0.3)
        
           let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(singleTapGestureClick))
           self.addGestureRecognizer(singleTapGesture)
        
       
            // 初始化
                   let layout = UICollectionViewFlowLayout.init()
                   layout.itemSize = CGSize(width: CGFloat(SCREEN_WIDTH), height: CGFloat(kMBLiveOrderViewCollectionViewHight))
                   layout.minimumLineSpacing = 0
                   layout.minimumInteritemSpacing = 0
                   layout.scrollDirection = .vertical
                   layout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
                   
                   collectionView = UICollectionView.init(frame: CGRect(x:0, y: 57.0, width: CGFloat(SCREEN_WIDTH), height: CGFloat(kMBLiveOrderViewCollectionViewHight)), collectionViewLayout: layout)
                   collectionView?.backgroundColor = UIColor(hexStr: "F3F3F3")
                   collectionView?.delegate = self
                   collectionView?.dataSource = self
                   collectionView?.register(MBLiveOrderCollectionViewCell.self, forCellWithReuseIdentifier: MBLiveOrderCollectionViewCell.cellWithReuseIdentifier)
                   collectionView?.isUserInteractionEnabled = true
                   self.addSubview(collectionView!)
          
            
      }
    
    
    
    // MARK: - layout
    private func setuplayout() {
        
        
    }
    
    fileprivate lazy var titleView: UIView = {
        var titleView = UIView()
        titleView.backgroundColor = UIColor(hexStr: "ffffff")
        titleView.isUserInteractionEnabled = true
        return titleView
    }()
    
    fileprivate lazy var  priceLabel : YYLabel = {
        var stateLab  =  YYLabel()
        let textStr = "共 2 件宝贝" as NSString
        let text = NSMutableAttributedString(string: textStr as String)
        text.yy_font = PFSC_Light(size: 12)
        text.yy_color = UIColor(hexStr: "333333")
        
        stateLab.numberOfLines = 1
        let highLightText1 = "2"
        text.yy_setTextHighlight(textStr.range(of: highLightText1), color: UIColor(hexStr: "FF3B3B"), backgroundColor: nil) {(view, text, range, rect) in
            
        }
        stateLab.attributedText = text
        return stateLab
    }()
    
    
    fileprivate lazy var addProductLabel: UILabel = {
           var Label = UILabel()
           Label.text = "添加商品"
           Label.textAlignment = .center
           Label.textColor  = UIColor(hexStr: "E49230")
           
           Label.font = MBConstant.FONT_14
           Label.isUserInteractionEnabled = true
           let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(addProductLabelClick))
           Label.addGestureRecognizer(singleTapGesture)
           return Label
       }()
    
    
}

extension MBLiveProductView {
    
    @objc func singleTapGestureClick()  {
         
        self.isHidden = true
    }
    
    @objc func addProductLabelClick() {
        
    }
   
}

extension MBLiveProductView :  UICollectionViewDataSource, UICollectionViewDelegate {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 2
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

    let cell : MBLiveOrderCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: MBLiveOrderCollectionViewCell.cellWithReuseIdentifier, for: indexPath) as! MBLiveOrderCollectionViewCell
   return  cell
   
  }
  
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

  }
  
  func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {

  }
  
 

}
