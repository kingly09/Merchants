//
//  MBLiveSubmitOrderView.swift
//  Merchants
//
//  Created by kingly on 2020/7/27.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

class MBLiveSubmitOrderView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCustomView()
        setuplayout()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCustomView() {
        
        self.isUserInteractionEnabled = true
        self.backgroundColor = UIColor(hexStr: "000000", alpha: 0.3)
        
    

    }
    
    
    
    // MARK: - layout
    private func setuplayout() {
        
        
    }
    
    fileprivate lazy var UserInfoAllContentView : UIView = {
        var view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = .clear
        return view
    }()
    
    fileprivate lazy var  UserInfoContentTopImageView : UIImageView = {
        var imageView = UIImageView()
        imageView.isUserInteractionEnabled = true
        imageView.image = globalImageWithName(MBImageName.work.live.livePBg)
        return imageView
    }()
    
    
    fileprivate lazy var resetOrderView: UIView = {
        var resetOrderView = UIView()
        resetOrderView.backgroundColor = UIColor(hexStr: "000000", alpha: 0.5)
        resetOrderView.isUserInteractionEnabled = true
        return resetOrderView
    }()
    
    fileprivate lazy var resetOrderLabel: UILabel = {
        var Label = UILabel()
        Label.text = "重新截图"
        Label.textAlignment = .center
        Label.textColor  = UIColor(hexStr: "ffffff")
        
        Label.font = MBConstant.FONT_14
        Label.isUserInteractionEnabled = true
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(resetOrderLabelClick))
        Label.addGestureRecognizer(singleTapGesture)
        return Label
    }()
    
    fileprivate lazy var OrderInfoView: UIView = {
        var OrderInfoView = UIView()
        OrderInfoView.backgroundColor = .white
        OrderInfoView.isUserInteractionEnabled = true
        return OrderInfoView
    }()
    
    fileprivate lazy var OrderTitleLabel: UILabel = {
        var Label = UILabel()
        Label.text = "#直播#28137485"
        Label.textAlignment = .left
        Label.textColor  = UIColor(hexStr: "333333")
        Label.font = MBConstant.FONT_12
        Label.isUserInteractionEnabled = true
        return Label
    }()
    
    
    fileprivate lazy var OrderPriceTextField: BYUITextField = {
        var TextField = BYUITextField()
        TextField.placeholder = "¥ 请输入宝贝价格"
        TextField.font = PFSC_Regular(size: 14)
        TextField.placeholderFont = PFSC_Regular(size: 12)
        TextField.textColor = kColor_textBlack
        TextField.keyboardType = .phonePad
        TextField.placeholderColor = KColor(hexStr: "cccccc")
        TextField.lineColor = kColor_lineGrayColor
        TextField.showLine = true
        TextField.maxLength = 12
        TextField.leftSpace = 3
        TextField.clearButtonMode = .whileEditing
        return TextField
    }()
    
    fileprivate lazy var OrdertimeView: UIView = {
        var view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = .clear
        return view
    }()
    
    fileprivate lazy var OrderTimeLabel: UILabel = {
           var Label = UILabel()
           Label.text = "订单有效期"
           Label.textAlignment = .left
           Label.textColor  = UIColor(hexStr: "333333")
           Label.font = MBConstant.FONT_12
           Label.isUserInteractionEnabled = true
           return Label
    }()
    
    fileprivate lazy var OrderTimeNoteLabel: UILabel = {
           var Label = UILabel()
           Label.text = "有效期过后自动取消"
           Label.textAlignment = .left
           Label.textColor  = UIColor(hexStr: "AAAAAA")
           Label.font = MBConstant.FONT_12
           Label.isUserInteractionEnabled = true
           return Label
    }()
    
    fileprivate lazy var  rightImageView : UIImageView = {
           var imageView = UIImageView()
           imageView.isUserInteractionEnabled = true
           imageView.image = globalImageWithName(MBImageName.icon.rightArrow)
           return imageView
    }()
    
    
   fileprivate lazy var OrdetypeListbtnView: UIView = {
       var view = UIView()
       view.isUserInteractionEnabled = true
       view.backgroundColor = .clear
       return view
   }()
    
   
    
    
    
    
    
}

extension MBLiveSubmitOrderView {
   
    @objc func resetOrderLabelClick() {
        
    }
}
