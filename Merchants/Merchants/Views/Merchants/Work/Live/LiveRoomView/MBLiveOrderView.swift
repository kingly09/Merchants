//
//  MBLiveOrderView.swift
//  Merchants
//
//  Created by kingly on 2020/7/25.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

let kMBLiveOrderViewCollectionViewHight = 473.0

class MBLiveOrderView: UIView {
    
     var collectionView : UICollectionView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCustomView()
        setuplayout()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCustomView() {
        
        self.isUserInteractionEnabled = true
        self.backgroundColor = UIColor(hexStr: "000000", alpha: 0.3)
        
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(singleTapGestureClick))
        self.addGestureRecognizer(singleTapGesture)
        
        self.addSubview(titleView)
        titleView.addSubview(titleLabel)
        
        // 初始化
        let layout = UICollectionViewFlowLayout.init()
        layout.itemSize = CGSize(width: CGFloat(SCREEN_WIDTH), height: CGFloat(kMBLiveOrderViewCollectionViewHight))
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        collectionView = UICollectionView.init(frame: CGRect(x:0, y: 57.0, width: CGFloat(SCREEN_WIDTH), height: CGFloat(kMBLiveOrderViewCollectionViewHight)), collectionViewLayout: layout)
        collectionView?.backgroundColor = UIColor(hexStr: "F3F3F3")
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.register(MBLiveOrderCollectionViewCell.self, forCellWithReuseIdentifier: MBLiveOrderCollectionViewCell.cellWithReuseIdentifier)
        collectionView?.isUserInteractionEnabled = true
        self.addSubview(collectionView!)
        
        
        
    }
    
    // MARK: - layout
    private func setuplayout() {
        
        titleView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.width.equalTo(SCREEN_WIDTH)
            make.height.equalTo(57.0)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(22)
            make.left.equalTo(25)
            make.right.equalTo(-25)
            make.height.equalTo(20)
        }
    }
    
    
    fileprivate lazy var titleView: UIView = {
        var titleView = UIView()
        titleView.backgroundColor = UIColor(hexStr: "ffffff")
        titleView.isUserInteractionEnabled = true
        return titleView
    }()
    
    fileprivate lazy var titleLabel  : UILabel = {
           var titleLabel = UILabel()
           titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
           titleLabel.textColor = UIColor(hexStr: "666666")
           titleLabel.textAlignment = .left
           titleLabel.text = "直播间订单"
           return titleLabel
    }()
    
    
    
    
}

extension MBLiveOrderView :  UICollectionViewDataSource, UICollectionViewDelegate {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 2
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

    let cell : MBLiveOrderCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: MBLiveOrderCollectionViewCell.cellWithReuseIdentifier, for: indexPath) as! MBLiveOrderCollectionViewCell
   return  cell
   
  }
  
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

  }
  
  func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {

  }
  
 

}

extension MBLiveOrderView {
    
    @objc func singleTapGestureClick()  {
         
        self.isHidden = true
    }
    
    @objc func hideLiveOrderView()  {
           
    }
    
    @objc func showLiveOrderView()  {
        
    }
    
}
