//
//  MBLiveUserInfoView.swift
//  Merchants
//
//  Created by kingly on 2020/7/23.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

class MBLiveUserInfoView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCustomView()
        setuplayout()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setupCustomView() {
        
        self.isUserInteractionEnabled = true
        self.backgroundColor = UIColor.clear
        
        self.addSubview(UserInfoView)
        
        UserInfoView.addSubview(UserInfoAllContentView)
        
        UserInfoAllContentView.addSubview(UserInfoContentView)
        UserInfoAllContentView.addSubview(closeBtn)
        
        UserInfoContentView.addSubview(UserInfoContentTopImageView)
        UserInfoContentView.addSubview(UserInfoContentTopView)
        
       
      
        
        UserInfoContentTopView.addSubview(UserLogoImageView)
        
        UserInfoContentTopView.addSubview(UserNameLabel)
        
        UserInfoContentTopView.addSubview(FollowLabel)
        
        UserInfoContentTopView.addSubview(UserInfoLineView)
        
        UserInfoContentTopView.addSubview(UserOrderLabel)
        
        UserInfoContentTopView.addSubview(UserfavourableLabel)
        
        UserInfoContentTopView.addSubview(UserInfoSLineView)
        
        
        /// 中间部分
        UserInfoContentView.addSubview(PaymentAmountLabel)
        
        UserInfoContentView.addSubview(PaymentAmountNoteLabel)
        
        UserInfoContentView.addSubview(RefundAmountLabel)
               
        UserInfoContentView.addSubview(RefundAmountNoteLabel)
        
        UserInfoContentView.addSubview(PaymentNumLabel)
               
        UserInfoContentView.addSubview(PaymentNumNoteLabel)
        
        UserInfoContentView.addSubview(RefundNumLabel)
                    
        UserInfoContentView.addSubview(RefundNumNoteLabel)
        
        
        UserInfoContentView.addSubview(createOrderView)
        
        UserInfoContentView.addSubview(createOrderLabel)
        
        
        ///底部部分
        UserInfoContentView.addSubview(UserInfofootLineView)
        
        UserInfoContentView.addSubview(prohibitBtn)
        
        UserInfoContentView.addSubview(atBtn)
        
        UserInfoContentView.addSubview(daibanBtn)
        
    }
    // MARK: - layout
    private func setuplayout() {
        
        UserInfoView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.right.equalTo(0)
            make.left.equalTo(0)
            make.width.equalTo(kScreenWidth)
            make.height.equalTo(kScreenHeight)
        }
        
        UserInfoAllContentView.snp.makeConstraints { (make) in
            make.width.equalTo(298)
            make.height.equalTo(410)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    
        closeBtn.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(25)
            make.height.equalTo(25)
        }
        
        UserInfoContentView.snp.makeConstraints { (make) in
            make.top.equalTo(32)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(298)
            make.height.equalTo(378)
        }
        
        
        UserInfoContentTopImageView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(298)
            make.height.equalTo(87)
        }
        
        
        UserInfoContentTopView.snp.makeConstraints { (make) in
            make.top.equalTo(18)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(115)
        }
      
        UserInfoContentTopView.layer.cornerRadius  = 4
        // shadowCode
        UserInfoContentTopView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05).cgColor
        UserInfoContentTopView.layer.shadowOffset = CGSize(width: 0, height: 2)
        UserInfoContentTopView.layer.shadowOpacity = 1
        UserInfoContentTopView.layer.shadowRadius = 10
        
        UserLogoImageView.snp.makeConstraints { (make) in
              make.top.equalTo(14)
              make.left.equalTo(17)
              make.width.equalTo(50)
              make.height.equalTo(50)
          }
        
        UserLogoImageView.layer.cornerRadius  = 25
       
        UserNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(19)
            make.left.equalTo(UserLogoImageView.snp.right).offset(17)
            make.right.equalTo(-17)
            make.height.equalTo(20)
        }
        
        FollowLabel.snp.makeConstraints { (make) in
            make.top.equalTo(UserNameLabel.snp.bottom).offset(3)
            make.left.equalTo(UserLogoImageView.snp.right).offset(17)
            make.right.equalTo(-17)
            make.height.equalTo(17)
        }
        
        UserInfoLineView.snp.makeConstraints { (make) in
            make.bottom.equalTo(-36)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(1)
        }
        
        UserOrderLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.width.equalTo(129)
            make.height.equalTo(35)
        }
        
        
        UserfavourableLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(0)
            make.left.equalTo(129)
            make.width.equalTo(129)
            make.height.equalTo(35)
        }
        
        UserInfoSLineView.snp.makeConstraints { (make) in
            make.bottom.equalTo(-13)
            make.left.equalTo(129)
            make.width.equalTo(1)
            make.height.equalTo(10)
        }
        
        
        PaymentAmountLabel.snp.makeConstraints { (make) in
            make.top.equalTo(UserInfoContentTopView.snp.bottom).offset(20)
            make.left.equalTo(20)
            make.width.equalTo(129)
            make.height.equalTo(17)
        }
        
        PaymentAmountNoteLabel.snp.makeConstraints { (make) in
            make.top.equalTo(PaymentAmountLabel.snp.bottom).offset(6)
            make.left.equalTo(20)
            make.width.equalTo(129)
            make.height.equalTo(17)
        }
        
        RefundAmountLabel.snp.makeConstraints { (make) in
            make.top.equalTo(UserInfoContentTopView.snp.bottom).offset(20)
            make.right.equalTo(-20)
            make.width.equalTo(129)
            make.height.equalTo(17)
        }
        
        RefundAmountNoteLabel.snp.makeConstraints { (make) in
            make.top.equalTo(RefundAmountLabel.snp.bottom).offset(6)
            make.right.equalTo(-20)
            make.width.equalTo(129)
            make.height.equalTo(17)
        }
        
        PaymentNumLabel.snp.makeConstraints { (make) in
            make.top.equalTo(PaymentAmountNoteLabel.snp.bottom).offset(18)
            make.left.equalTo(20)
            make.width.equalTo(129)
            make.height.equalTo(17)
        }
        PaymentNumNoteLabel.snp.makeConstraints { (make) in
            make.top.equalTo(PaymentNumLabel.snp.bottom).offset(6)
            make.left.equalTo(20)
            make.width.equalTo(129)
            make.height.equalTo(17)
        }
        
        RefundNumLabel.snp.makeConstraints { (make) in
            make.top.equalTo(RefundAmountNoteLabel.snp.bottom).offset(18)
            make.right.equalTo(-20)
            make.width.equalTo(129)
            make.height.equalTo(17)
        }
        
        RefundNumNoteLabel.snp.makeConstraints { (make) in
            make.top.equalTo(RefundNumLabel.snp.bottom).offset(6)
            make.right.equalTo(-20)
            make.width.equalTo(129)
            make.height.equalTo(17)
        }
        
        createOrderView.snp.makeConstraints { (make) in
            make.top.equalTo(RefundNumNoteLabel.snp.bottom).offset(20)
            make.right.equalTo(-20)
            make.left.equalTo(20)
            make.height.equalTo(34)
        }
        
        // fillCode
        let bgLayer1 = CAGradientLayer()
        bgLayer1.colors = [UIColor(hexStr: "F2AE52").cgColor, UIColor(hexStr: "E69230").cgColor]
        bgLayer1.locations = [0, 1]
        bgLayer1.frame =  CGRect(x: 0, y: 0, width: 258, height: 34)
        bgLayer1.startPoint = CGPoint(x: 0, y: 0)
        bgLayer1.endPoint = CGPoint(x: 1, y: 1)
        
        createOrderView.layer.addSublayer(bgLayer1)
        createOrderView.layer.cornerRadius = 4
        
        createOrderLabel.snp.makeConstraints { (make) in
            make.top.equalTo(RefundNumNoteLabel.snp.bottom).offset(20)
            make.right.equalTo(-20)
            make.left.equalTo(20)
            make.height.equalTo(34)
        }
        
        UserInfofootLineView.snp.makeConstraints { (make) in
              make.bottom.equalTo(-50)
              make.right.equalTo(0)
              make.left.equalTo(0)
              make.height.equalTo(1)
        }
        
        prohibitBtn.snp.makeConstraints { (make) in
              make.bottom.equalTo(-14)
              make.left.equalTo(32)
              make.width.equalTo(46)
              make.height.equalTo(22)
        }
        prohibitBtn.layer.borderColor = UIColor(hexStr: "E69230").cgColor;
        prohibitBtn.layer.borderWidth = 1;
   
        
        daibanBtn.snp.makeConstraints { (make) in
              make.bottom.equalTo(-14)
              make.right.equalTo(-32)
              make.width.equalTo(46)
              make.height.equalTo(22)
        }
        
        daibanBtn.layer.borderColor = UIColor(hexStr: "E69230").cgColor;
        daibanBtn.layer.borderWidth = 1;
        
        
        atBtn.snp.makeConstraints { (make) in
              make.bottom.equalTo(-14)
              make.centerX.equalToSuperview()
              make.width.equalTo(46)
              make.height.equalTo(22)
        }
        atBtn.layer.borderColor = UIColor(hexStr: "E69230").cgColor;
        atBtn.layer.borderWidth = 1;
    }
    
    fileprivate lazy var UserInfoView : UIView = {
        var view = UIView()
        view.backgroundColor = UIColor(hexStr: "000000", alpha: 0.3)
        view.isUserInteractionEnabled = true
        return view
    }()
    
    fileprivate lazy var UserInfoAllContentView : UIView = {
      var view = UIView()
      view.isUserInteractionEnabled = true
      view.backgroundColor = .clear
      return view
   }()
    
    fileprivate lazy var UserInfoContentView : UIView = {
        var view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = .white
        return view
    }()
    
    fileprivate lazy var closeBtn: UIButton = {
          var closeBtn = UIButton().ky_custombutton(imageName:MBImageName.icon.backclose)
          closeBtn.addTarget(self, action: #selector(closeBtnDidClick), for: .touchUpInside)
          return closeBtn
      }()
    
    fileprivate lazy var  UserInfoContentTopImageView : UIImageView = {
        var imageView = UIImageView()
        imageView.isUserInteractionEnabled = true
        imageView.image = globalImageWithName(MBImageName.work.live.livePBg)
        return imageView
    }()
    
    fileprivate lazy var  UserInfoContentTopView : UIView = {
       var view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = .white
        return view
    }()
    
    fileprivate lazy var  UserLogoImageView : UIImageView = {
       var imageView = UIImageView()
        imageView.image =  globalImageWithName(MBImageName.defaultImageName.defaltHeadlogo)
        return imageView
    }()
    
    fileprivate lazy var  UserNameLabel : UILabel = {
       var Label = UILabel()
        Label.text = "辛辛"
        Label.textAlignment = .left
        Label.textColor  = UIColor(hexStr: "333333")
        Label.font = MBConstant.FONT_14
        return Label
    }()
    
    fileprivate lazy var  FollowLabel : UILabel = {
       var Label = UILabel()
        Label.text = "已关注直播间"
        Label.textAlignment = .left
        Label.textColor  = UIColor(hexStr: "AAAAAA")
        Label.font = MBConstant.FONT_12
        return Label
    }()
    
    fileprivate lazy var  UserInfoLineView : UIView = {
       var view = UIView()
        view.backgroundColor = UIColor(hexStr: "EEEEEE")
        return view
    }()
    
    fileprivate lazy var  UserOrderLabel : UILabel = {
       var Label = UILabel()
        Label.text = "订单信息"
        Label.textAlignment = .center
        Label.textColor  = UIColor(hexStr: "E69230")
        Label.font = MBConstant.FONT_11
        return Label
    }()
    
    fileprivate lazy var  UserInfoSLineView : UIView = {
       var view = UIView()
        view.backgroundColor = UIColor(hexStr: "EEEEEE")
        return view
    }()
    
    fileprivate lazy var  UserfavourableLabel : UILabel = {
       var Label = UILabel()
        Label.text = "优惠券"
        Label.textAlignment = .center
        Label.textColor  = UIColor(hexStr: "666666")
        Label.font = MBConstant.FONT_11
       
        return Label
    }()
    
    fileprivate lazy var  PaymentAmountLabel : UILabel = {
       var Label = UILabel()
        Label.text = "9999"
        Label.textAlignment = .center
        Label.textColor  = UIColor(hexStr: "FD3A39")
        Label.font = MBConstant.FONT_11
        return Label
    }()
    
   
    
    fileprivate lazy var  PaymentAmountNoteLabel : UILabel = {
        var Label = UILabel()
        Label.text = "付款金额"
        Label.textAlignment = .center
        Label.textColor  = UIColor(hexStr: "666666")
        Label.font = MBConstant.FONT_11
        return Label
    }()
    
    fileprivate lazy var  RefundAmountLabel : UILabel = {
        var Label = UILabel()
        Label.text = "888"
        Label.textAlignment = .center
        Label.textColor  = UIColor(hexStr: "FD3A39")
        Label.font = MBConstant.FONT_11
        return Label
    }()
    
    fileprivate lazy var  RefundAmountNoteLabel : UILabel = {
        var Label = UILabel()
        Label.text = "退款金额"
        Label.textAlignment = .center
        Label.textColor  = UIColor(hexStr: "666666")
        Label.font = MBConstant.FONT_11
        return Label
    }()
    
    
    fileprivate lazy var  PaymentNumLabel : UILabel = {
        var Label = UILabel()
         Label.text = "99"
         Label.textAlignment = .center
         Label.textColor  = UIColor(hexStr: "FD3A39")
         Label.font = MBConstant.FONT_11
         return Label
     }()
     
    
     
     fileprivate lazy var  PaymentNumNoteLabel : UILabel = {
         var Label = UILabel()
         Label.text = "付款个数"
         Label.textAlignment = .center
         Label.textColor  = UIColor(hexStr: "666666")
         Label.font = MBConstant.FONT_11
         return Label
     }()
    
    fileprivate lazy var  RefundNumLabel : UILabel = {
        var Label = UILabel()
        Label.text = "888"
        Label.textAlignment = .center
        Label.textColor  = UIColor(hexStr: "FD3A39")
        Label.font = MBConstant.FONT_11
        return Label
    }()
    
    fileprivate lazy var  RefundNumNoteLabel : UILabel = {
        var Label = UILabel()
        Label.text = "退款个数"
        Label.textAlignment = .center
        Label.textColor  = UIColor(hexStr: "666666")
        Label.font = MBConstant.FONT_11
        return Label
    }()
    
    
    fileprivate lazy var createOrderView: UIView = {
        var view = UIView()
        return view
    }()
    
    //
    fileprivate lazy var createOrderLabel: UILabel = {
        var Label = UILabel()
        Label.text = "截图创建订单"
        Label.textAlignment = .center
        Label.textColor  = UIColor(hexStr: "ffffff")
        
        Label.font = MBConstant.FONT_11
        Label.isUserInteractionEnabled = true
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(createOrderLabelClick))
        Label.addGestureRecognizer(singleTapGesture)
        return Label
    }()
    
    fileprivate lazy var  UserInfofootLineView : UIView = {
       var view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = UIColor(hexStr: "EEEEEE")
        return view
    }()
    
    fileprivate lazy var prohibitBtn: UIButton = {
      var button = UIButton()
      button.backgroundColor = UIColor.white
    
      button.setTitleColor(UIColor(hexStr: "E69230"), for: .normal)
      button.setTitleColor(UIColor(hexStr: "666666"), for:.highlighted) //触摸高亮状态下文字的颜色
      
      button.adjustsImageWhenDisabled = true
      button.adjustsImageWhenHighlighted = true
      button.setTitle("禁言",for: .normal)
        
      button.titleLabel?.font  = MBConstant.FONT_12
      
      button.addTarget(self, action: #selector(prohibitBtnDidClick(sender:)), for: .touchUpInside)
      return button
    }()
    
    fileprivate lazy var atBtn: UIButton = {
        var button = UIButton()
        button.backgroundColor = UIColor.white
        
        button.setTitleColor(UIColor(hexStr: "E69230"), for: .normal)
        button.setTitleColor(UIColor(hexStr: "666666"), for:.highlighted) //触摸高亮状态下文字的颜色
        
        button.adjustsImageWhenDisabled = true
        button.adjustsImageWhenHighlighted = true
        
        button.setTitle("@ta",for: .normal)
    
        button.titleLabel?.font  = MBConstant.FONT_11
        
        button.addTarget(self, action: #selector(atBtnDidClick(sender:)), for: .touchUpInside)
        return button
    }()
    
    
    fileprivate lazy var daibanBtn: UIButton = {
        var button = UIButton()
        button.backgroundColor = UIColor.white
        
        button.setTitleColor(UIColor(hexStr: "E69230"), for: .normal)
        button.setTitleColor(UIColor(hexStr: "666666"), for:.highlighted) //触摸高亮状态下文字的颜色
        
        button.setTitle("待办",for: .normal)
        
        button.adjustsImageWhenDisabled = true
        button.adjustsImageWhenHighlighted = true
        
        button.titleLabel?.font  = MBConstant.FONT_12
        
        
        button.addTarget(self, action: #selector(daibanBtnDidClick(sender:)), for: .touchUpInside)
        return button
        
    }()
    
    
    
    
}

extension MBLiveUserInfoView {
   
    
    @objc func closeBtnDidClick() {
        self.isHidden = true
    }
    
    /// 点击创建截图订单
    @objc func createOrderLabelClick() {
        
    
    }
    
    /// 点击禁言
    @objc func prohibitBtnDidClick(sender: UIButton) {
           
    
    }
    
    /// 点击 @他
    @objc func atBtnDidClick(sender: UIButton) {
           
    
    }
    
    /// 点击 代办
    @objc func daibanBtnDidClick(sender: UIButton) {
           
    
    }
}



