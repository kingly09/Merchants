//
//  MBLiveRoomtoolbar.swift
//  Merchants
//
//  Created by kingly on 2020/7/23.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

let KliveSpace = (SCREEN_WIDTH - 40.0 * 7 - 20.0)/6.0


class MBLiveRoomtoolbar: UIView {
    
    
    fileprivate lazy var liveProductBtn: UIButton = {
        var liveProductBtn = UIButton().ky_custombutton(imageName:MBImageName.work.live.liveProduct)
       liveProductBtn.addTarget(self, action: #selector(liveProductBtnDidClick), for: .touchUpInside)
       return liveProductBtn
     }()
    
    
    fileprivate lazy var liveDaibanBtn: UIButton = {
       var liveDaibanBtn = UIButton().ky_custombutton(imageName:MBImageName.work.live.liveDaiban)
      liveDaibanBtn.addTarget(self, action: #selector(liveDaibanBtnDidClick), for: .touchUpInside)
      return liveDaibanBtn
    }()
    
    fileprivate lazy var liveHdBtn: UIButton = {
       var liveHdBtn = UIButton().ky_custombutton(imageName:MBImageName.work.live.liveHd)
      liveHdBtn.addTarget(self, action: #selector(liveHdBtnDidClick), for: .touchUpInside)
      return liveHdBtn
    }()
    
    
    fileprivate lazy var liveActiveBtn: UIButton = {
          var liveActiveBtn = UIButton().ky_custombutton(imageName:MBImageName.work.live.liveActive)
         liveActiveBtn.addTarget(self, action: #selector(liveActiveBtnDidClick), for: .touchUpInside)
         return liveActiveBtn
    }()

    fileprivate lazy var liveMoreBtn: UIButton = {
       var liveMoreBtn = UIButton().ky_custombutton(imageName:MBImageName.work.live.liveMore)
      liveMoreBtn.addTarget(self, action: #selector(liveMoreBtnDidClick), for: .touchUpInside)
      return liveMoreBtn
    }()
    
//    fileprivate lazy var livePraiseBtn: MBPraiseEmitterBtn = {
//        var livePraiseBtn = MBPraiseEmitterBtn(frame: CGRect(x: 100, y: 0, width: 40, height:40))
//        livePraiseBtn.addTarget(self, action: #selector(livePraiseBtnDidClick(sender:)), for: .touchUpInside)
//        return livePraiseBtn
//    }()
    
    fileprivate lazy var liveCameraBtn: UIButton = {
       var liveCameraBtn = UIButton().ky_custombutton(imageName:MBImageName.work.live.liveCamera)
      liveCameraBtn.addTarget(self, action: #selector(liveCameraBtnDidClick), for: .touchUpInside)
      return liveCameraBtn
    }()
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCustomView()
        setuplayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupCustomView() {
        
        self.addSubview(liveProductBtn)
        self.addSubview(liveActiveBtn)
        self.addSubview(liveDaibanBtn)
        self.addSubview(liveHdBtn)
        self.addSubview(liveCameraBtn)
        self.addSubview(liveMoreBtn)
        
        let livePraiseBtn = MBPraiseEmitterBtn(frame: CGRect(x: kScreenWidth - 50, y: 0, width: 40, height:40))
        livePraiseBtn.setImage(globalImageWithName(MBImageName.work.live.livePraise), for: .normal)
        livePraiseBtn.addTarget(self, action: #selector(livePraiseBtnDidClick(sender:)), for: .touchUpInside)
        self.addSubview(livePraiseBtn)
        
        
        
    }
    
    // MARK: - layout
    private func setuplayout() {
        
        liveProductBtn.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.bottom.equalTo(-10)
        }
        
        liveActiveBtn.snp.makeConstraints { (make) in
            make.left.equalTo(liveProductBtn.snp.right).offset(KliveSpace)
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.bottom.equalTo(-10)
        }
        
        liveDaibanBtn.snp.makeConstraints { (make) in
            make.left.equalTo(liveActiveBtn.snp.right).offset(KliveSpace)
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.bottom.equalTo(-10)
        }
        
        liveHdBtn.snp.makeConstraints { (make) in
            make.left.equalTo(liveDaibanBtn.snp.right).offset(KliveSpace)
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.bottom.equalTo(-10)
        }
        
        liveCameraBtn.snp.makeConstraints { (make) in
            make.left.equalTo(liveHdBtn.snp.right).offset(KliveSpace)
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.bottom.equalTo(-10)
        }
        
        liveMoreBtn.snp.makeConstraints { (make) in
            make.left.equalTo(liveCameraBtn.snp.right).offset(KliveSpace)
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.bottom.equalTo(-10)
        }
        
//        livePraiseBtn.snp.makeConstraints { (make) in
//            make.left.equalTo(liveMoreBtn.snp.right).offset(KliveSpace)
//            make.width.equalTo(40)
//            make.height.equalTo(40)
//            make.bottom.equalTo(-10)
//        }
//
       
        
    }
    
    
    
}

extension MBLiveRoomtoolbar {
    
    @objc func liveProductBtnDidClick()  {
        
    }
    
    @objc  func liveDaibanBtnDidClick () {
        
    }
    
    @objc func liveHdBtnDidClick() {
        
    }
    
    @objc func liveActiveBtnDidClick(){
        
    }
    
    @objc func livePraiseBtnDidClick(sender: UIButton) {
            sender.isSelected = !sender.isSelected
    }
       
    @objc func liveMoreBtnDidClick() {
           
    }
    
    @objc func liveCameraBtnDidClick() {
        
    }
    
}
