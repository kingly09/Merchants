//
//  MBLiveRoomHeadView.swift
//  Merchants
//
//  Created by kingly on 2020/7/21.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

// 代理
protocol MBLiveRoomHeadViewDelegate {
    
    /// 用户头像点击
    func userLogoBtnDidClick()
    /// 点击关闭当前直播
    func closeLiveBtnDidClick()
    /// 点击开始直播
    func openLiveBtnDidClick()
    /// 返回上一个界面
    func backBtnDidClick()
}


class MBLiveRoomHeadView: UIView {
    
    var delegate: MBLiveRoomHeadViewDelegate?
    
    fileprivate lazy var UserInfoView : UIView = {
        var view = UIView()
        view.backgroundColor = UIColor(hexStr: "000000", alpha: 0.3)
        view.isUserInteractionEnabled = true
//        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(homeAppClick))
//          self.addGestureRecognizer(singleTapGesture)
        return view
    }()
    
    fileprivate lazy var userLogoBtn: UIButton = {
        var userLogoBtn = UIButton().ky_custombutton(imageName:MBImageName.defaultImageName.defaltHeadlogo)
        userLogoBtn.addTarget(self, action: #selector(userLogoBtnDidClick), for: .touchUpInside)
        return userLogoBtn
    }()
    
    fileprivate lazy var backBtn: UIButton = {
        var backBtn = UIButton().ky_custombutton(imageName:MBImageName.icon.whiteback)
        backBtn.addTarget(self, action: #selector(backBtnDidClick), for: .touchUpInside)
        return backBtn
    }()
    
    fileprivate lazy var titleLabel  : UILabel = {
           var titleLabel = UILabel()
           titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
           titleLabel.textColor = .white
           titleLabel.textAlignment = .center
           titleLabel.text = "主播管理"
           return titleLabel
    }()
    
    fileprivate lazy var liveRoomnameLabel  : UILabel = {
        var liveRoomnameLabel = UILabel()
        liveRoomnameLabel.font = UIFont.boldSystemFont(ofSize: 13)
        liveRoomnameLabel.textColor = .white
        liveRoomnameLabel.textAlignment = .left
        liveRoomnameLabel.text = "老辣椒源头直…"
        return liveRoomnameLabel
    }()
    
    fileprivate lazy var nicknameLabel  : UILabel = {
        var nicknameLabel = UILabel()
        nicknameLabel.font = UIFont.boldSystemFont(ofSize: 10)
        nicknameLabel.textColor = .white
        nicknameLabel.textAlignment = .left
        nicknameLabel.text = "辛辛"
        return nicknameLabel
    }()
    
    fileprivate lazy var closeLiveBtn: UIButton = {
        var button = UIButton()
        
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitleColor(UIColor(hexString: "919191"), for:.highlighted) //触摸高亮状态下文字的颜色
        button.adjustsImageWhenDisabled = true
        button.adjustsImageWhenHighlighted = true
        button.setTitle(MBStr.Live.closeLive, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        button.addTarget(self, action: #selector(closeLiveBtnDidClick(sender:)), for: .touchUpInside)
        return button
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupCustomView()
        setuplayout()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setupCustomView() {
        
        self.isUserInteractionEnabled = true
        self.backgroundColor = UIColor.clear
        self.addSubview(backBtn)
        
        self.addSubview(UserInfoView)
        
        self.addSubview(titleLabel)
        
        UserInfoView.addSubview(userLogoBtn)
        UserInfoView.addSubview(userLogoBtn)
        UserInfoView.addSubview(liveRoomnameLabel)
        UserInfoView.addSubview(nicknameLabel)
        
        self.addSubview(closeLiveBtn)
        
      
      
        
        setupLiveType(type: .NONE)
    }
    
    
    // MARK: - layout
    private func setuplayout() {
        
        backBtn.snp.makeConstraints { (make) in
            make.top.equalTo(28)
            make.left.equalTo(20)
            make.width.equalTo(28)
            make.height.equalTo(28)
        }
        backBtn.hw_clickEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)

        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(28)
            make.left.equalTo(0)
            make.width.equalTo(kScreenWidth)
            make.height.equalTo(40)
        }
        
        UserInfoView.snp.makeConstraints { (make) in
            make.top.equalTo(22)
            make.left.equalTo(10)
            make.width.equalTo(148)
            make.height.equalTo(40)
        }
        UserInfoView.layer.masksToBounds = true
        UserInfoView.layer.cornerRadius = 20.0
        
        userLogoBtn.snp.makeConstraints { (make) in
            make.top.equalTo(4)
            make.left.equalTo(4)
            make.width.equalTo(32)
            make.height.equalTo(32)
        }
        userLogoBtn.layer.masksToBounds = true
        userLogoBtn.layer.cornerRadius = 16.0
        userLogoBtn.hw_clickEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        liveRoomnameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.left.equalTo(userLogoBtn.snp.right).offset(4)
            make.width.equalTo(92)
            make.height.equalTo(13)
        }
        
        nicknameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(liveRoomnameLabel.snp.bottom).offset(4)
            make.left.equalTo(userLogoBtn.snp.right).offset(4)
            make.width.equalTo(92)
            make.height.equalTo(13)
        }
        
        closeLiveBtn.snp.makeConstraints { (make) in
            make.top.equalTo(22)
            make.right.equalTo(20)
            make.width.equalTo(134)
            make.height.equalTo(40)
        }
        closeLiveBtn.layer.masksToBounds = true
        closeLiveBtn.layer.cornerRadius = 20.0
        closeLiveBtn.contentHorizontalAlignment = .center
        closeLiveBtn.contentVerticalAlignment   = .center
        closeLiveBtn.hw_clickEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        //
        let gradientLayer = CAGradientLayer()
        //几个颜色
        gradientLayer.colors = [UIColor(hexStr: "444545").cgColor,UIColor(hexStr: "1D1D1D").cgColor]
        //颜色的分界点
        gradientLayer.locations = [0.2,1.0]
        //开始
        gradientLayer.startPoint = CGPoint.init(x: 0, y: 0)
        //结束,主要是控制渐变方向
        gradientLayer.endPoint  = CGPoint.init(x: 1.0, y: 0)
        //多大区域
        gradientLayer.frame = CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH, height: 40.0)
        //最后作为背景
        closeLiveBtn.layer.insertSublayer(gradientLayer, at: 0)
        
        
        
    }
    
    
    
}

extension MBLiveRoomHeadView {
    
    
    @objc private func userLogoBtnDidClick() {
        
        delegate?.userLogoBtnDidClick()
        
    }
    
    @objc private func backBtnDidClick(){
        
        delegate?.backBtnDidClick()
    }
    
    
    @objc fileprivate func closeLiveBtnDidClick(sender: UIButton) {
        sender.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
            sender.isEnabled = true
        }
        
        if MBUserInfo.shared.getLiveType() == .LIVEING {
             delegate?.closeLiveBtnDidClick()
        }else{
            delegate?.openLiveBtnDidClick()
        }

    }
    
}


extension MBLiveRoomHeadView {
    
    func setupLiveType(type: LIVE_TYPE){
        
        MBUserInfo.shared.liveType = type
        
        if MBUserInfo.shared.liveType == .LIVEING {
            closeLiveBtn.setTitle(MBStr.Live.closeLive, for: .normal)
            UserInfoView.isHidden = false
            backBtn.isHidden = true
            titleLabel.isHidden = true
            
        }else {
            
            closeLiveBtn.setTitle(MBStr.Live.openLive, for: .normal)
            UserInfoView.isHidden = true
            backBtn.isHidden = false
            titleLabel.isHidden = false
        }
        
    }
    
}
