//
//  MBLiveEntryTableViewCell.swift
//  Merchants
//
//  Created by kingly on 2020/7/17.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

let kMBLiveEntryTableViewCellHight    = 478.0


class MBLiveEntryTableViewCell : BaseTableViewCell {

    deinit {
          print("MBCommonEntryTableViewCell deinit")
    }
    
    var VJFuntionModelList = [VJFuntionModel]()
    
     var collectionView : UICollectionView?
    
    // MARK: - 懒加载
     fileprivate lazy var CommonImageView: UIImageView = {
        var CommonImageView = UIImageView()
        CommonImageView.image = globalImageWithName(MBImageName.work.titleImageBg)
        return CommonImageView
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
      var titleLabel = UILabel()
      titleLabel.font = UIFont.systemFont(ofSize: 15)
      titleLabel.textColor = UIColor(hexStr: "333333")
      titleLabel.textAlignment = .left
      titleLabel.text = MBStr.Work.liveEntry
      return titleLabel
    }()
    
   
    
    static func cellHight(arrCont : Int) -> CGFloat {
      
      if arrCont == 0 {
        return 0.0
      }
      
      let y =  ceil(Double(arrCont)/3)

      return CGFloat(kCommonHight) * CGFloat(y)
      
    }
    

    override func setupUI(){
        
        self.contentView.isUserInteractionEnabled = true
        
        contentView.addSubview(CommonImageView)
        contentView.addSubview(titleLabel)
        // 初始化
           let layout = UICollectionViewFlowLayout.init()
           layout.itemSize = CGSize(width: productWidth, height: kCommonHight)
           layout.minimumLineSpacing = 0
           layout.minimumInteritemSpacing = 0
           layout.scrollDirection = .vertical
           layout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
           
           collectionView = UICollectionView.init(frame: CGRect(x:0, y: 38, width: SCREEN_WIDTH, height:kCommonHight), collectionViewLayout: layout)
           collectionView?.backgroundColor = UIColor.clear
           collectionView?.delegate = self
           collectionView?.dataSource = self
           collectionView?.isScrollEnabled = false
           collectionView?.register(MBCommonCollectionViewCell.self, forCellWithReuseIdentifier: MBCommonCollectionViewCell.kMBCommonCollectionViewCell)
            collectionView?.isUserInteractionEnabled = true

            self.contentView.addSubview(collectionView!)
        
    }
     // MARK: - layout
    override func setupLayout() {
        
        CommonImageView.snp.makeConstraints { (make) in
            make.top.equalTo(30)
            make.left.equalTo(15)
            make.width.equalTo(63)
            make.height.equalTo(7)
        }
        
      titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(16)
            make.left.equalTo(15)
            make.width.equalTo(63)
            make.height.equalTo(22)
      }
        

    }
        
    var appArr : [VJFuntionModel]? {
      
      didSet{
      
        addAppView(appList: appArr)
        
      }
      
    }

     fileprivate func addAppView(appList: [VJFuntionModel]?)  {
        self.VJFuntionModelList = []
        self.VJFuntionModelList = appList!
        let cout =  appList?.count
        collectionView?.frame =  CGRect(x:0, y: 50, width: SCREEN_WIDTH, height:MBCommonEntryTableViewCell.cellHight(arrCont: cout!))
        collectionView?.reloadData()

      }
}

 

extension MBLiveEntryTableViewCell :  UICollectionViewDataSource, UICollectionViewDelegate {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.VJFuntionModelList.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
      
     let cell : MBCommonCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: MBCommonCollectionViewCell.kMBCommonCollectionViewCell, for: indexPath) as! MBCommonCollectionViewCell
      // cell.delegate = self
        cell.vJFuntionModel =   self.VJFuntionModelList[indexPath.row]
       return  cell
  }
  
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
  }
  
  func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    
  }
  
  
  
}

