//
//  MBCommonCollectionViewCell.swift
//  Merchants
//
//  Created by kingly on 2020/7/20.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit


// 代理
protocol MBCollectionViewCelDelegate {
  
  ///
 func appCollectionViewCellAppBtnDidClick(model: VJFuntionModel)

}


class MBCommonCollectionViewCell: UICollectionViewCell {
    
    static let kMBCommonCollectionViewCell = "kMBCommonCollectionViewCell"
    
    var delegate: MBCollectionViewCelDelegate?
    /// 初始化
    ///
    /// - Parameter frame: frame
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupCustomView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCustomView(){
        
        
        self.contentView.backgroundColor =  UIColor.clear
        self.contentView.addSubview(LogoButton)
        
        LogoButton.addSubview(logoImageView)
        LogoButton.addSubview(titleLabel)
    }
    
    fileprivate lazy var LogoButton : UIButton = {
        var button = UIButton()
        button.backgroundColor = UIColor.white
        
        button.setTitleColor(UIColor(hexStr: "666666"), for: .normal)
        button.setTitleColor(UIColor(hexStr: "000000"), for:.highlighted) //触摸高亮状态下文字的颜色
        
        button.adjustsImageWhenDisabled = true
        button.adjustsImageWhenHighlighted = true
        
        button.addTarget(self, action: #selector(logoButtonDidClick(sender:)), for: .touchUpInside)
        return button
    }()
    
    
    
    fileprivate lazy var logoImageView: UIImageView = {
        var imageView  = UIImageView()
        return imageView
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        var titleLabel = UILabel()
        titleLabel.font = MBConstant.FONT_12
        titleLabel.textColor = UIColor(hexStr: "666666")
        titleLabel.textAlignment = .center
        return titleLabel
    }()
    
    // MARK: - 布局
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        LogoButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(0)
            make.left.equalTo(0)
            make.top.equalTo(0)
            make.right.equalTo(0)
        }
        
        logoImageView.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.width.equalTo(45)
            make.height.equalTo(45)
            make.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(logoImageView.snp.bottom).offset(5)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(20)
        }
        
        
    }
    
    var vJFuntionModel: VJFuntionModel? {
        
        didSet {
            
            titleLabel.text = vJFuntionModel?.permissionName
            
            if( vJFuntionModel?.permissionName == MBStr.Work.orderManage) {
                
                logoImageView.image =  globalImageWithName(MBImageName.work.orderManage)
                
            }else if( vJFuntionModel?.permissionName == MBStr.Work.commodManage) {
                
                logoImageView.image =  globalImageWithName(MBImageName.work.commodManage)
                
            }else if( vJFuntionModel?.permissionName == MBStr.Work.marketingManage) {
                
                logoImageView.image =  globalImageWithName(MBImageName.work.marketingManage)
                
            }else if( vJFuntionModel?.permissionName == MBStr.Work.myBoutique) {
                
                logoImageView.image =  globalImageWithName(MBImageName.work.myBoutique)
                
            }else if( vJFuntionModel?.permissionName == MBStr.Work.publicGoods) {
                
                logoImageView.image =  globalImageWithName(MBImageName.work.publicGoods)
                
            }else if( vJFuntionModel?.permissionName == MBStr.Work.activityDec) {
                
                logoImageView.image =  globalImageWithName(MBImageName.work.activityDec)
                
            }else if( vJFuntionModel?.permissionName == MBStr.Work.liveAnnounce) {
                
                logoImageView.image =  globalImageWithName(MBImageName.work.liveAnnounce)
                
            }else if( vJFuntionModel?.permissionName == MBStr.Work.liveRoom) {
                
                logoImageView.image =  globalImageWithName(MBImageName.work.liveRoom)
                
            }else if( vJFuntionModel?.permissionName == MBStr.Work.anchorManage) {
                
                logoImageView.image =  globalImageWithName(MBImageName.work.anchorManage)
                
            }else if( vJFuntionModel?.permissionName == MBStr.Work.liveRecording) {
                
                logoImageView.image =  globalImageWithName(MBImageName.work.liveRecording)
            }
            
        }
    }
    
}




extension MBCommonCollectionViewCell {
    
    @objc fileprivate func logoButtonDidClick(sender: UIButton) {
        self.titleLabel.textColor = UIColor(hexStr: "333333")
        sender.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
            sender.isEnabled = true
            self.titleLabel.textColor = UIColor(hexStr: "666666")
        }
        
        
        if( vJFuntionModel?.permissionName == MBStr.Work.orderManage) {
            
             MBOpenRouter.orderList()
            
        }else if( vJFuntionModel?.permissionName == MBStr.Work.commodManage) {
            
             MBOpenRouter.commodManage()
            
        }else if( vJFuntionModel?.permissionName == MBStr.Work.marketingManage) {
            
             MBOpenRouter.marketing()
            
        }else if( vJFuntionModel?.permissionName == MBStr.Work.myBoutique) {
            
             MBOpenRouter.qualityGoodsMe()
            
        }else if( vJFuntionModel?.permissionName == MBStr.Work.publicGoods) {
            
              MBOpenRouter.qualityGoodsPublics()
            
        }else if( vJFuntionModel?.permissionName == MBStr.Work.activityDec) {
            
             MBOpenRouter.activityIndex()
            
        }else if( vJFuntionModel?.permissionName == MBStr.Work.liveAnnounce) {
            
           // MGJRouter.open(MBUrl.Router.Work.Live.LiveAnnounce.List)
           MBOpenRouter.livePlan()
            
        }else if( vJFuntionModel?.permissionName == MBStr.Work.liveRoom) {
            
            MBUserInfo.shared.isFirst = true
            MGJRouter.open(MBUrl.Router.Work.Live.LiveRoom.Open)
            
        }else if( vJFuntionModel?.permissionName == MBStr.Work.anchorManage) {
            
             MBOpenRouter.anchorManage()
            
        }else if( vJFuntionModel?.permissionName == MBStr.Work.liveRecording) {
            
            MBOpenRouter.liveRecord()
        }
        
        
    }
}
