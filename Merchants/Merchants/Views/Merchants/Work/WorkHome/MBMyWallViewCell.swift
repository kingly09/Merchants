//
//  MBMyWallViewCell.swift
//  Merchants
//
//  Created by kingly on 2020/7/23.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

let kMBMyWallViewCellHight    = 165.0

class MBMyWallViewCell: BaseTableViewCell {
    
    fileprivate lazy var myWalletImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.image = globalImageWithName(MBImageName.work.myWalletImageBg)
        return imageView
    }()
    
    fileprivate lazy var totalamountLabel: UILabel = {
        var totalamountLabel = UILabel()
        totalamountLabel.font = UIFont.boldSystemFont(ofSize: 36)
        totalamountLabel.textColor = UIColor(hexStr: "FFFFFF")
        totalamountLabel.textAlignment = .left
        totalamountLabel.text = "302944"
        return totalamountLabel
    }()
    
    fileprivate lazy var totalamountnoteLabel: UILabel = {
        var totalamountnoteLabel = UILabel()
        totalamountnoteLabel.font = UIFont.systemFont(ofSize: 16)
        totalamountnoteLabel.textColor = UIColor(hexStr: "FFFFFF")
        totalamountnoteLabel.textAlignment = .right
        totalamountnoteLabel.text = "可提现（元）"
        return totalamountnoteLabel
    }()
    
    
    fileprivate lazy var financeLabel: UILabel = {
        var financeLabel = UILabel()
        financeLabel.font = UIFont.systemFont(ofSize: 12)
        financeLabel.textColor = UIColor(hexStr: "FFFFFF")
        financeLabel.textAlignment = .left
        financeLabel.text = "待结算金额（元）"
        return financeLabel
    }()
    
    fileprivate lazy var financesettledLabel: UILabel = {
        var financesettledLabel = UILabel()
        financesettledLabel.font = UIFont.systemFont(ofSize: 12)
        financesettledLabel.textColor = UIColor(hexStr: "FFFFFF")
        financesettledLabel.textAlignment = .center
        financesettledLabel.text = "898912"
        return financesettledLabel
    }()
    
    fileprivate lazy var lineView: UIView = {
        var lineView = UIView()
        lineView.backgroundColor = UIColor(hexStr: "ffffff")
        lineView.isUserInteractionEnabled = true
        return lineView
    }()
    
    fileprivate lazy var redbagLabel: UILabel = {
        var redbagLabel = UILabel()
        redbagLabel.font = UIFont.systemFont(ofSize: 12)
        redbagLabel.textColor = UIColor(hexStr: "FFFFFF")
        redbagLabel.textAlignment = .center
        redbagLabel.text = "红包活动金额（元）"
        return redbagLabel
    }()
    
    fileprivate lazy var redbagAmountLabel: UILabel = {
        var redbagAmountLabel = UILabel()
        redbagAmountLabel.font = UIFont.systemFont(ofSize: 12)
        redbagAmountLabel.textColor = UIColor(hexStr: "FFFFFF")
        redbagAmountLabel.textAlignment = .center
        redbagAmountLabel.text = "898912"
        return redbagAmountLabel
    }()
    
    deinit {
        print("MBApplyResidenceViewCell deinit")
    }
    
    override func setupUI(){
        
        contentView.addSubview(myWalletImageView)
        myWalletImageView.addSubview(totalamountLabel)
        myWalletImageView.addSubview(totalamountnoteLabel)
        myWalletImageView.addSubview(financeLabel)
        myWalletImageView.addSubview(financesettledLabel)
        myWalletImageView.addSubview(lineView)
        myWalletImageView.addSubview(redbagLabel)
        myWalletImageView.addSubview(redbagAmountLabel)
        
    }
    // MARK: - layout
    override func setupLayout() {
        
        myWalletImageView.snp.makeConstraints { (make) in
            make.top.equalTo(93)
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.height.equalTo(165)
        }
        
        
        totalamountLabel.snp.makeConstraints { (make) in
            make.top.equalTo(28)
            make.left.equalTo(20)
            make.right.equalTo(-115)
            make.height.equalTo(43)
        }
        
        totalamountnoteLabel.snp.makeConstraints { (make) in
            make.top.equalTo(48)
            make.width.equalTo(100)
            make.right.equalTo(-20)
            make.height.equalTo(22)
        }
        
        financeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(100)
            make.width.equalTo(100)
            make.left.equalTo(20)
            make.height.equalTo(18)
        }
        
        financesettledLabel.snp.makeConstraints { (make) in
            make.top.equalTo(127)
            make.width.equalTo(100)
            make.left.equalTo(20)
            make.height.equalTo(18)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.top.equalTo(102)
            make.centerX.equalToSuperview()
            make.width.equalTo(1)
            make.height.equalTo(41)
        }
        
        redbagLabel.snp.makeConstraints { (make) in
            make.top.equalTo(100)
            make.width.equalTo(130)
            make.right.equalTo(-20)
            make.height.equalTo(18)
        }
        
        redbagAmountLabel.snp.makeConstraints { (make) in
            make.top.equalTo(127)
            make.width.equalTo(130)
            make.right.equalTo(-20)
            make.height.equalTo(18)
        }
        
        
        
    }
    
    
}


