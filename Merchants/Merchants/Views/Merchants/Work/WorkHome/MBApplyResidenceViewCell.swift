//
//  MBApplyResidenceViewCell.swift
//  Merchants
//
//  Created by kingly on 2020/7/23.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

let kMBApplyResidenceViewCellHight    = 100.0

class MBApplyResidenceViewCell: BaseTableViewCell {

    fileprivate lazy var ApplyBtn: UIButton = {
        var ApplyBtn = UIButton().ky_custombutton(imageName: MBImageName.work.myWalletImageBg)
        ApplyBtn.addTarget(self, action: #selector(applyBtnDidClick(sender:)), for: .touchUpInside)
        return ApplyBtn
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
      var titleLabel = UILabel()
       titleLabel.text = MBStr.Work.applyResidence
      titleLabel.font = MBConstant.FONT_20
      titleLabel.textColor = UIColor(hexStr: "FFFFFF")
      titleLabel.textAlignment = .center
      titleLabel.isUserInteractionEnabled = true
     let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(titleLabelViewClick))
      titleLabel.addGestureRecognizer(singleTapGesture)
      return titleLabel
    }()
    
     deinit {
         print("MBApplyResidenceViewCell deinit")
     }
    
     override func setupUI(){
        contentView.isUserInteractionEnabled = true
        contentView.addSubview(ApplyBtn)
        ApplyBtn.addSubview(titleLabel)

      }
    // MARK: - layout
    override func setupLayout() {
        
        ApplyBtn.snp.makeConstraints { (make) in
            make.top.equalTo(15)
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.height.equalTo(85)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(85)
        }
        
    }
    
    
}
extension MBApplyResidenceViewCell {
    
    @objc fileprivate func applyBtnDidClick(sender: UIButton) {
        sender.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
          sender.isEnabled = true
        }
       
    }
    
    @objc func titleLabelViewClick() {
        self.titleLabel.textColor = UIColor(hexStr: "666666")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
            self.titleLabel.textColor = UIColor(hexStr: "FFFFFF")
        }
        
        //点击申请入驻
        MBOpenRouter.settledIndex()
        
    }
}
