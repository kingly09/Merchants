//
//  MBMerchantTableViewCell.swift
//  Merchants
//
//  Created by kingly on 2020/7/17.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

let kMBMerchantTableViewCellHight    = 75

class MBMerchantTableViewCell: BaseTableViewCell {

      // MARK: - 懒加载
       fileprivate lazy var MerchatsView: UIView = {
          var view = UIView()
          view.backgroundColor = UIColor.white
          view.isUserInteractionEnabled = true
          return view
        }()
       
        fileprivate lazy var logoImageView: UIImageView = {
          var logoImageView = UIImageView()
          logoImageView.image = globalImageWithName(MBImageName.defaultImageName.defaltHeadlogo)
          return logoImageView
        }()
       
       fileprivate lazy var nameLabel: UILabel = {
          var titleLabel = UILabel()
          titleLabel.font = UIFont.systemFont(ofSize: 20)
          titleLabel.textColor = UIColor(hexStr: "333333")
          titleLabel.textAlignment = .left
          titleLabel.text = MBStr.Work.mbname
          return titleLabel
        }()
    
    
    fileprivate lazy var noticeView: UIView = {
      var view = UIView()
      view.backgroundColor =  UIColor(hexString: "E69230",alpha: 0.14)
      view.isUserInteractionEnabled = true
      return view
    }()
    
    fileprivate lazy var noticeImageView: UIImageView = {
      var noticeImageView = UIImageView()
        noticeImageView.image = globalImageWithName(MBImageName.icon.noticeImage)
      return noticeImageView
    }()
    
    fileprivate lazy var noticeLabel: UILabel = {
      var titleLabel = UILabel()
      titleLabel.font = UIFont.systemFont(ofSize: 12)
      titleLabel.textColor = UIColor(hexStr: "E69230")
      titleLabel.textAlignment = .left
      titleLabel.text = MBStr.Work.noticeLabel
      return titleLabel
    }()
    
   
    
    deinit {
       print("MBMerchantTableViewCell deinit")
     }
    
    override func setupUI(){
       
       contentView.addSubview(MerchatsView)
       MerchatsView.addSubview(logoImageView)
       MerchatsView.addSubview(nameLabel)
        
      contentView.addSubview(noticeView)
      noticeView.addSubview(noticeImageView)
      noticeView.addSubview(noticeLabel)
        
    
        
     }
     
     // MARK: - layout
    override func setupLayout() {
        
          MerchatsView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.height.equalTo(30)
          }
          
          logoImageView.snp.makeConstraints { (make) in
            make.left.equalTo(MerchatsView).offset(0)
            make.width.equalTo(30)
            make.height.equalTo(30)
          }
          logoImageView.layer.cornerRadius = 15
          logoImageView.layer.masksToBounds = true
          
          nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(MerchatsView).offset(40)
            make.right.equalTo(MerchatsView).offset(-10)
            make.height.equalTo(30)
          }
        
          noticeView.snp.makeConstraints { (make) in
            make.top.equalTo(43)
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.height.equalTo(30)
         }
          
          noticeImageView.snp.makeConstraints { (make) in
           make.top.equalTo(noticeView).offset(3)
           make.left.equalTo(noticeView).offset(3)
           make.width.equalTo(24)
           make.height.equalTo(24)
        }
        
          noticeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(5)
            make.left.equalTo(30)
            make.right.equalTo(-3)
            make.height.equalTo(20)
         }
        
       
        
    }

}
