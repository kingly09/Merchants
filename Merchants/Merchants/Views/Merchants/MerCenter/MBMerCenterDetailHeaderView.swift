//
//  MBMerCenterDetailHeaderView.swift
//  Merchants
//
//  Created by kingly on 2020/7/20.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import Kingfisher

let kMBMerCenterDetailHeaderViewHight : CGFloat =  227

class MBMerCenterDetailHeaderView: UIView {

    override init(frame: CGRect) {
       super.init(frame: frame)
       
       setupCustomView()
       setuplayout()
       
     }
     
     required init?(coder aDecoder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
     }
     
     
     private func setupCustomView() {
       
       self.backgroundColor = UIColor.white
       self.isUserInteractionEnabled = true
       
      self.addSubview(shopHeaderImageView)
      self.addSubview(logoImageView)
      self.addSubview(companyNameLabel)
        
      self.addSubview(shopView)
      
      shopView.addSubview(shopAuthImageView)
      shopView.addSubview(authNameLabel)

     }
    
     // MARK: - layout
    private func setuplayout() {
        
        
        shopHeaderImageView.snp.makeConstraints { (make) in
          make.top.equalTo(0)
          make.height.equalTo(kMBMerCenterDetailHeaderViewHight)
          make.left.equalTo(0)
          make.right.equalTo(0)
        }
        
        logoImageView.snp.makeConstraints { (make) in
          make.top.equalTo(56)
          make.centerX.equalToSuperview()
          make.width.equalTo(80)
          make.height.equalTo(80)
        }
        logoImageView.layer.masksToBounds = true
        logoImageView.layer.cornerRadius = 40.0
        
        companyNameLabel.snp.makeConstraints { (make) in
          make.top.equalTo(logoImageView.snp.bottom).offset(6)
          make.left.equalTo(10)
          make.right.equalTo(-10)
          make.height.equalTo(22)
        }
        
        shopView.snp.makeConstraints { (make) in
            make.top.equalTo(companyNameLabel.snp.bottom).offset(8)
            make.width.equalTo(120)
            make.height.equalTo(25)
            make.centerX.equalToSuperview()
         }
        shopView.layer.masksToBounds = true
        shopView.layer.cornerRadius = 4.0
        
        shopAuthImageView.snp.makeConstraints { (make) in
           make.left.equalTo(2)
           make.width.equalTo(20)
           make.height.equalTo(20)
           make.centerY.equalToSuperview()
        }
        
        
        authNameLabel.snp.makeConstraints { (make) in
           make.left.equalTo(shopAuthImageView.snp.right).offset(3)
           make.right.equalTo(5)
           make.height.equalTo(15)
           make.top.equalTo(5)
        }
        
    }
    
    fileprivate lazy var shopHeaderImageView: UIImageView = {
        var shopHeaderImageView = UIImageView()
        shopHeaderImageView.image = globalImageWithName(MBImageName.MerCenter.shopHeaderBg)
        shopHeaderImageView.isUserInteractionEnabled = true
        return shopHeaderImageView
    }()
     
     fileprivate lazy var logoImageView : UIImageView = {
        var logoImageView = UIImageView()
        logoImageView.image = globalImageWithName(MBImageName.defaultImageName.defaltHeadlogo)
    //    var  logoUrl = BCUserInfo.shared.smallUserInfo().logo.ky_removingPercentEncoding()
    //    logoImageView.kf.setImage(with: ImageResource(downloadURL:URL.init(string: logoUrl)!), placeholder:  globalImageWithName(BCImageName.defaultImageName.defaltHeadlogo))
        return logoImageView
      }()
    
    
    fileprivate lazy var companyNameLabel: UILabel = {
      var companyNameLabel = UILabel()
      companyNameLabel.font = UIFont.boldSystemFont(ofSize: 16)
      companyNameLabel.textColor = UIColor(hexStr: "ffffff")
      var  companyName = MBUserInfo.shared.getLoginRspInfo().userInfo?.businessName
      if companyName?.ky_stringLength() == 0 {
        companyName = "四会翡翠批发"
      }
      companyNameLabel.textAlignment = .center
      companyNameLabel.text = companyName
      return companyNameLabel
    }()
    
    
      fileprivate lazy var shopView: UIView = {
        var view = UIView()
        view.backgroundColor = UIColor(hexStr: "18AFB9")
        return view
      }()
    
     fileprivate lazy var shopAuthImageView: UIImageView = {
         var shopAuthImageView = UIImageView()
          shopAuthImageView.image = globalImageWithName(MBImageName.MerCenter.authinfoMer)
        return shopAuthImageView
      }()
      
      fileprivate lazy var authNameLabel: UILabel = {
        var authNameLabel = UILabel()
        authNameLabel.font = UIFont.systemFont(ofSize: 10)
        authNameLabel.textColor = UIColor(hexStr: "ffffff")
        authNameLabel.text = MBStr.MerCenter.authinfoMer
        return authNameLabel
      }()
      

}
