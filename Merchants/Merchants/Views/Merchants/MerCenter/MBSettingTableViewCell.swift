//
//  MBSettingTableViewCell.swift
//  Merchants
//
//  Created by kingly on 2020/7/20.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

class MBSettingTableViewCell: BCDetailBaseTableViewCell {

  deinit {
    print("deinit:: MBSettingTableViewCell")
  }
  
  override func setupUI(){
    super.setupUI()
    
    self.contentView.addSubview(logoImageView)
    
  }
    
  fileprivate lazy var logoImageView: UIImageView = {
       var logoImageView = UIImageView()
       return logoImageView
   }()
  
  // MARK: - layout
  override func setupLayout() {
 
    
    logoImageView.snp.makeConstraints { (make) in
         make.top.equalTo((CGFloat(44) - 22)/2)
         make.left.equalTo(10)
         make.width.equalTo(22)
         make.height.equalTo(22)
    }
    
    titleLabel.snp.makeConstraints { (make) in
         make.top.equalTo(0)
         make.left.equalTo(42)
         make.right.equalTo(-30)
         make.height.equalTo(44)
    }
    
     rightIcon.snp.makeConstraints { (make) in
         make.width.equalTo(16)
         make.height.equalTo(16)
         make.centerY.equalToSuperview()
         make.right.equalTo(-10)
       }
    
       lineView.snp.makeConstraints { (make) in
         
         make.height.equalTo(1/SCREEN_SCALE)
         make.bottom.equalToSuperview()
         make.right.equalTo(0)
         make.left.equalToSuperview()
       }
  }
  
  var settingModel: MBSettingModel? {
    
    didSet {
      
      logoImageView.image =   globalImageWithName(settingModel!.imageName)
        
      titleLabel.text = settingModel?.title
      
      if  settingModel?.detail.ky_stringLength() == 0 {
        titleDetailLabel.text = ""
      }else{
        titleDetailLabel.text = settingModel?.detail
      }
    }
  }

  
}
