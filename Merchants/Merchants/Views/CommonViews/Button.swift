import UIKit


enum BYUIButtonImagePosition {
    case top
    case bottom
    case left
    case right
}

open class Button: UIButton {
    
    public typealias Action = (Button) -> Swift.Void
    
    fileprivate var actionOnTouch: Action?
    
    init() {
        super.init(frame: .zero)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(image: String) {
        self.init()
        self.setImage(UIImage.init(named: image), for: .normal)
    }
    convenience init(frame: CGRect, title : String?, titleColor: UIColor?, font: UIFont?) {
        self.init(frame:frame)
        self.titleLabel?.font = font
        self.setTitle(title, for: .normal)
        self.setTitleColor(titleColor, for: .normal)
    }
    
    public func action(_ closure: @escaping Action) {
        if actionOnTouch == nil {
            addTarget(self, action: #selector(Button.actionOnTouchUpInside), for: .touchUpInside)
        }
        self.actionOnTouch = closure
    }
    
    @objc internal func actionOnTouchUpInside() {
        actionOnTouch?(self)
    }
    
    func setImagePosition(imagePosition: BYUIButtonImagePosition, spacingBetweenImageAndTitle: CGFloat) {
        
        let imageWidth = imageView?.intrinsicContentSize.width ?? 0
        let imageHeight = imageView?.intrinsicContentSize.height ?? 0
                  
        let labelWidth = titleLabel?.intrinsicContentSize.width ?? 0
        let labelHeight = titleLabel?.intrinsicContentSize.height ?? 0
                
        //初始化imageEdgeInsets和labelEdgeInsets
        var imageEdgeInsets = UIEdgeInsets.zero
        var labelEdgeInsets = UIEdgeInsets.zero
                
        switch imagePosition {
        case .top:
            //上 左 下 右
            imageEdgeInsets = UIEdgeInsets(top: -labelHeight-spacingBetweenImageAndTitle/2, left: 0, bottom: 0, right: -labelWidth)
            labelEdgeInsets = UIEdgeInsets(top: 0, left: -imageWidth, bottom: -imageHeight-spacingBetweenImageAndTitle/2, right: 0)
            break;
                    
        case .left:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: -spacingBetweenImageAndTitle/2, bottom: 0, right: spacingBetweenImageAndTitle)
            labelEdgeInsets = UIEdgeInsets(top: 0, left: spacingBetweenImageAndTitle/2, bottom: 0, right: -spacingBetweenImageAndTitle/2)
            break;
                    
        case .bottom:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: -labelHeight-spacingBetweenImageAndTitle/2, right: -labelWidth)
            labelEdgeInsets = UIEdgeInsets(top: -imageHeight-spacingBetweenImageAndTitle/2, left: -imageWidth, bottom: 0, right: 0)
            break;
                    
        case .right:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: labelWidth+spacingBetweenImageAndTitle/2, bottom: 0, right: -labelWidth-spacingBetweenImageAndTitle/2)
            labelEdgeInsets = UIEdgeInsets(top: 0, left: -imageWidth-spacingBetweenImageAndTitle/2, bottom: 0, right: imageWidth+spacingBetweenImageAndTitle/2)
            break;
                    
        }
                
        self.titleEdgeInsets = labelEdgeInsets
        self.imageEdgeInsets = imageEdgeInsets
        
    }
}


