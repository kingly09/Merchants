//
//  KSMessageManager.swift
//  KaisafinStockSwift
//
//  Created by ht on 2020/4/17.
//  Copyright © 2020 administrator. All rights reserved.
//

import UIKit

class BYMessageManager: NSObject {

    static let sharedInstace = BYMessageManager.init()
   
    lazy var redDots : [UIView] = []
    
    private override init(){
        super.init()
    }
    
    func messageBtn () -> UIButton {
        let msgBtn = Button(image: BYUserDeaultMacro.readBool(key: "noti_show_red_dot") ? "nav_message_red_dot" : "nav_message")
        msgBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        msgBtn.addTarget(self, action: #selector(message), for: .touchUpInside)
        return msgBtn
    }
    
    func searchBtn () -> UIButton {
        let button = Button(image: BYUserDeaultMacro.readBool(key: "noti_show_red_dot") ? "nav_message_red_dot" : "nav_message")
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.addTarget(self, action: #selector(message), for: .touchUpInside)
        return button
    }
    
    func serviceBtn () -> UIButton {
        let button = Button(image: BYUserDeaultMacro.readBool(key: "noti_show_red_dot") ? "nav_message_red_dot" : "nav_message")
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.addTarget(self, action: #selector(message), for: .touchUpInside)
        return button
    }
    
    @objc private func message() {
//        MGJRouter.open(BaseURLSchemes+MineRouterUrl.Noti.rawValue)
    }
    
}
