//
//  KSGetuiNotiModel.swift
//  KaisafinStockSwift
//
//  Created by ht on 2020/7/17.
//  Copyright © 2020 administrator. All rights reserved.
//

import UIKit

class BYGetuiNotiModel: BYBaseModel {
    
    var title: String = ""
    
    var transmissionContent: String = ""
    
    var contentAvailable: Int = 0
    
    var msgCode: String = ""
    
}
