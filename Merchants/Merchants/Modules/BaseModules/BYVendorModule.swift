//
//  BYVendorModule.swift
//  Merchants
//
//  Created by kingly09 on 2020/7/26.
//  Copyright © 2020 kingly09. All rights reserved.
//

import UIKit
import TXLiteAVSDK_Professional

let WXOAuthState = "wx_oauth_authorization_state"
let WXScope = "snsapi_userinfo"

class BYVendorModule: NSObject, ModuleManagerDelegate{
    
    static let WXAppId = "wx4427af5edb767e33"
    
    static let WXAppSecret = "d9a068dde00610769c19482407216e2e"
    
    static let UNIVERSALLINK = "https://help.wechat.com/sdksample/"
    
    static let licenceURL = "http://license.vod2.myqcloud.com/license/v1/0d04ee897afc7cc9210b785c3f12f896/TXLiveSDK.licence"
    
    static let licenceKey = "54ca9ce8fb80eac9d0a22a7187779a5c"
    
    static let UMAppKey = "5efaebc6570df3545e0001cb"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]?) {
        
        UMCommonLogManager.setUp()
        UMConfigure.initWithAppkey(BYVendorModule.UMAppKey, channel: "")
        
        
        confitUShareSettings()
        configUSharePlatforms()
        
        WXApi.startLog(by: .detail) { (log) in
            print(log)
        }
        
//        if WXApi.registerApp(BYVendorModule.WXAppId, universalLink: BYVendorModule.UNIVERSALLINK){
//            print("微信模块初始化成功")
//            WXApi.checkUniversalLinkReady { (step, result) in
//                print("\(step.rawValue), \(result.success), \(result.errorInfo), \(result.suggestion)")
//            }
//        }
        
        //腾讯直播
        TXLiveBase.setLicenceURL(BYVendorModule.licenceURL, key: BYVendorModule.licenceKey)
        print("TXLiteAVSDK_Professional SDK Version = " + TXLiveBase.getSDKVersionStr())
        TXLiveBase.setConsoleEnabled(true)
    }
}

extension BYVendorModule{
    class func wxLogin(){
//        if WXApi.isWXAppInstalled(){
//            let req = SendAuthReq()
//            req.state = WXOAuthState//用于保持请求和回调的状态，授权请求或原样带回
//            req.scope = WXScope//授权作用域：获取用户个人信息
//            WXApi.send(req) { (success) in
//                print("调用微信" + (success ? "成功" : "失败"))
//            }
//        }else{
//            BYToast(text: "请先安装微信").show()
//        }
        UMSocialManager.default()?.getUserInfo(with: .wechatSession, currentViewController: nil, completion: { (result, error) in
            if let resp = result as? UMSocialUserInfoResponse{
                // 第三方登录数据(为空表示平台未提供)
                // 授权数据
                print(" uid: " + resp.uid)
                print(" openid: " + resp.openid)
                print(" accessToken: " + resp.accessToken)
                print(" refreshToken: " + resp.refreshToken)
                print(" expiration: \(String(describing: resp.expiration))")
                // 用户数据
                print(" name: " + resp.name)
                print(" iconurl: " + resp.iconurl)
                print(" gender: " + resp.unionGender)
                // 第三方平台SDK原始数据
                print(" originalResponse: \(String(describing: resp.originalResponse))")
            }
        })
    }
    
    func confitUShareSettings(){
        UMSocialGlobal.shareInstance()?.isUsingWaterMark = true
        UMSocialGlobal.shareInstance()?.universalLinkDic = [NSNumber(integerLiteral: UMSocialPlatformType.wechatSession.rawValue) : BYVendorModule.UNIVERSALLINK]
    }
    
    func configUSharePlatforms(){
        UMSocialManager.default()?.setPlaform(.wechatSession, appKey: BYVendorModule.WXAppId, appSecret: BYVendorModule.WXAppSecret, redirectURL: "http://mobile.umeng.com/social")
        UMSocialManager.default()?.setLauchFrom(.wechatSession, completion: { (userInfoResponse, error) in
            print("setLauchFromPlatform:userInfoResponse:\(userInfoResponse ?? "--")")
        })
        
        UMSocialManager.default()?.removePlatformProvider(with: .wechatFavorite)
        
    }
}
