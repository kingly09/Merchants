//
//  ComponentMoudel.swift
//  KaisafinStockSwift
//
//  Created by administrator on 2019/12/20.
//  Copyright © 2019 administrator. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift

class UIModule : NSObject, ModuleManagerDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]?){
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "完成"
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.toolbarManageBehaviour = .bySubviews
    }
    
}
