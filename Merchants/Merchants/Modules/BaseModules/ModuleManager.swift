//
//  ModuleManager.swift
//  KaisafinStockSwift
//
//  Created by administrator on 2019/12/20.
//  Copyright © 2019 administrator. All rights reserved.
//

import Foundation
import UIKit

public protocol ModuleManagerDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
}


public class ModuleManager : NSObject {
    
    static  let sharedInstace = ModuleManager.init()
    private override init(){
        super.init()
    }
    
    public func loadModule(_ module: ModuleManagerDelegate? ){
         if((module) != nil){
             self.allModules.append(module!)
         }
     }
    
    public func loadAllModule(modules:[ModuleManagerDelegate]? ){
        if(modules != nil){
            self.allModules.removeAll()
            for item in modules!{
                self.allModules.append(item)
            }
        }
    }
    
    public  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:[UIApplication.LaunchOptionsKey : Any]?) {
        for  service in self.allModules {
            let serviceDelegate  =  service
            serviceDelegate.application(application, didFinishLaunchingWithOptions: launchOptions)
        }
    }
    
    
    lazy var allModules: [ModuleManagerDelegate] = {
         var array = [ModuleManagerDelegate]()
         return array
     }()
    
}
