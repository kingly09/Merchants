//
//  BYRouterModule.swift
//  Merchants
//
//  Created by kingly09 on 2020/7/16.
//  Copyright © 2020 kingly09. All rights reserved.
//

import UIKit
import WebKit
import RTRootNavigationController

class BYRouterModule: NSObject, ModuleManagerDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]?) {
        
        CommonURLRegister.registerURL()
      
        MBWorkUrlRouter.registerURL()
        MBMerCenterUrlRouter.registerURL()
        MBMsgCenterUrlRouter.registerURL()
        
    }
}

enum CommonURLType: String{
    case WebView
    case Search
    
    //登录相关
    case Login
    case OtherLogin
    case LoginByCode
}

class CommonURLRegister: NSObject {
    
    class func registerURL(){
        // MARK: -跳转通用webView
        MGJRouter.registerWithHandler(BaseURLSchemes + CommonURLType.WebView.rawValue) { (params) in
            var jumpMode = 0
            let vc = BYCommonWebController()
            if let dic = params?[MGJRouterParameterUserInfo] as? [String : Any]{
                if let title = dic["title"] as? String{
                    vc.title = title
                }
                if let url = dic["url"] as? String{
                    vc.url = url
                }
                if let type = dic["jumpMode"] as? Int{
                    jumpMode = type
                }
            }
            if jumpMode == 1{
                let nav = RTRootNavigationController(rootViewController: vc)
                nav.modalPresentationStyle = .fullScreen
                topViewController(nil)?.present(nav, animated: true, completion: nil)
            }else{
                topViewController(nil)?.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
        // MARK: -跳转搜索
//        MGJRouter.registerWithHandler(BaseURLSchemes + CommonURLType.Search.rawValue) { (params) in
//            let vc = BYSearchViewController()
//            if let dic = params?[MGJRouterParameterUserInfo] as? [String : Any]{
//                if let searchText = dic["searchText"] as? String{
//                    vc.searchText = searchText
//                }
//            }
//            vc.modalPresentationStyle = .fullScreen
//            let nav = RTRootNavigationController(rootViewController: vc)
//            nav.modalPresentationStyle = .fullScreen
//            topViewController(nil)?.present(nav, animated: true, completion: nil)
//        }
        
        // MARK: -跳转登陆页
//        MGJRouter.registerWithHandler(BaseURLSchemes + CommonURLType.Login.rawValue) { (params) in
//            let dic = params?[MGJRouterParameterUserInfo] as? [String : Any]
//            let vc = BYLoginViewController()
//            vc.modalPresentationStyle = .fullScreen
//            vc.loginStatus = dic?["loginStatus"] as? LoginStatusBlock
//            let nav = UINavigationController(rootViewController: vc)
//            nav.modalPresentationStyle = .fullScreen
//            topViewController(nil)?.present(nav, animated: true, completion: {
//                let completion = params?[MGJRouterParameterCompletion] as? ((Any?)->())
//                completion?(nil)
//            })
//        }
//
        // MARK: -跳转第三方登录登陆页
//        MGJRouter.registerWithHandler(BaseURLSchemes + CommonURLType.OtherLogin.rawValue) { (params) in
//            let dic = params?[MGJRouterParameterUserInfo] as? [String : Any]
//            let vc = BYOtherLoginController()
//            vc.loginStatus = dic?["loginStatus"] as? LoginStatusBlock
//            if let topVC = topViewController(nil) as? BYLoginViewController{
//                vc.loginStatus = topVC.loginStatus
//            }
//            topViewController(nil)?.navigationController?.pushViewController(vc, animated: true)
//        }
        
        // MARK: - 跳转到验证码登录界面
        MGJRouter.registerWithHandler(BaseURLSchemes + CommonURLType.LoginByCode.rawValue) { (params) in
            let dic = params?[MGJRouterParameterUserInfo] as? [String : Any]
            let vc = MBLoginByCodeVC()
            vc.phonetext = dic?["phonetext"] as? String
            topViewController(nil)?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension MGJRouter{
    
    public class func open(_ url: String, needLogin: Bool = false) {
        if needLogin  {
            if !MBUserInfo.shared.isLogin() {
                MGJRouter.open(BaseURLSchemes + CommonURLType.Login.rawValue)
                return
            }
        }
        MGJRouter.open(url, nil)
    }
}

extension NSObject{
    
    //类方法
    class func topViewController(_ currentVC: UIViewController?) -> UIViewController? {
        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else {return nil}
        let topVC = currentVC ?? rootVC
        
        switch topVC {
        case is UITabBarController:
            if let top = (topVC as! UITabBarController).selectedViewController { return topViewController(top)}
            else { return nil }
        case is UINavigationController:
            if let top = (topVC as! UINavigationController).topViewController { return topViewController(top) }
            else { return nil }
        default:
            if topVC.presentedViewController != nil{
                return topViewController(topVC.presentedViewController)
            }else{
                return topVC
            }
            
        }
    }
    
    //实例方法
    public func topViewController(_ currentVC: UIViewController?) -> UIViewController?{
        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else {return nil}
        let topVC = currentVC ?? rootVC
        
        switch topVC {
        case is UITabBarController:
            if let top = (topVC as! UITabBarController).selectedViewController { return topViewController(top)}
            else { return nil }
        case is UINavigationController:
            if let top = (topVC as! UINavigationController).topViewController { return topViewController(top) }
            else { return nil }
        default:
            if topVC.presentedViewController != nil{
                return topViewController(topVC.presentedViewController)
            }else{
                return topVC
            }
        }
    }

}
