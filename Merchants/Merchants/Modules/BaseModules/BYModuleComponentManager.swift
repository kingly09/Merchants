//
//  BYModuleComponentManager.swift
//  Merchants
//
//  Created by kingly09 on 2020/7/13.
//  Copyright © 2020 kingly09. All rights reserved.
//

import UIKit
import Foundation
@_exported import HandyJSON
@_exported import Toaster
@_exported import Moya

public class BYModuleComponentManager {
    public class func  registerAllService(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:[UIApplication.LaunchOptionsKey : Any]?) {
        
        //配置module
        let modeArr:[ModuleManagerDelegate] = [UIModule(), GetuiModule(), BYRouterModule(), BYVendorModule()]
        ModuleManager.sharedInstace.loadAllModule(modules: modeArr)
        ModuleManager.sharedInstace.application(application,didFinishLaunchingWithOptions: launchOptions)
    }
}
