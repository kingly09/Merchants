//
//  MBBaseURLRouter.swift
//  Merchants
//
//  Created by kingly on 2020/7/21.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

class MBBaseURLRouter: NSObject {
   
    class func registerURL(){
        
    }
    
    class  func registerWithHandler(routerUrl : String,JumpViewController: BCMainBaseViewController ){
        
        MGJRouter.registerWithHandler(routerUrl) { (registerparams) in
            let vc = JumpViewController
            vc.hidesBottomBarWhenPushed = true
            vc.params = registerparams!
            topViewController(nil)?.navigationController?.pushViewController(vc, animated: true)
           }
    }
    
    /// 订单管理
          
}
