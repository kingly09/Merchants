//
//  MBWorkURLRouter.swift
//  Merchants
//
//  Created by kingly on 2020/7/21.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit


class MBWorkUrlRouter: MBBaseURLRouter {
    
      override class func registerURL(){
        super.registerURL()
        
        /// 订单管理
        MBBaseURLRouter.registerWithHandler(routerUrl:MBUrl.Router.Work.Order.OrderManage, JumpViewController: MBOrderManageVC())
        
        /// 商品管理
         MBBaseURLRouter.registerWithHandler(routerUrl:MBUrl.Router.Work.Commod.CommodManage, JumpViewController: MBOrderManageVC())
        
        /// 直播间
        MBBaseURLRouter.registerWithHandler(routerUrl:MBUrl.Router.Work.Live.LiveRoom.Open, JumpViewController: MBLiveRoomVC())
        
        /// 直播计划
        MBBaseURLRouter.registerWithHandler(routerUrl:MBUrl.Router.Work.Live.LiveAnnounce.List, JumpViewController: MBLiveAnnounceVC())
        
        
        
        
    }
    
}
