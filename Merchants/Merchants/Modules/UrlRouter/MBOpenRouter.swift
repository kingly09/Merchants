//
//  MBOpenRouter.swift
//  Merchants
//
//  Created by kingly on 2020/7/29.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

class MBOpenRouter: NSObject {

     static let shared = MBOpenRouter()
   
    /// 直播计划
    static func  livePlan(){
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.live.plan, "title" : MBStr.Work.liveAnnounce, "jumpMode" : 0], nil)
    }
    
    /// 直播记录
    static func  liveRecord(){
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.live.record, "title" : MBStr.Work.liveRecording, "jumpMode" : 0], nil)
    }
    /// 主播管理
    static func  anchorManage(){
         MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.live.anchorManage, "title" : MBStr.Work.anchorManage, "jumpMode" : 0], nil)
    }
    
    /// 申请入驻
    static func  settledIndex(){
    
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.settled.index, "title" : MBStr.Work.applyResidence, "jumpMode" : 0], nil)
        
        
    }
    /// 订单管理
    static func  orderList(){
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.order.orderList, "title" : MBStr.Work.orderManage, "jumpMode" : 0], nil)
    }
    /// 商品管理
    static func commodManage() {
        
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.product.list, "title" : MBStr.Work.commodManage, "jumpMode" : 0], nil)
    }
    /// 营销管理
    static func marketing() {
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.marketing.index, "title" : MBStr.Work.marketingManage, "jumpMode" : 0], nil)
    }
    /// 我的精品库
    static func qualityGoodsMe() {
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.qualityGoods.me, "title" : MBStr.Work.myBoutique, "jumpMode" : 0], nil)
    }
    /// 公有私有库
    static func qualityGoodsPublics() {
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.qualityGoods.publics, "title" : MBStr.Work.publicGoods, "jumpMode" : 0], nil)
    }
    /// 活动申报
    static func  activityIndex() {
    
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.activity.index, "title" : MBStr.Work.activityDec, "jumpMode" : 0], nil)
    }
    
    /// 店铺设置
    static func  businessDetail() {
    
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.merCenter.businessDetail, "title" : MBStr.MerCenter.shopsetting, "jumpMode" : 0], nil)
    }
    
    /// 编辑资料
    static func businessedit() {
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.merCenter.businessedit, "title" : MBStr.MerCenter.shopsetting, "jumpMode" : 0], nil)
    }
    
    /// 账户中心
    static func  merCenterAccount() {
    
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.merCenter.account, "title" : MBStr.MerCenter.accsetting, "jumpMode" : 0], nil)
    }
    
    /// 角色管理
    static func  merCenterRole() {
    
        MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : MBUrl.h5.merCenter.role, "title" : MBStr.MerCenter.rolesetting, "jumpMode" : 0], nil)
    }
    
    
}
