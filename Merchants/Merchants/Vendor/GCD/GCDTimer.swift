//
//  GCDTimer.swift
//  Swift-GCD
//
//  Created by kingly on 2017/9/20.
//  Copyright © 2017年 kingly. All rights reserved.
//

import UIKit

class GCDTimer {
  
    // 间隔定时器, 相当于repeats设置为true的Timer.
    public let dispatchSourceTimer : DispatchSourceTimer
  
  
    /// 间隔定时器, 相当于repeats设置为true的Timer.
    ///
    /// - Parameters:
    ///   - in: 当前队列
    ///   - delay: 截止时间, 计时器最迟开始时间;
    ///   - interval: 时间间隔
    init(in : GCDQueue, delay : Float = 0, interval : Float) {
        
        dispatchSourceTimer = DispatchSource.makeTimerSource(flags: [], queue: `in`.dispatchQueue)
        dispatchSourceTimer.schedule(deadline: .now() + .milliseconds(Int(delay * 1000)), repeating: .milliseconds(Int(interval * 1000)))
    }
    
    /// 设定定时器任务的回调函数
    ///
    /// - Parameter eventHandler: 回调函数
    func setTimerEventHandler(eventHandler: @escaping (GCDTimer)-> Void) {
        
        dispatchSourceTimer.setEventHandler {
            
            eventHandler(self)
        }
    }
    
    /// 设定定时器销毁时候的回调函数
    ///
    /// - Parameter eventHandler: 回调函数
    func setDestroyEventHandler(eventHandler: @escaping ()-> Void) {
    
        dispatchSourceTimer.setCancelHandler {
            
            eventHandler()
        }
    }
    
    /// 挂起
    func suspend() {
        
        dispatchSourceTimer.suspend()
    }
    
    /// 开始定时
    func start() {
        
        dispatchSourceTimer.resume()
    }
    
    /// 定时器销毁(执行了此方法后,start就会变得无效)
    func destroy() {
        
        dispatchSourceTimer.cancel()
    }
}
