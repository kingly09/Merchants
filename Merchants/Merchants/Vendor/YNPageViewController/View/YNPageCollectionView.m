//
//  YNPageCollectionView.m
//  YNPageViewController
//
//  Created by ZYN on2020/7/14.
//  Copyright ©2020年 yongneng. All rights reserved.
//

#import "YNPageCollectionView.h"

@interface YNPageCollectionView () <UIGestureRecognizerDelegate>

@end

@implementation YNPageCollectionView

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

@end
