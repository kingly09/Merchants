//
//  MBLiveAnnounceModel.swift
//  Merchants
//
//  Created by kingly on 2020/7/28.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

class MBLiveAnnounceModel : JSONMappable {

    var appLogoName: String
    var titleStr: String
    
    required init(jsonData: JSON) {
      
      appLogoName   = jsonData["appLogoName"].stringValue
      titleStr      = jsonData["titleStr"].stringValue
      
    }
}
