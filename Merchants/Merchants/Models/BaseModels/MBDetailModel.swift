//
//  MBDetailModel.swift
//  Merchants
//
//  Created by kingly on 2020/7/20.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

class MBDetailModel: JSONMappable {

    ///  应用logo
    var appLogoName: String
    /// 应用名称
    var titleStr: String
    
    required init(jsonData: JSON) {
      
      appLogoName   = jsonData["appLogoName"].stringValue
      titleStr      = jsonData["titleStr"].stringValue
      
    }
  
  
}
