//
//  MBSettingModel.swift
//  Merchants
//
//  Created by kingly on 2020/7/20.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

class MBSettingModel: JSONMappable {
  
  /// 图片名称
  var imageName : String
  
  /// 标题
  var title : String
  
  /// 说明
  var detail : String
    
  /// 时间
  var timetext : String = ""
  
  
  required init(jsonData: JSON) {
    
    imageName        = jsonData["imageName"].stringValue
    title            = jsonData["title"].stringValue
    detail           = jsonData["detail"].stringValue
    timetext         = jsonData["timetext"].stringValue
    
  }
}
