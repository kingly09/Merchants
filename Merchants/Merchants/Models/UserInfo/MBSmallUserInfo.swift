//
//  MBSmallUserInfo.swift
//  Merchants
//
//  Created by kingly on 2020/7/16.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

/// 登录返回结构体
@objc internal  class MBLoginRspInfo :  NSObject, JSONMappable {
  
  @objc var merToken: String?

  var userInfo: MBSmallUserInfo?
  
  required init(jsonData: JSON) {

    if jsonData["merToken"].stringValue.ky_stringLength() > 0  {
         self.merToken    = jsonData["merToken"].stringValue
    }else{
         self.merToken    = jsonData["token"].stringValue
    }
      
    self.userInfo    = MBSmallUserInfo(jsonData: jsonData["userInfo"])
    
  }
}


/// 功能名称模型
class VJFuntionModel : JSONMappable {

   /// 功能名称
   @objc var  permissionName : String

   required init(jsonData: JSON) {
    
     self.permissionName  = jsonData["permissionName"].stringValue
   
  }
  
}

/// 我的钱包
class MBWalletEntityModel : JSONMappable {
      
       /// 序号
      @objc var  wid : String
       ///我的钱包id
      @objc var merchantId : String
      ///  待提现金额
      @objc var noWithdrawMoney : Int
      /// 红包余额
      @objc var redPacket : Int
      ///  待结算余额
      @objc var waitAccount : Int
      /// 已提现金额
      @objc var isWithdrawMoney : Int

      required init(jsonData: JSON) {
       
        self.wid         = jsonData["id"].stringValue
        self.merchantId  = jsonData["merchantId"].stringValue
        self.noWithdrawMoney  = jsonData["noWithdrawMoney"].intValue
        self.redPacket    = jsonData["redPacket"].intValue
        self.waitAccount  = jsonData["waitAccount"].intValue
        self.isWithdrawMoney = jsonData["isWithdrawMoney"].intValue
      
     }
}


///  商户状态  0：禁用 1：启用
@objc  enum  USER_MERCHANT_STATUS : Int {
  case MERCHANT_STATUS_DISABLE  = 0
  case MERCHANT_STATUS_ENABLE   = 1
}


/// 商户端用户信息
@objc class MBSmallUserInfo : NSObject, JSONMappable {
  
  ///  是否开通店铺
  @objc var isHaveShop : Int
  ///  是否开通直播间
  @objc var isHaveLive : Int
     
  /// 默认是启用
  @objc var merchantStatus : USER_MERCHANT_STATUS = .MERCHANT_STATUS_ENABLE

   /// 商户角色id 0为商户 其余的参照 merchantRoleName 字段
   @objc var accountIdentity : Int
   
    ///  店铺状态 0:申请中 1：申请成功 2：审核失败 4：未申请
   @objc var shopsAuditStatus: Int
    
   ///   直播间状态 0:申请中 1：申请成功 2：审核失败 4：未申请
   @objc var storeAuditStatus : Int

   /// 商户角色名称
   @objc var  merchantRoleName : String

   /// 手机号码
   @objc var telephoneNumber : String

   /// 名称
   @objc var businessName : String
   /// 头像
   @objc var businessHeadImg : String

   /// 操作入口列表
   var commonFuntion: [VJFuntionModel]?
   /// 直播入口列表
   var liveFuntion: [VJFuntionModel]?
    
   var walletEntity : MBWalletEntityModel?
     
   required init(jsonData: JSON) {
     
      businessHeadImg      = jsonData["businessHeadImg"].stringValue
      businessName         = jsonData["businessName"].stringValue
      telephoneNumber      = jsonData["phone"].stringValue
      accountIdentity      = jsonData["accountIdentity"].intValue
      merchantRoleName     = jsonData["merchantRoleName"].stringValue
      merchantStatus       = USER_MERCHANT_STATUS(rawValue: jsonData["merchantStatus"].intValue)!
      isHaveLive           = jsonData["isHaveLive"].intValue
      isHaveShop           = jsonData["isHaveShop"].intValue
    
      shopsAuditStatus      = jsonData["shopsAuditStatus"].intValue
      storeAuditStatus      = jsonData["storeAuditStatus"].intValue
     

    commonFuntion         =  jsonData["commonFuntion"].arrayValue.map({ (json) -> VJFuntionModel in
       VJFuntionModel(jsonData: json)
     })
    
     
     liveFuntion         = jsonData["liveFuntion"].arrayValue.map({ (json) -> VJFuntionModel in
       VJFuntionModel(jsonData: json)
     })
    
     walletEntity        =  MBWalletEntityModel(jsonData: jsonData["walletEntity"])
     
   }
  
}
