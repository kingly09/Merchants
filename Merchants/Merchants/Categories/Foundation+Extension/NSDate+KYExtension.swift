//
//  NSDate+KYExtension.swift
//  KFManager
//
//  Created by kingly on 2016/11/15.
//  Copyright © 2016年 kingly inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE. All rights reserved.
//

import UIKit

// MARK: - NSDate

public extension NSDate {
    
    /// 根据时间格式 显示对应的时间 （譬如 yyy/MM/dd  yyyy-MM-dd hh:mm:ss）
    ///
    /// - Parameter dateFormat: 时间格式
    /// - Returns: 返回对应的时间格式
    func ky_stringFromDate(dateFormat: String?) -> String {
        let dateFormatter = DateFormatter()
        if dateFormat != nil {
            dateFormatter.dateFormat = dateFormat
        } else {
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        }
        
        return dateFormatter.string(from: self as Date)
   }
    
  /// 获得当前时间的时间戳
  ///
  /// - Returns: 返回当前时间的时间戳
  func ky_currTimeStamp() -> Int64 {
    
    //获取当前时间
    let now = NSDate()
    // 创建一个日期格式器
    let dformatter =  DateFormatter()
    dformatter.dateFormat = "yyyy年MM月dd日 HH:mm:ss"
    
    //当前时间的时间戳
    let timeInterval:TimeInterval = now.timeIntervalSince1970
    let timeStamp = Int64(timeInterval)
    
    return timeStamp;
    
  }
  
  /// 根据Int64时间戳转换成NSDate时间
  ///
  /// - Parameter timeStamp: Int64位时间戳
  /// - Returns: 返回NSDate时间
  func ky_timeStampToDate(timeStamp: Int64) -> NSDate {
    
      let timeInterval:TimeInterval = TimeInterval(timeStamp)
      let date = NSDate(timeIntervalSince1970: timeInterval)
      
      return date;
  }
  
  ///  根据Int64时间戳转换成对应的时间格式字符串
  ///
  /// - Parameters:
  ///   - timeStamp: Int64位时间戳
  ///   - dateFormat: 时间格式 譬如 yyy/MM/dd  yyyy-MM-dd hh:mm:ss）
  /// - Returns: 返回对应的时间格式字符串
  func ky_timeStampToSting(timeStamp: Int64,dateFormat: String?) -> String {
       let date =  ky_timeStampToDate(timeStamp: timeStamp)
       return  date.ky_stringFromDate(dateFormat: dateFormat)
  }
}
