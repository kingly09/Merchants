//
//  KSUserDeaultMacro.swift
//  KaisafinStockSwift
//
//  Created by 云 on 2019/12/25.
//  Copyright © 2019 administrator. All rights reserved.
//

import Foundation

let Kaisa_search_history_data = "Kaisa_search_history_data"
let Noti_tip_close_time = "noti_tip_close_time"
let response_time = "response_time"
let geTuiClientID = "geTuiClientID"
let rate_save_time = "rate_respose_time"
let rate_save_data = "rate_respose_data"
var hiddenPrivacyProtocol = "hiddenPrivacyProtocol"

class BYUserDeaultMacro: NSObject {
    
    /**
    *  保存字符串配置属性
    *
    *  @param key   保存的key
    *  @param value 保存对应的值
    */
    class func saveString(value:String, key:String) {
        let defauls = UserDefaults.standard
        defauls.set(value, forKey: key)
        defauls.synchronize()
    }
    
    /**
    *  读取保存的配置信息
    *
    *  @param key 保存的key
    *
    *  @return String
    */
    class func readString(key:String) -> String {
        let defauls = UserDefaults.standard
        guard let str = defauls.string(forKey: key) else {
            return ""
        }
        return str
    }
    
    /**
    *  Int
    */
    class func saveInt(value:Int, key:String) {
        let defauls = UserDefaults.standard
        defauls.set(value, forKey: key)
        defauls.synchronize()
    }
    
    class func readInt(key:String) -> Int {
        let defauls = UserDefaults.standard
        return defauls.integer(forKey: key)
    }
    
    /**
    *  Double
    */
    class func saveDouble(value:Double, key:String) {
        let defauls = UserDefaults.standard
        defauls.set(value, forKey: key)
        defauls.synchronize()
    }
    
    class func readDouble(key:String) -> Double {
        let defauls = UserDefaults.standard
        return defauls.double(forKey: key)
    }
    
    /**
    *  Float
    */
    class func saveFloat(value:Float, key:String) {
        let defauls = UserDefaults.standard
        defauls.set(value, forKey: key)
        defauls.synchronize()
    }
    
    class func readFloat(key:String) -> Float {
        let defauls = UserDefaults.standard
        return defauls.float(forKey: key)
    }
    
    /**
    *  Bool
    */
    class func saveBool(value:Bool, key:String) {
        let defauls = UserDefaults.standard
        defauls.set(value, forKey: key)
        defauls.synchronize()
    }
    
    class func readBool(key:String) -> Bool {
        let defauls = UserDefaults.standard
        return defauls.bool(forKey: key)
    }
    
    /**
    *  Object
    */
    class func saveObject(value:AnyObject, key:String) {
        let data = NSKeyedArchiver.archivedData(withRootObject: value)
        let defauls = UserDefaults.standard
        defauls.set(data, forKey: key)
        defauls.synchronize()
    }
    
    class func readObject(key:String) -> (isReadSuccess:Bool, dataObject:AnyObject) {
        let defauls = UserDefaults.standard
        guard let data = defauls.value(forKey: key) as? NSData else {
            return (false, NSNull())
        }
        return (true, NSKeyedUnarchiver.unarchiveObject(with: data as Data)! as AnyObject)
    }
    
    class func removeObject(key:String) {
        let defauls = UserDefaults.standard
        defauls.removeObject(forKey: key)
        defauls.synchronize()
    }
    
}
