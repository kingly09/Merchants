//
//  Bundle+KYExtension.swift
//  KFManager
//
//  Created by kingly on 2016/11/15.
//  Copyright © 2016年 kingly inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE. All rights reserved.
//


import Foundation

extension Bundle {

    //计算性属性 类似于函数 没有参数 有返回值
    var ky_nameSpace : String {

        return infoDictionary?["KFMNameSpace"] as? String ?? ""
   }
  
   var ky_appSeverStatus : Bool {
      let infoDict = Bundle.main.infoDictionary! as Dictionary<String, AnyObject>
      return infoDict["AppSeverStatus"] as! Bool
  }
  
  var  ky_appVersion : String {
    let infoDict = Bundle.main.infoDictionary! as Dictionary<String, AnyObject>
    return infoDict["CFBundleShortVersionString"] as! String
  }
  
  /// 获得 CFBundleVersion
  var ky_appBuildVersion : String {
    let infoDict = Bundle.main.infoDictionary! as Dictionary<String, AnyObject>
    return infoDict["CFBundleVersion"] as! String
  }
    
}
