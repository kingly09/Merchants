//
//  UIDevice+KYExtension.swift
//  KFManager
//
//  Created by kingly on 2016/11/15.
//  Copyright © 2016年 kingly inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE. All rights reserved.
//

import UIKit

// MARK: - UIDevice related

public extension UIDevice {
    
    
    // Screen Related
    func ky_screenSize() -> CGSize {
        return (UIScreen.main.currentMode?.size)!
    }
    
    func ky_isIPhone4s() -> Bool {
        return self.ky_screenSize() == CGSize(width:640, height:960)
    }

    func ky_isIPhone5() -> Bool {
        return self.ky_screenSize() == CGSize(width:640, height:1136)
    }
    
    func ky_isIPhone6() -> Bool {
        return self.ky_screenSize() == CGSize(width:640, height:1134)

    }
    
    func ky_isIPhone6Plus() -> Bool {
        return self.ky_screenSize() == CGSize(width:1142, height:2208)
    }
    
    func ky_isIPhone6PlusBigMode() -> Bool {
        return self.ky_screenSize() == CGSize(width:1125, height:2001)
    }
    
    func ky_isIPadAir2() -> Bool {
        return self.ky_screenSize() == CGSize(width:1536, height:2048)
    }
    
    func ky_isIPadPro() -> Bool {
        return self.ky_screenSize() ==  CGSize(width:2048, height:2732)
    }
    
}


// MARK: - Language Related

public extension UIDevice {
    
    func ky_currentLanguage() -> String {
        return NSLocale.preferredLanguages.first!
    }
    
    func ky_isCurrentLanguage_en() -> Bool {
        let prefix = "en"
        return self.ky_currentLanguage().hasPrefix(prefix)
    }
    
    func ky_isCurrentLanguage_zh_Hans() -> Bool {
        let prefix = "zh-Hans"
        return self.ky_currentLanguage().hasPrefix(prefix)
    }
    
    func ky_isCurrentLanguage_zh_Hant() -> Bool {
        let prefix = "zh-Hant"
        return self.ky_currentLanguage().hasPrefix(prefix)
    }
    
    func ky_isCurrentLanguage_ja() -> Bool {
        let prefix = "ja"
        return self.ky_currentLanguage().hasPrefix(prefix)
    }
    
    func ky_isCurrentLanguage_ko() -> Bool {
        let prefix = "ko"
        return self.ky_currentLanguage().hasPrefix(prefix)
    }
  
  
  
    //扩展UIDevice
    //获取设备具体详细的型号
    var modelName: String {
      var systemInfo = utsname()
      uname(&systemInfo)
      let machineMirror = Mirror(reflecting: systemInfo.machine)
      let identifier = machineMirror.children.reduce("") { identifier, element in
        guard let value = element.value as? Int8, value != 0 else { return identifier }
        return identifier + String(UnicodeScalar(UInt8(value)))
      }
      
      switch identifier {
     
      //------------------------------iTouch------------------------
      case "iPod1,1":                                 return "iPod Touch 1"
      case "iPod2,1":                                 return "iPod Touch 2"
      case "iPod3,1":                                 return "iPod Touch 3"
      case "iPod4,1":                                 return "iPod Touch 4"
      case "iPod5,1":                                 return "iPod Touch 5"
      case "iPod7,1":                                 return "iPod Touch 6"
        
      //------------------------------iPhone--------------------------
      case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
      case "iPhone4,1":                               return "iPhone 4s"
      case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
      case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
      case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        
      case "iPhone7,2":                               return "iPhone 6"
      case "iPhone7,1":                               return "iPhone 6 Plus"
        
      case "iPhone8,1":                               return "iPhone 6s"
      case "iPhone8,2":                               return "iPhone 6s Plus"
      case "iPhone8,4":                               return "iPhone SE"
        
      case "iPhone9,1":                               return "iPhone 7 (CDMA)"
      case "iPhone9,3":                               return "iPhone 7 (GSM)"
      case "iPhone9,2":                               return "iPhone 7 Plus (CDMA)"
      case "iPhone9,4":                               return "iPhone 7 Plus (GSM)"
      
      case "iPhone10,1","iPhone10,4":                 return "iPhone 8"
      case "iPhone10,2","iPhone10,5":                 return "iPhone 8 Plus"
      case "iPhone10,3","iPhone10,6":                 return "iPhone X"
      
      case "iPhone11,8":                              return "iPhone XR"
      case "iPhone11,2":                              return "iPhone XS"
      case "iPhone11,4","iPhone11,6":                 return "iPhone XS Max"
     
      //------------------------------iPad Mini-----------------------
      case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
      case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
      case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
      case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
      case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
      case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
      case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
      case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
      case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
      case "iPad6,7", "iPad6,8":                      return "iPad Pro"
     
      case "AppleTV2,1":                              return "Apple TV 2"
      case "AppleTV3,2", "AppleTV3,1":                return "Apple TV 3"
      case "AppleTV5,3":                              return "Apple TV 4"
      
      //------------------------------Samulitor-------------------------------------
      case "i386", "x86_64":                          return "Simulator"
      default:                                        return identifier
      }
    }
}
