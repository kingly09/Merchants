//
//  UIAlertController+extension.swift
//  Merchants
//
//  Created by kingly on 2020/7/21.
//  Copyright © 2020年 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

extension UIAlertController {
  //在指定视图控制器上弹出普通消息提示框
  static func showAlert(message: String, in viewController: UIViewController) {
    let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "确定", style: .cancel))
    viewController.present(alert, animated: true)
  }
  
  //在根视图控制器上弹出普通消息提示框
  static func showAlert(message: String) {
    if let vc = UIApplication.shared.keyWindow?.rootViewController {
      showAlert(message: message, in: vc)
    }
  }
  
  //在指定视图控制器上弹出确认框
  static func showConfirm(message: String,
                          in viewController: UIViewController,
                          confirmBtnTitle: String,
                          confirm: ((UIAlertAction)->Void)?) {
    let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title:confirmBtnTitle, style: .default, handler: confirm))
    viewController.present(alert, animated: true)
  }
  
  //在指定视图控制器上弹出确认框
  static func showConfirm(message: String, in viewController: UIViewController,
                          confirm: ((UIAlertAction)->Void)? , cancel : ((UIAlertAction)->Void)?) {
    let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: cancel))
    alert.addAction(UIAlertAction(title: "确定", style: .default, handler: confirm))
    viewController.present(alert, animated: true)
  }
  
  //在根视图控制器上弹出确认框
  static func showConfirm(message: String, confirm: ((UIAlertAction)->Void)? , cancel : ((UIAlertAction)->Void)?) {
    if let vc = UIApplication.shared.keyWindow?.rootViewController {
      showConfirm(message: message, in: vc, confirm: confirm , cancel: cancel)
    }
  }
  
  //两秒钟后自动消失
  static func asyncAfterShowAlert(message: String) {
    
    if let vc = UIApplication.shared.keyWindow?.rootViewController {
      showAlert(message: message, in: vc)
      DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
        vc.presentedViewController?.dismiss(animated: false, completion: nil)
      }
    }
    
  }

  
}
