//
//  UIViewController+extension.swift
//  Merchants
//
//  Created by kingly on2020/9/4.
//  Copyright © 2020年 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

@objc extension UIViewController {
  
 @objc class func currentViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
    
    if let nav = base as? UINavigationController {
      return currentViewController(base: nav.visibleViewController)
      
    }
    if let tab = base as? UITabBarController {
      return currentViewController(base: tab.selectedViewController)
      
    }
    if let presented = base?.presentedViewController {
      return currentViewController(base: presented)
      
    }
    return base
    
  }
  
}
