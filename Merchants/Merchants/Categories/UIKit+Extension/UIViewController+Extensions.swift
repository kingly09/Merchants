import UIKit

extension UIViewController {
    
    var alertController: UIAlertController? {
        guard let alert = topViewController(nil) as? UIAlertController else { return nil }
        return alert
    }
}
