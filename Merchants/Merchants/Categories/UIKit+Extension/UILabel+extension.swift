//
//  UILabel+extension.swift
//  Merchants
//
//  Created by kingly on 2020/7/21.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

extension UILabel{
    
    //根据text的字体大小和控件的宽度以及字体内容动态计算label的高度
    func getTheHeightForLabel(font:UIFont,width:CGFloat,text:String) -> UILabel{
        
        let label = UILabel()
        let textW = width
        label.text = text
        label.font = font
        label.numberOfLines=0
        let textAtt : NSDictionary = [NSAttributedString.Key.font: font]
        let textSize : CGSize = CGSize(width: textW,height: CGFloat(MAXFLOAT))
        let textH:CGFloat = label.text!.boundingRect(with: textSize,options: .usesLineFragmentOrigin,attributes: textAtt as? [NSAttributedString.Key : Any],context:nil).size.height
        label.frame = CGRect(x: 0,y: 0, width: textW, height: textH)
        
        return label
        
    }
    
}
