//
//  AppDelegate.swift
//  Merchants
//
//  Created by kingly09 on 2020/7/12.
//  Copyright © 2020 kingly09. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

   var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

    
       // MBUserInfo.shared.signOut()
        
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white
        
        if !MBUserInfo.shared.isLogin() {
             let nav = UINavigationController(rootViewController: MBLoginVC())
            nav.modalPresentationStyle = .fullScreen
            self.window?.rootViewController = nav
        }else{
            self.window?.rootViewController =  BCTabBarController()
        }
     
        self.window?.makeKeyAndVisible()
        
        BYModuleComponentManager.registerAllService(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .allButUpsideDown
    }

    
}

extension AppDelegate: WXApiDelegate{
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        return WXApi.handleOpen(url, delegate: self)
        if let result = UMSocialManager.default()?.handleOpen(url, options: options){
            if !result{
                // 其他如支付等SDK的回调
            }
            return result
        }
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
//        return WXApi.handleOpenUniversalLink(userActivity, delegate: self)
        if let result = UMSocialManager.default()?.handleUniversalLink(userActivity, options: nil){
            if !result{
                // 其他如支付等SDK的回调
            }
        }
        return true
    }
    
    func onResp(_ resp: BaseResp) {
        if let resp = resp as? SendAuthResp{
            if resp.state == WXOAuthState{ //微信授权成功
                print("微信登录完成，code:\(resp.code ?? "--")")
             
                MGJRouter.open(BaseURLSchemes + CommonURLType.OtherLogin.rawValue)
            }
        }
    }
}

