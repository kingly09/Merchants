//
//  MBMsgCenterVC.swift
//  Merchants
//
//  Created by kingly on 2020/7/16.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

class MBMsgCenterVC: BCBaseTableViewController {

    private var MsgList = [MBSettingModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = MBStr.VCTitle.msgCenter
         // Do any additional setup after loading the view.
         loadTableDataList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.isNavigationBarHidden(isHidden: false)
          self.isTabBarHidden(isHidden: false)
      
    }

   private func loadTableDataList() {
         
         MsgList = [MBSettingModel]()
       
         MsgList.append(getSettingModel(imageName: MBImageName.MsgCenter.platformnotice, title:  MBStr.MsgCenter.platformnotice, detail: "公告公告公告公告", time: "16:9"))
    
       MsgList.append(getSettingModel(imageName: MBImageName.MsgCenter.systeminformation, title:  MBStr.MsgCenter.systeminformation, detail: "系统信息", time: "昨天 16：09"))
    
     MsgList.append(getSettingModel(imageName: MBImageName.MsgCenter.auctionresult, title:  MBStr.MsgCenter.auctionresult, detail: "公告公告公告公告", time: "5月6号 16：09"))
    
    
    MsgList.append(getSettingModel(imageName: MBImageName.MsgCenter.orderinfo, title:  MBStr.MsgCenter.orderinfo, detail: "公告公告公告公告", time: "6月6号 16：09"))
    
    
    MsgList.append(getSettingModel(imageName: MBImageName.MsgCenter.appraise, title:  MBStr.MsgCenter.appraise, detail: "辛辛添加一条评价", time: "7月6号 16：09"))
        
       
    }
       
    private func getSettingModel(imageName : String, title : String , detail : String, time : String) -> MBSettingModel {
         
         let settingModel = MBSettingModel(jsonData: JSON.null)
         settingModel.imageName = imageName
         settingModel.title     = title
         settingModel.detail    = detail
         settingModel.timetext   = time
         return settingModel
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         super.viewWillDisappear(animated)
     
    }
       
       override func didReceiveMemoryWarning() {
         super.didReceiveMemoryWarning()
         // Dispose of any resources that can be recreated.
       }
       
       
       // MARK: - 初始化
       override func initUI() {
         
          super.initUI()
          
          self.tableView?.register(MBMsgCenterTableViewCell.self, forCellReuseIdentifier: MBMsgCenterTableViewCell.cellReuseIdentifier)
        
         setupLayout()
             
        }
          
       fileprivate func setupUI() {
          
        }
        
        override func refreshHeader() {
        
        }
        
        override func refreshFooter() {
          
        }
}

// MARK: - Layout
extension MBMsgCenterVC {
  
  fileprivate  func setupLayout() {
    
  }
  
}



// MARK: - 协议
extension MBMsgCenterVC {
  
  //MARK: -  UITableViewDataSource
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return MsgList.count
  }
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: MBMsgCenterTableViewCell.cellReuseIdentifier, for: indexPath) as! MBMsgCenterTableViewCell
    //改变UITableViewCell选中时背景色(无色)
    cell.selectionStyle = .none;
    let detailModel = MsgList[indexPath.row]
    cell.settingModel = detailModel
    return cell
  }
  
  //MARK: -  UITableViewDelegate
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    
  
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
   
    if MsgList.count > 0 {
       return CGFloat(KMBMsgCenterTableViewCellHight)
    }
    return FLT_MIN
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return FLT_MIN
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return FLT_MIN
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    return nil
  }
  
  
}
