//
//  MCLoginVC.swift
//  Merchants
//
//  Created by kingly on 2020/7/15.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import YYText
import Toaster



class MBLoginVC: BYBaseViewController {
        
        
        let loginBtn = Button(frame: .zero, title: "获取验证码", titleColor: kColor_white, font: .systemFont(ofSize: 18, weight: .medium))
    
        let phoneTf = BYUITextField()

        var isAgreeProtocol = true

        override func viewDidLoad() {
            super.viewDidLoad()

            self.navigationController?.setNavigationBarHidden(true, animated: true)
            
            setupUI()
        }
        deinit {

        }
        
        func setupUI() {
          
            
            let logoImageView = UIImageView(image: UIImage(named: "login_appLogo"))
            view.addSubview(logoImageView)
            
            let tip1Lb = Label(font: .systemFont(ofSize: 30, weight: .medium), textColor: kColor_textBlack, defaultText: "你好，")
            view.addSubview(tip1Lb)
            let tip2Lb = Label(font: .systemFont(ofSize: 14, weight: .medium), textColor: kColor_textGray, defaultText: "欢迎使用宝玉有价")
            view.addSubview(tip2Lb)
            
            let phoneView = UIView()
            view.addSubview(phoneView)
            
            let phoneTypeBtn = Button(frame: .zero, title: "+86", titleColor: kColor_textBlack, font: .systemFont(ofSize: 18, weight: .medium))
            phoneTypeBtn.action { (_) in
                
            }
            phoneView.addSubview(phoneTypeBtn)
            
            let phoneSepLine = UIView()
            phoneSepLine.backgroundColor = kColor_tableViewSeparator
            phoneView.addSubview(phoneSepLine)
            
            
            phoneTf.placeholder = "请输入您的手机号码"
            phoneTf.font = PFSC_Regular(size: 16)
            phoneTf.placeholderFont = PFSC_Regular(size: 16)
            phoneTf.textColor = kColor_textBlack
            phoneTf.keyboardType = .phonePad
            phoneTf.placeholderColor = KColor(hexStr: "C3C3C3")
            phoneTf.lineColor = kColor_lineGrayColor
            phoneTf.showLine = true
            phoneTf.maxLength = 11
            phoneTf.leftSpace = 3
            phoneTf.clearButtonMode = .whileEditing
            phoneView.addSubview(phoneTf)
            
        
            
            let agreeMentSureBtn = Button(image: "checkbox_unchecked")
            agreeMentSureBtn.setImage(UIImage(named: "checkbox_checked"), for: .selected)
            agreeMentSureBtn.isSelected = true
            view.addSubview(agreeMentSureBtn)
            agreeMentSureBtn.action {[weak self] (sender) in
                sender.isSelected = !sender.isSelected
                self?.isAgreeProtocol = sender.isSelected
                self?.operationVaifyPhone(phoneNumber: self?.phoneTf.text)
            }
            let textStr = "同意《宝玉有价商户协议》" as NSString
            let text = NSMutableAttributedString(string: textStr as String)
            text.yy_font = PFSC_Light(size: 11)
            text.yy_color = kColor_detailTextGray
            let highLightText1 = "《宝玉有价商户协议》"
            text.yy_setTextHighlight(textStr.range(of: highLightText1), color: kColor_main, backgroundColor: nil) {(view, text, range, rect) in
                let title = highLightText1
                let url = "http://112.74.92.216:8078"
                MGJRouter.open(BaseURLSchemes + CommonURLType.WebView.rawValue, ["url" : url, "title" : title, "jumpMode" : 1], nil)
                
            }
            
            let stateLab = YYLabel()
            stateLab.attributedText = text
            view.addSubview(stateLab)
            
            loginBtn.backgroundColor = kColor_main
            loginBtn.layer.cornerRadius = 4
            loginBtn.clipsToBounds = true
            view.addSubview(loginBtn)
            loginBtn.action {[weak self] (sender) in
                self?.loginTouchEvent(sender: sender)
            }
            setLoginEnable(enable: true)
                 
        
            logoImageView.snp.makeConstraints { (make) in
                make.top.equalToSuperview().offset(94)
                make.centerX.equalToSuperview()
                make.size.equalTo(CGSize(width: 80, height: 80))
            }
            
            tip1Lb.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(logoImageView.snp.bottom).offset(23)
            }
            tip2Lb.snp.makeConstraints { (make) in
                make.left.equalTo(tip1Lb)
                make.top.equalTo(tip1Lb.snp.bottom).offset(5)
            }
            
            phoneView.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(30)
                make.right.equalToSuperview().offset(-30)
                make.top.equalTo(tip2Lb.snp.bottom).offset(30)
                make.height.equalTo(49)
            }
            
            phoneTypeBtn.snp.makeConstraints { (make) in
                make.left.equalToSuperview()
                make.centerY.equalToSuperview()
                make.width.equalTo(60)
            }
            phoneSepLine.snp.makeConstraints { (make) in
                make.left.equalTo(phoneTypeBtn.snp.right)
                make.centerY.equalToSuperview()
                make.size.equalTo(CGSize(width: 1, height: 20))
            }
            phoneTf.snp.makeConstraints { (make) in
                make.left.equalTo(phoneSepLine.snp.right).offset(10)
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
                make.right.equalToSuperview()
            }
            
          
            loginBtn.snp.makeConstraints { (make) in
                make.top.equalTo(phoneView.snp.bottom).offset(15)
                make.left.equalTo(25)
                make.right.equalTo(-25)
                make.height.equalTo(48)
            }
            agreeMentSureBtn.snp.makeConstraints { (make) in
                make.left.equalTo(loginBtn).offset(-5)
                make.top.equalTo(loginBtn.snp.bottom).offset(5)
                make.size.equalTo(CGSize(width: 25, height: 25))
            }

            stateLab.snp.makeConstraints { (make) in
                make.centerY.equalTo(agreeMentSureBtn)
                make.left.equalTo(agreeMentSureBtn.snp.right)
            }
            
           
             
        }
    }

extension MBLoginVC: UITextFieldDelegate{
    
        func operationVaifyPhone(phoneNumber: String?) {
            if let phoneNumber = phoneNumber, checkPhoneLegal(phoneNum: phoneNumber),  isAgreeProtocol{
                setLoginEnable(enable: true)
            }else{
                setLoginEnable(enable: false)
            }
        }
        
        //验证手机号码正确性
        func checkPhoneLegal(phoneNum: String?) -> Bool {
            
            return String.isChinaPhoneLegal(phoneNo: phoneNum ?? "")
        }
        
        func setLoginEnable(enable: Bool){
            self.loginBtn.isEnabled = enable
            self.loginBtn.backgroundColor = enable ? kColor_main : kColor_disableGray
        }
}

extension MBLoginVC {
        //SMS获取
        @objc func smsBtnTouchEvent(sender: Button){
            
            guard phoneTf.text?.count > 0, self.checkPhoneLegal(phoneNum: phoneTf.text) else {
                let toast = Toast(text: "请检查手机号码")
                toast.view.bottomOffsetPortrait = kScreenHeight / 2
                toast.show()
                return
            }
            BYNetwork.sendRequest(target: MultiTarget(MBLoginApi.sendSmsVaifyCode(phone: phoneTf.text!)), showError: true, success: { (response) in
                if response?.code == 0{
//                    MGJRouter.open(BaseURLSchemes + CommonURLType.LoginByCode.rawValue, ["phonetext": self.phoneTf.text!], nil)
                    
              
                let smsVerifyVC = MBLoginByCodeVC()
                smsVerifyVC.phonetext = self.phoneTf.text!
                smsVerifyVC.isCounting = true
  
                self.navigationController?.pushViewController(smsVerifyVC, animated: true)
                
              }else{
                    let toast = Toast(text: "获取验证码失败")
                    toast.view.bottomOffsetPortrait = kScreenHeight / 2
                    toast.show()
                }
            }) { (error) in
                let toast = Toast(text: "获取验证码失败")
                toast.view.bottomOffsetPortrait = kScreenHeight / 2
                toast.show()
                
            }
        
        }
        
        //点击发送验证码
        @objc func loginTouchEvent(sender : Button) {

            self.smsBtnTouchEvent(sender: sender)
            
       }
    }
