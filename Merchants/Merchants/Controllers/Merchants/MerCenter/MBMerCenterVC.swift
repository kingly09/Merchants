//
//  MBMerCenterVC.swift
//  Merchants
//
//  Created by kingly on 2020/7/16.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

class MBMerCenterVC: BCBaseTableViewController {

    //商户表格视图
    fileprivate var companyTableView: UITableView!
    
    private var companyList = [MBSettingModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadTableData()
    }
    

    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
       self.isNavigationBarHidden(isHidden: true)
       self.isTabBarHidden(isHidden: false)
   
     }
     
     func loadTableData() {
         loadTableDataCompanyList()
     }
    

    func loadTableDataCompanyList() {
      
      companyList = [MBSettingModel]()
    
      companyList.append(getSettingModel(imageName: MBImageName.MerCenter.accsetting, title:  MBStr.MerCenter.accsetting, detail: ""))
      companyList.append(getSettingModel(imageName: MBImageName.MerCenter.rolesetting, title:  MBStr.MerCenter.rolesetting, detail: ""))
    
    }
    
    private func getSettingModel(imageName : String, title : String , detail : String) -> MBSettingModel {
      
      let settingModel = MBSettingModel(jsonData: JSON.null)
      settingModel.imageName = imageName
      settingModel.title     = title
      settingModel.detail    = detail
      
      return settingModel
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       super.viewWillDisappear(animated)
       self.isNavigationBarHidden(isHidden: false)
        
     }
     
     override func didReceiveMemoryWarning() {
       super.didReceiveMemoryWarning()
       // Dispose of any resources that can be recreated.
     }
     
     
     // MARK: - 初始化
     override func initUI() {
       
        super.initUI()
        
        
           self.tableView?.register(MBSettingTableViewCell.self, forCellReuseIdentifier: MBSettingTableViewCell.cellReuseIdentifier)
           if isIPhoneX {
             self.tableView.contentInset = UIEdgeInsets(top: statusBar, left: 0, bottom: 0, right: 0)
           }else {
             self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
           }
          self.tableView.isHidden = true
        
           companyTableView = UITableView(frame: self.tableView.frame, style: self.tableviewStyle)
           companyTableView.separatorStyle = .none
           companyTableView.delegate = self
           companyTableView.dataSource = self
           companyTableView?.register(MBSettingTableViewCell.self, forCellReuseIdentifier: MBSettingTableViewCell.cellReuseIdentifier)
           companyTableView.isHidden = false
           self.view.addSubview(companyTableView)
           
           if isIPhoneX {
             self.companyTableView.contentInset = UIEdgeInsets(top: statusBar, left: 0, bottom: 0, right: 0)
           }else {
             self.companyTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
           }
           
           companyTableView.ky_top = kMBMerCenterDetailHeaderViewHight
        
           self.view.addSubview(headerView)
           
           
           setupLayout()
           
           self.tableView.ky_top = kMBMerCenterDetailHeaderViewHight
           
          
        }
        
     fileprivate func setupUI() {
        
        
     
        
      }
      
      override func refreshHeader() {
        
        
      }
      
      override func refreshFooter() {
        
        
      }
    
      // MARK: -  Getter and Setter
      // 头部headerview
      fileprivate lazy var headerView: MBMerCenterDetailHeaderView = {
        let headerView = MBMerCenterDetailHeaderView()
        headerView.backgroundColor = .red
        // headerView.delegate = self
        return headerView
      }()
}

// MARK: - Layout
extension MBMerCenterVC {
  
  fileprivate  func setupLayout() {
    
     headerView.snp.makeConstraints { (make) in
         make.top.equalTo(0)
         make.left.equalTo(0)
         make.width.equalTo(CGFloat(SCREEN_WIDTH))
         make.height.equalTo(kMBMerCenterDetailHeaderViewHight)
       }
       
  }
  
}

// MARK: - 协议
extension MBMerCenterVC {
  
  //MARK: -  UITableViewDataSource
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  
    if tableView == self.companyTableView {
      if section == 0 {
         return 1
       }else if section == 1 {
         return 2
       }else if section == 2 {
         return 1
       }
       return 0
    }
    return 0
  }
  func numberOfSections(in tableView: UITableView) -> Int {
    
    if tableView == self.companyTableView {
          return 3
    }
    return 0
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
   if tableView == self.companyTableView {
      
      let cell = tableView.dequeueReusableCell(withIdentifier: MBSettingTableViewCell.cellReuseIdentifier, for: indexPath) as! MBSettingTableViewCell
      cell.selectionStyle = .none
      if indexPath.section == 0 {
        cell.settingModel  = getSettingModel(imageName: MBImageName.MerCenter.shopsetting, title:  MBStr.MerCenter.shopsetting, detail: "")
      }else if indexPath.section == 1 {
        let settingModel = companyList[indexPath.row]
            cell.settingModel = settingModel
      }else if indexPath.section == 2 {
        cell.settingModel  = getSettingModel(imageName: MBImageName.MerCenter.systemsetting, title:  MBStr.MerCenter.systemsetting, detail: "")
      }
    
      return cell
      
    }
    
    return UITableViewCell()
  }
  
  //MARK: -  UITableViewDelegate
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    
    
    if tableView ==  self.companyTableView {
    
        if indexPath.section == 0 {
             MBOpenRouter.businessDetail()
        }else if indexPath.section == 1 {
            
             let settingModel = companyList[indexPath.row]
            if(settingModel.title == MBStr.MerCenter.accsetting){
                MBOpenRouter.merCenterAccount()
            }else if(settingModel.title == MBStr.MerCenter.rolesetting){
                MBOpenRouter.merCenterRole()
            }
            
        }else if indexPath.section == 2 {
            
        }
    
    }
    
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
      return CGFloat(44)
   
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     if section == 1 {
        return 10.0
      }else if section == 2 {
        return 10.0
      }
    return FLT_MIN
  }
  
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return FLT_MIN
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

    return nil
  }
  

}
