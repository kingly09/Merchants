//
//  MBWorkVC.swift
//  Merchants
//
//  Created by kingly on 2020/7/16.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift
import SwiftyJSON

class MBWorkVC: BCBaseTableViewController {
    
   override func viewDidLoad() {
     super.viewDidLoad()
     
   }
   
   override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
     
     self.isNavigationBarHidden(isHidden: true)
     self.isTabBarHidden(isHidden: false)
     
     loadTableData()
   
   }
   
   //加载数据信息
   func loadTableData() {
     
   }
   
   override func viewWillDisappear(_ animated: Bool) {
       super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
       super.didReceiveMemoryWarning()
       // Dispose of any resources that can be recreated.
    }
    
    // MARK: - 初始化
    override func initUI() {
      super.initUI()
       
       self.tableView?.register(MBMerchantTableViewCell.self, forCellReuseIdentifier: MBMerchantTableViewCell.cellReuseIdentifier)
        
        
        self.tableView?.register(MBApplyResidenceViewCell.self, forCellReuseIdentifier: MBApplyResidenceViewCell.cellReuseIdentifier)
         
        
        self.tableView?.register(MBMyWallViewCell.self, forCellReuseIdentifier: MBMyWallViewCell.cellReuseIdentifier)
        

          self.tableView?.register(MBCommonEntryTableViewCell.self, forCellReuseIdentifier: MBCommonEntryTableViewCell.cellReuseIdentifier)
          self.tableView?.register(MBLiveEntryTableViewCell.self, forCellReuseIdentifier: MBLiveEntryTableViewCell.cellReuseIdentifier)
      
          
          if isIPhoneX {
            self.tableView.contentInset = UIEdgeInsets(top: globalNavigationBarHeight + statusBar, left: 0, bottom: 0, right: 0)
          }else {
            self.tableView.contentInset = UIEdgeInsets(top: globalNavigationBarHeight, left: 0, bottom: 0, right: 0)
          }
        
    }
    
    override func refreshHeader() {
    }
    
    override func refreshFooter() {
        
    }

}


// MARK: - 协议
extension MBWorkVC {
  
  //MARK: -  UITableViewDataSource
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  func numberOfSections(in tableView: UITableView) -> Int {
    return 4
  }
    
  //MARK: -  UITableViewDelegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if  indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: MBMerchantTableViewCell.cellReuseIdentifier, for: indexPath) as! MBMerchantTableViewCell
            //改变UITableViewCell选中时背景色(无色)
            cell.selectionStyle = .none;
            return cell
            
        }else if  indexPath.section == 1 {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: MBApplyResidenceViewCell.cellReuseIdentifier, for: indexPath) as! MBApplyResidenceViewCell
            //改变UITableViewCell选中时背景色(无色)
            cell.selectionStyle = .none;
            return cell
            
            //         let cell = tableView.dequeueReusableCell(withIdentifier: MBMyWallViewCell.cellReuseIdentifier, for: indexPath) as! MBMyWallViewCell
            //         //改变UITableViewCell选中时背景色(无色)
            //         cell.selectionStyle = .none;
            //return cell
        }else if  indexPath.section == 2 {
         
         let cell = tableView.dequeueReusableCell(withIdentifier: MBCommonEntryTableViewCell.cellReuseIdentifier, for: indexPath) as! MBCommonEntryTableViewCell
         //改变UITableViewCell选中时背景色(无色)
            cell.selectionStyle = .none;
            
            cell.appArr = (MBUserInfo.shared.getLoginRspInfo().userInfo?.commonFuntion as [VJFuntionModel]?)!
            
            return cell
        }else if  indexPath.section == 3 {
         
         let cell = tableView.dequeueReusableCell(withIdentifier: MBLiveEntryTableViewCell.cellReuseIdentifier, for: indexPath) as! MBLiveEntryTableViewCell
         //改变UITableViewCell选中时背景色(无色)
            cell.selectionStyle = .none;
            
            cell.appArr = (MBUserInfo.shared.getLoginRspInfo().userInfo?.liveFuntion as [VJFuntionModel]?)!
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if indexPath.section == 0 {
            return CGFloat(kMBMerchantTableViewCellHight)
        }else if indexPath.section == 1 {
            
            return CGFloat(kMBApplyResidenceViewCellHight)
            
        }else if indexPath.section == 2 {
            
            return CGFloat(MBCommonEntryTableViewCell.cellHight(arrCont: (MBUserInfo.shared.getLoginRspInfo().userInfo?.commonFuntion!.count)!))+38.0
            
        }else if indexPath.section == 3 {
            return CGFloat(MBLiveEntryTableViewCell.cellHight(arrCont: (MBUserInfo.shared.getLoginRspInfo().userInfo?.liveFuntion!.count)!))+38.0
        }
        return CGFloat(0)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return FLT_MIN
    }
  
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 3 {
            return CGFloat(50)
        }
        return FLT_MIN
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 3 {
            let view = UIView()
            view.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 50.0)
            view.backgroundColor = .white
            return view
        }
        return nil
    }
    
    
}
