//
//  MBOrderManageVC.swift
//  Merchants
//
//  Created by kingly on 2020/7/21.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

class MBOrderManageVC: BCBaseTableViewController {
    
   override func viewDidLoad() {
      super.viewDidLoad()
      
    }
    
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      self.isNavigationBarHidden(isHidden: true)
      self.isTabBarHidden(isHidden: true)
      
      loadTableData()
    
    }
    
    //加载数据信息
    func loadTableData() {
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
     }
     
     override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
     }
     
     // MARK: - 初始化
     override func initUI() {
       super.initUI()
        
    
         
     }
     
     override func refreshHeader() {
     }
     
     override func refreshFooter() {
         
     }
     

}
