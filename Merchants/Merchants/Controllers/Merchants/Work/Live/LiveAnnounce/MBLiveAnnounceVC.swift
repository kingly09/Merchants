//
//  MBLiveAnnounceVC.swift
//  Merchants
//
//  Created by kingly on 2020/7/21.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

///直播计划
class MBLiveAnnounceVC : BCBaseTableViewController {
    
    var pageTitleList: [String] = ["全部", "今天", "明天", "后天", "07月28日", "07月29日", "07月30日", "07月31日"]
    
    lazy var headerView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 25))
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = MBStr.Work.liveAnnounce
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isNavigationBarHidden(isHidden: false)
        self.isTabBarHidden(isHidden: true)
        
   
        
    }
    
//    //加载数据信息
//    func loadTableData() {
//
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 50))
//
//        configration  = YNPageConfigration.defaultConfig()
//        configration?.pageStyle = .suspensionCenter
//        configration?.scrollViewBackgroundColor = kColor_tableViewDefaultBackground
//        configration?.showTabbar = false
//        configration?.showNavigation = true
//        configration?.scrollMenu = true
//        configration?.aligmentModeCenter = false
//        configration?.showScrollLine = false
//        configration?.showBottomLine = false
//        configration?.itemFont = PFSC_Regular(size: 12)
//        configration?.selectedItemFont = PFSC_Regular(size: 12)
//        configration?.menuHeight = 50
//        configration?.itemHeight = 25
//        configration?.normalItemColor = kColor_lightBlack
//        configration?.selectedItemColor = kColor_white
//        configration?.normalItemBgColor = kColor_white
//        configration?.selectedItemBgColor = kColor_lightBlack
//        configration?.itemLeftAndRightMargin = 20
//        configration?.itemLeftAndRightInset = 10
//        configration?.itemMargin = 20
//        configration?.pageScrollEnabled = true
//
//        pageVC = nil
//        pageVC = YNPageViewController(pageViewControllerWithControllers: pageVCList, titles: pageTitleList, config: configration)
//        pageVC?.pageIndex = 0
//        pageVC?.delegate = self
//        pageVC?.dataSource = self
//        pageVC?.headerView = view
//        pageVC?.addSelf(toParentViewController: self)
//        pageVC?.view.snp.makeConstraints({ (make) in
//            make.left.right.top.bottom.equalToSuperview()
//        })
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - 初始化
    override func initUI() {
        super.initUI()
        self.tableView?.register(MBLiveAnnounceCell.self, forCellReuseIdentifier: MBLiveAnnounceCell.cellReuseIdentifier)
        self.tableView.backgroundColor =  UIColor(hexStr: "F3F3F3")
    }
    
    override func refreshHeader() {
    }
    
    override func refreshFooter() {
        
    }
    
    
}

// MARK: - 协议
extension MBLiveAnnounceVC {

    //MARK: -  UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    //MARK: -  UITableViewDelegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        tableView.deselectRow(at: indexPath, animated: true)

        let cell = tableView.dequeueReusableCell(withIdentifier: MBLiveAnnounceCell.cellReuseIdentifier, for: indexPath) as! MBLiveAnnounceCell
        //改变UITableViewCell选中时背景色(无色)
        cell.selectionStyle = .none;
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return CGFloat(kMBLiveAnnounceCellHight)

    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        return FLT_MIN
    }


    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        return nil
    }

 func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
         return 10.0
 }

 func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
      let view = UIView()
      view.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 10.0)
      view.backgroundColor =  UIColor(hexStr: "F3F3F3")
      return view
  }


}

//extension MBLiveAnnounceVC: YNPageViewControllerDelegate, YNPageViewControllerDataSource{
//    func pageViewController(_ pageViewController: YNPageViewController!, pageFor index: Int) -> UIScrollView! {
//        if let childVC = pageViewController.controllersM[index] as? BYShowListBaseController{
//            return childVC.layoutScrollView
//        }else{
//            return UIScrollView()
//        }
//    }
//}
//
//
//class MBLiveAnnounceListVC:  BYShowListBaseController, UITableViewDataSource, UITableViewDelegate {
//
//    var modelItems: [MBLiveAnnounceModel] = []{
//        didSet{
//            tableView.reloadData()
//        }
//    }
//
//    lazy var tableView: BYBaseTableView = {
//        let table = BYBaseTableView(frame: .zero, style: .plain)
//        table.separatorStyle = .none
//        table.delegate = self
//        table.dataSource = self
//        table.rowHeight = CGFloat(kMBLiveAnnounceCellHight)
//        table.gifHeaderRefreshing {[weak self] in
//            self?.reloadData(nil)
//        }
//        table.normalFooterRefreshing {[weak self] in
//            self?.loadMoreData(nil)
//        }
//       table.register(MBLiveAnnounceCell.self, forCellReuseIdentifier: MBLiveAnnounceCell.cellReuseIdentifier)
//        layoutScrollView = table
//        return table
//    }()
//
//
//    override func viewDidLoad() {
//        view.addSubview(tableView)
//        tableView.snp.makeConstraints { (make) in
//            make.edges.equalToSuperview()
//        }
//        tableView.mj_header?.executeRefreshingCallback()
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.tabBarController?.tabBar.isHidden = true
//
//    }
//
//    override func reloadData(_ complation: (() -> ())?) {
//        modelItems = (0..<10).map{ i in
//            let model = MBLiveAnnounceModel(jsonData:  JSON.null)
//            return model
//        }
//        tableView.reloadData()
//        //tableView.mj_header?.endRefreshing()
//    }
//
//    override func loadMoreData(_ complation: (() -> ())?) {
//        tableView.reloadData()
//        tableView.mj_footer?.endRefreshing()
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//            return 1
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//           return modelItems.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: MBLiveAnnounceCell.cellReuseIdentifier, for: indexPath) as! MBLiveAnnounceCell
//
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//            return 10.0
//    }
//
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//         let view = UIView()
//         view.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 10.0)
//         view.backgroundColor =  UIColor(hexStr: "F3F3F3")
//         return view
//     }
//}
