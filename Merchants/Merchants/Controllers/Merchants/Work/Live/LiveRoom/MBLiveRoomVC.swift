//
//  MBLiveRoomVC.swift
//  Merchants
//
//  Created by kingly on 2020/7/20.
//  Copyright © 2020 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import YYText

/// 直播间
class MBLiveRoomVC: BCBaseTableViewController {

    /// 默认为未开直播
    public  var liveType : LIVE_TYPE = .NONE
    
    public  var isFirst : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isNavigationBarHidden(isHidden: true)
        self.isTabBarHidden(isHidden: true)
        
        loadTableData()
        
        MBUserInfo.shared.liveType = self.liveType
        
       
        
    }
    
    //加载数据信息
    func loadTableData() {
        if  MBUserInfo.shared.isFirst == true {
            UserWatchInfoView.setupWachNumber(Num: 0)
             MBUserInfo.shared.isFirst = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - 初始化
    override func initUI() {
        super.initUI()
        
        self.view.addSubview(liveBgImageView)
        self.view.addSubview(headerView)
        self.view.addSubview(UserWatchInfoView)
        self.view.addSubview(LiveRoomtoolbar)
        
        self.view.addSubview(liveOrderView)
        liveOrderView.addSubview(liveOrderLabel)
        liveOrderView.addSubview(xiaoHongdianLabel)
    
        
        //点击用户头像或昵称的显示界面
        self.view.addSubview(LiveUserInfoView)
        
        
        setupLayout()
        
       
        
    }
    
    override func refreshHeader() {
        
    }
    
    override func refreshFooter() {
        
    }
    
    // MARK: -  Getter and Setter
    // 头部headerview
    fileprivate lazy var headerView: MBLiveRoomHeadView = {
        let headerView = MBLiveRoomHeadView()
        headerView.delegate = self
        let y = isIPhoneX ? 22.0 : 0.0
        headerView.frame = CGRect(x: 0.0, y: y, width: Double(SCREEN_WIDTH) , height: 40)
        return headerView
    }()
    
    fileprivate lazy var liveBgImageView: UIImageView = {
        var liveBgImageView = UIImageView()
        liveBgImageView.image = globalImageWithName(MBImageName.work.live.liveBgImageView)
        liveBgImageView.isUserInteractionEnabled = true
        return liveBgImageView
    }()
    
    fileprivate lazy var UserWatchInfoView: MBUserWatchInfoView = {
        let UserWatchInfoView = MBUserWatchInfoView()
        UserWatchInfoView.frame = CGRect(x: 15, y: 100.0, width: Double(SCREEN_WIDTH) - 30 , height: 32)
        return UserWatchInfoView
    }()
    
    fileprivate lazy var LiveRoomtoolbar : MBLiveRoomtoolbar = {
        let LiveRoomtoolbar = MBLiveRoomtoolbar()
        LiveRoomtoolbar.frame = CGRect(x: 0.0, y: Double(SCREEN_HEIGHT - 50), width: Double(SCREEN_WIDTH) , height: 50)
        return LiveRoomtoolbar
    }()
    
    
    fileprivate lazy var liveOrderView : UIView  = {
        var view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = UIColor(hexStr: "000000", alpha: 0.5)
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(liveOrderViewClick))
        view.addGestureRecognizer(singleTapGesture)
        return view
    }()
    
    fileprivate lazy var liveOrderLabel : UILabel = {
        var liveOrderLabel = UILabel()
       
        liveOrderLabel.font = UIFont.systemFont(ofSize: 14)
        liveOrderLabel.textColor = .white
        liveOrderLabel.textAlignment = .center
        liveOrderLabel.text =  "订单"
        
        return liveOrderLabel
    }()
    
    fileprivate lazy var xiaoHongdianLabel : YYLabel = {
        var xiaoHongdianLabel = YYLabel()
        let textStr = "1" as NSString
        let text = NSMutableAttributedString(string: textStr as String)
        text.yy_font = PFSC_Light(size: 11)
        text.yy_color = kColor_white
        xiaoHongdianLabel.attributedText = text
        xiaoHongdianLabel.backgroundColor = UIColor(hexStr: "DC3030")
        xiaoHongdianLabel.textAlignment = .center
        return xiaoHongdianLabel
    }()
    
    
    
    fileprivate lazy var LiveUserInfoView : MBLiveUserInfoView = {
        let LiveUserInfoView = MBLiveUserInfoView()
        LiveUserInfoView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
       return LiveUserInfoView
    }()
    
}

// MARK: - 私有方法
extension MBLiveRoomVC {
  
    @objc fileprivate  func liveOrderViewClick () {
    
   }
    
}

// MARK: - Layout
extension MBLiveRoomVC {
  
  fileprivate  func setupLayout() {
    
     liveBgImageView.snp.makeConstraints {(make) in
         make.top.equalTo(0)
         make.left.equalTo(0)
         make.width.equalTo(CGFloat(SCREEN_WIDTH))
         make.height.equalTo(CGFloat(SCREEN_HEIGHT))
    }
    
    liveOrderView.snp.makeConstraints {(make) in
         make.bottom.equalTo(-333)
         make.left.equalTo(-16)
         make.width.equalTo(66)
         make.height.equalTo(32)
    }
    liveOrderView.layer.cornerRadius = 16
    //liveOrderView.layer.masksToBounds = true
    
    liveOrderLabel.snp.makeConstraints {(make) in
       
         make.width.equalTo(50)
         make.height.equalTo(32)
         make.top.equalTo(0)
         make.left.equalTo(16)
    }
    
    xiaoHongdianLabel.snp.makeConstraints {(make) in
         make.width.equalTo(16)
         make.height.equalTo(16)
         make.right.equalTo(0)
         make.top.equalTo(-8)
    }
    xiaoHongdianLabel.layer.cornerRadius = 8
  }
}


// MARK: - 协议 MBLiveRoomHeadViewDelegate
extension MBLiveRoomVC: MBLiveRoomHeadViewDelegate {
  
   /// 返回上一个界面
  func backBtnDidClick(){

    self.navigationController?.popViewController(animated: true)
      
  }
    
  /// 点击头像
  func userLogoBtnDidClick() {
    
  }
    
 /// 点击开始直播
 func openLiveBtnDidClick() {
    
 }
    
 /// 点击关闭当前直播
 func closeLiveBtnDidClick() {
    
    self.navigationController?.popViewController(animated: true)
    MBUserInfo.shared.liveType = .NONE
 
 }
  
    
}


