//
//  BCMainNavgationVC.swift
//  BCCastle
//
//  Created by kingly on 2018/7/16.
//  Copyright © 2018年 Bambooclound Co., Ltd. All rights reserved.
//

import UIKit

class BCMainNavgationVC: UINavigationController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationBar.barTintColor = UIColor.appMainColor()
    navigationBar.tintColor = UIColor.white
    
    let color = UIColor.white
    let shadow = NSShadow()
    shadow.shadowOffset = CGSize.zero
    let titleTextAttributes = [
      NSAttributedString.Key.foregroundColor: color,
      NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
      NSAttributedString.Key.shadow: shadow
            ]
    navigationBar.titleTextAttributes = titleTextAttributes
  
  }
  
  // 拦截 push 操作
  override func pushViewController(_ viewController: UIViewController, animated: Bool) {
    if viewControllers.count > 0 {
      viewController.hidesBottomBarWhenPushed = true
      viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: MBImageName.icon.backblack), style: .plain, target: self, action: #selector(navigationBack))
    }
    super.pushViewController(viewController, animated: true)
//    //  修改tabBra的frame
//    var  frame: CGRect = (self.tabBarController?.tabBar.frame)!
//    frame.origin.y = SCREEN_WIDTH - frame.size.height
//    self.tabBarController?.tabBar.frame = frame;
   
  }
  
  /// 返回上一控制器
  @objc private func navigationBack() {
    popViewController(animated: true)
  }
  
  override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
