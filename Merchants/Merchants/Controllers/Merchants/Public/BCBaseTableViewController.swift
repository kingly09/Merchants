//
//  BCBaseTableViewController.swift
//  Merchants
//
//  Created by kingly on2020/7/16.
//  Copyright © 2020年 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit
import MJRefresh

class BCBaseTableViewController: BCMainBaseViewController {

  //表格视图
  var tableView: UITableView!
  var tableviewStyle : UITableView.Style! = .plain
  /// 被选中的Cell
  var selectedCellIndexPath: IndexPath!
  //上拉刷新的标记
  var isPullup = false
  /// 默认分页数量为15
  var pageSize: Int = 15
  /// 总页数
  var total: Int = 0
  /// 当前页数
  var num: Int = 0
  /// 当前展示数据是否还有下一页
  var isHasMore: Bool = false
  /// 当前是第几页
  var curPage: Int = 1
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  //MARK:   UI
  override func initUI() {
    
    super.initUI()
    //设置表视图
    self.setupTableView()
  }
  
  
  /// 设置表格视图--  在预留前置方法 之后执行
  //子类重写方法 因为子类不需要关心预留前置方法之前的逻辑
   func setupTableView() {
    //取消自动缩进 - 如果隐藏了导航栏 会缩进20点
    automaticallyAdjustsScrollViewInsets = false
    
    self.tableView = UITableView(frame: view.bounds, style: self.tableviewStyle)
    if let navigationBar = self.navigationController?.navigationBar  {
      self.view.insertSubview(tableView, belowSubview: navigationBar)
    }
    if #available(iOS 11.0, *) {
      self.tableView.contentInsetAdjustmentBehavior = .never
      self.tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 49, right: 0)
      self.tableView.scrollIndicatorInsets = tableView.contentInset
    }
    self.tableView.delegate = self
    self.tableView.dataSource = self
    self.tableView.separatorStyle = .none
    //设置内容缩进
    if let hight  = navigationController?.navigationBar.bounds.height {
      tableView.contentInset = UIEdgeInsets(top: hight, left: 0, bottom: tabBarController?.tabBar.ky_height ?? 49, right: 0)
    }
    //设置导航条的缩进
    self.tableView.scrollIndicatorInsets  = tableView.contentInset
    
    if isIPhoneX {
       self.tableView.contentInset = UIEdgeInsets(top:  globalNavigationBarHeight + statusBar, left: 0, bottom: 0, right: 0)
    }else {
       self.tableView.contentInset = UIEdgeInsets(top: globalNavigationBarHeight, left: 0, bottom: 0, right: 0)
    }
    //refreshHeader下拉刷新
    self.refreshHeader()
    //refreshFooter上拉刷新
    self.refreshFooter()
    //加载数据
    loadData()
  }
  
  
  // MARK: -  下拉上拉刷新
  /// 子类重写refreshHeader方法
  func refreshHeader() {
    self.tableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in
      self?.loadData()
    })
  }
  /// 子类重写refreshFooter方法
   func refreshFooter()  {
        self.tableView.mj_footer = MJRefreshBackNormalFooter.init(refreshingBlock: { [weak self] in
          self?.loadMoreData()
        })
  }
  /// 停止刷新
  func endRefreshing(){
        if self.tableView.mj_header != nil {
            self.tableView.mj_header?.endRefreshing()
        }
        if self.tableView.mj_footer != nil {
            self.tableView.mj_footer?.endRefreshing()
        }
    
  }
  /// 停止刷新，显示没有更多信息了
  func endRefreshingWithNoMoreData()  {
        if self.tableView.mj_footer != nil {
            self.tableView.mj_footer?.endRefreshingWithNoMoreData()
        }
  }
  
  /// 重置没有更多的数据（消除没有更多数据的状态）
  func resetNoMoreData() {
        if self.tableView.mj_footer != nil {
            self.tableView.mj_footer?.resetNoMoreData()
        }
  }
  /// 加载数据 具体由子类负责
  func loadData() {
    //如果子类不实现 默认关闭刷新
    self.endRefreshing()
    
  }
  /// 加载更多数据
  func loadMoreData() {
    //如果子类不实现 默认关闭刷新
    self.endRefreshing()
    
  }
  
  
}

// MARK: - 代理方法
extension BCBaseTableViewController: UITableViewDelegate,UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 0
  }
  //基类只是准备方法 子类负责具体实现  子类发数据源方法不需要super
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //只是保证没有语法错误
    return UITableViewCell()
  }
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return FLT_MIN
  }
  
  //在显示最后一行的时候 做上拉刷新
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    // 判断indexpath 是否最后一行
    let row = indexPath.row
    //取最后一组
    let section = tableView.numberOfSections - 1
    if row < 0 || section < 0 {
      return
    }
    //最后一组的行数
    let count = tableView.numberOfRows(inSection: section)
    //如果是最后一行 同时没有开始上拉刷新
    if (row == count - 1) && !isPullup {
      print("上拉刷新")
      isPullup = true
      //开始刷新
      loadData()
      
    }
  }
  
  
}

