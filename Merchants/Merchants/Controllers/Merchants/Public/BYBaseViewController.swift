//
//  BYBaseViewController.swift
//  Merchants
//
//  Created by kingly09 on 2020/7/12.
//  Copyright © 2020 kingly09. All rights reserved.
//

import UIKit
import RTRootNavigationController
import MJRefresh
import SnapKit
import Kingfisher

class BYBaseViewController: UIViewController {
     
    var params : [String : Any]?
    
    var navBottomLineHidden: Bool = false{
        didSet{
            if self.navBottomLineHidden == oldValue { return }
            let navBgView = self.navigationController?.navigationBar.subviews.first
            let line = navBgView?.subviews.first
            line?.isHidden = self.navBottomLineHidden
        }
    }
    
    lazy var messageBtn: Button = {
        let msgBtn = Button(image: "nav_message")
        msgBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        msgBtn.addTarget(self, action: #selector(message), for: .touchUpInside)
        return msgBtn
    }()
    lazy var whiteMessageBtn: Button = {
        let msgBtn = Button(image: "message_white")
        msgBtn.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        msgBtn.addTarget(self, action: #selector(message), for: .touchUpInside)
        return msgBtn
    }()
    
    @objc private func message() {
        //FIXME: 增加消息通知入口
    }
    
    lazy var messageBtnItem : UIBarButtonItem = {
        return UIBarButtonItem(customView: messageBtn)
    }()
    
    lazy var whiteMessageBtnItem : UIBarButtonItem = {
        return UIBarButtonItem(customView: whiteMessageBtn)
    }()
    
    deinit {
        let className = String(describing:self.classForCoder)as NSString
        print("--------控制器:\(className) --deinit")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: PFSC_Regular(size: 17)]
        view.backgroundColor = UIColor.white
        self.navBottomLineHidden = true
        
    }
    
    override func rt_customBackItem(withTarget target: Any!, action: Selector!) -> UIBarButtonItem! {
        
        if (self.navigationController?.viewControllers.count ?? 0 > 1) {
            
            let backBtn = Button(type: .custom)
            backBtn.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            backBtn.setImage(UIImage(named:"nav_back"), for: .normal)
            backBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: 15)
            backBtn.backgroundColor = UIColor.clear
            backBtn.addTarget(target, action: action, for: .touchUpInside)
            
            let backBarItem = UIBarButtonItem(customView: backBtn)
            return backBarItem
        }
        return nil
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}
