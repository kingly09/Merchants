//
//  BCBaseCollectionController.swift
//  Merchants
//
//  Created by kingly on2020/7/16.
//  Copyright © 2020年 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

/// 以CollectionView为主要的 控制器的基类
class BCBaseCollectionController: BCMainBaseViewController {

  
  public var layout: UICollectionViewFlowLayout!
  public var collectionView: UICollectionView!
  /// 被选中的Cell
  public var currentIndexPath: IndexPath!
  /// 当前展示数据是否还有下一页
  public var isHaveMore: Bool = false
  /// 当前是第几页
  public var currentPage: Int = 1
  /// 默认分页数量为15
  //    public var pageSize: Int = 10
  /// 数据源
  public var dataSource =  NSArray()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupCollectionViewUI()
  }
  
  //MARK:  UI
  func setupCollectionViewUI(){
    self.layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .vertical
    self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: self.layout)
    self.collectionView.backgroundColor = UIColor.groupTableViewBackground
    self.view.addSubview(self.collectionView)
    self.setupCollectionViewConstrains()
    
  }
  func setupCollectionViewConstrains(){
    self.collectionView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height:SCREEN_HEIGHT)
  }
  
  //MARK:  refresh
  /// 设置刷新头和腿CollectionView
  func setupRefreshCollectionView(){
    
//    self.collectionView.mj_header = MZRefreshAnimatedHeader.init(block: { [weak self] in
//      self?.pullDownRefreshCollectionView()
//    })
    
    //self.collectionView.mj_footer = MJRefreshBackNormalFooter.init(refreshingBlock: { _ in
      //            if self?.isHaveMore {
      //                self?.pullOnLoadingCollectionView()
      //            }else{
      //                self?.tableView.mj_footer.endRefreshingWithNoMoreData()
      //
      //            }
   // })
    
  }
  
  /// 下拉刷新CollectionView
  func pullDownRefreshCollectionView(){
  }
  /// 上拉加载CollectionView
  func pullOnLoadingCollectionView(){
    
    
  }
  
  /// 结束刷新
  func endRefreshingCollectionView(){
    //        if self.tableView.mj_header != nil {
    //            self.tableView.mj_header.endRefreshing()
    //        }
    //        if self.tableView.mj_footer != nil {
    //            self.tableView.mj_footer.endRefreshing()
    //        }
    
  }

  
}
