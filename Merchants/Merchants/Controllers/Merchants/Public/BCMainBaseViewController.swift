//
//  BCMainBaseViewController.swift
//  Merchants
//
//  Created by kingly on 2020/7/16.
//  Copyright © 2020年 VALUABLE JADE(SHENZHEN) JEWELRY JADE Co.,Ltd. All rights reserved.
//

import UIKit

/**
 1.extension 中不能有属性
 2.extension 不能重写父类发方法 重写父类方法是子类发职责 分类负责扩展
 3.作为所有控制器的基类，所有控制器类必须继承于它
 */
@objc class BCMainBaseViewController: UIViewController {
  
 var params : [String : Any]?
    
  fileprivate var isTabHiden = true
  fileprivate var isNavigationBarHiden = true
  @objc override func viewDidLoad() {
    super.viewDidLoad()
    self.initUI()

  }
  
  @objc public func initUI() {
    view.backgroundColor = UIColor.white
    //设置导航条
    self.setupNavigationBar()
    
  }
  @objc override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
     print("viewWillAppear: \(type(of: self))")
    self.isNavigationBarHidden(isHidden: self.isNavigationBarHiden)
    self.isTabBarHidden(isHidden: self.isTabHiden)
    
  }
  @objc override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
  }
  @objc override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    self.isNavigationBarHidden(isHidden: self.isNavigationBarHiden)
    self.isTabBarHidden(isHidden: self.isTabHiden)
  }
  //MARK:   生命周期
  deinit {
//    print("\(NSStringFromClass(type(of: self))) : deinit")
//
//    // 返回内部类名
//    print("deinit: \(object_getClassName(self))")
//    // 返回应用程序名+类名
//    print("deinit: \(NSStringFromClass(type(of: self)))")
//
//    // 返回应用程序名+类名+内存地址
//    print("deinit: \(self)")
//
//    // 返回应用程序名+类名+内存地址
//    print("deinit: \(self.description)")
    
    // 返回类名
    print("deinit: \(type(of: self))")
  
  }
  //MARK:  屏幕旋转
  /// 关闭自动重力感应  1.视图是否自动旋转,2.旋转支持方向,必须同时实现 才能生效
  override var shouldAutorotate : Bool {
    return false
    
  }
  /// 旋转支持方向,必须同时实现
  override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
    return UIInterfaceOrientationMask.portrait
  }

}

// MARK: - 设置界面
@objc extension BCMainBaseViewController {
 
 
}

// MARK: - 设置导航条 tab显示隐藏
@objc extension BCMainBaseViewController {
  
  /// 设置导航条
 @objc func setupNavigationBar()  {
    
  if self.children.count > 0 {
       self.setupLeftNavigationBar(image: globalImageWithName(MBImageName.icon.backblack),action: #selector(backToPopViewController))
    }
  }
  /// 设置导航条左边的带有图标按钮
  ///
  /// - Parameters:
  ///   - image: 按钮图片
  ///   - action: 点击方法
  @objc func setupLeftNavigationBar(image: UIImage?,action: Selector? ) {
    let leftBarBtn = UIBarButtonItem(image: image, style: UIBarButtonItem.Style.plain, target: self, action: action)
    self.navigationItem.leftBarButtonItem = leftBarBtn
  }
  ///  设置导航条右边的按钮带有图标
  ///
  /// - Parameters:
  ///   - image: 按钮图片
  ///   - action: 点击方法
  @objc func setupRightNavigationBar(image: UIImage?,action: Selector? ) {
    let rightBarBtn = UIBarButtonItem(image: image, style: UIBarButtonItem.Style.plain, target: self, action: action)
    self.navigationItem.rightBarButtonItem = rightBarBtn
  }
  /// 点击左边返回按钮，返回到上一页
  @objc func backToPopViewController() {
    self.navigationController?.popViewController(animated: true)
  }
  
  //MARK:  tab navigationbar 的显示隐藏
  /// 是否tabBarController视图
  // - Parameter isHidden: true 为隐藏 false 为显示
  @objc func isTabBarHidden(isHidden: Bool){
    if let tabarCtr = self.tabBarController {
      tabarCtr.tabBar.isHidden = isHidden
      self.isTabHiden = isHidden
    }
    
  }
  /// 是否显示导航条
  @objc func isNavigationBarHidden(isHidden: Bool)  {
    if let navCtr = self.navigationController {
      navCtr.isNavigationBarHidden =  isHidden
      self.isNavigationBarHiden = isHidden
    }
    
  }
  
  
}
