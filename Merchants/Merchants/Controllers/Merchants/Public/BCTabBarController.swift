//
//  BCTabBarController.swift
//  BCCastle
//
//  Created by kingly on 2018/7/17.
//  Copyright © 2018年 Bambooclound Co., Ltd. All rights reserved.
//

import UIKit

class BCTabBarController: UITabBarController {

    override func viewDidLoad() {
      super.viewDidLoad()
     
      UITabBar.appearance().isTranslucent = false
      
      self.tabBar.barTintColor  = UIColor.white
      self.tabBar.tintColor = UIColor.appMainColor()
      // 添加子控制器
      addChildViewControllers()
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  /// 添加子控制器
  private func addChildViewControllers() {
    
    setChildViewController(MBWorkVC(), title: MBStr.VCTitle.work, imageName: MBImageName.tabBar.work)

    setChildViewController(MBMsgCenterVC(), title: MBStr.VCTitle.msgCenter, imageName: MBImageName.tabBar.msgCenter)
    setChildViewController(MBMerCenterVC(), title: MBStr.VCTitle.merCenter, imageName: MBImageName.tabBar.merCenter)

  }
  
  /// 初始化子控制器
  private func setChildViewController(_ childController: UIViewController, title: String, imageName: String) {
    // 设置 tabbar 文字和图片
    childController.tabBarItem.image = UIImage(named: imageName)
    //childController.tabBarItem.selectedImage = UIImage(named: imageName + "-selected")
    childController.tabBarItem.title = title
    
    let nav = BCMainNavgationVC()
    nav.addChild(childController)
    addChild(nav)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }

}
